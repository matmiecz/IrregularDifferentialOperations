package org.visnow.vn.DiffOpPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunctionLagrangeForm;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import static org.visnow.jscic.cells.CellType.POINT;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;
import org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException;
import org.visnow.vn.DiffOpPlugin.VNUtils.IterationMeta;
import org.visnow.vn.DiffOpPlugin.VNUtils.IterationsHistory;

import org.visnow.vn.lib.utils.sort.IndirectComparator;
import static org.visnow.vn.lib.utils.sort.IndirectSort.indirectSort;

/**
 *
 * @author mateuszmieczkowski
 */
public class DecimateField {

    private static final Logger LOGGER = Logger.getLogger(DecimateField.class);

    private final ApproximateDifferentials fieldApproximation;
    private final CalculateHierarchy hierarchyCalculator;
    private final NodeNeighbours nodeNeighbours;
    private final IrregularField inField;
    private final int nNodes;
    private final DataArray normalized2ndOrdDiffNormForField;
    private final FloatLargeArray coords;

    private byte[] nodesMask;
    private int[] sortedNodes;
    private double[] originalDataRadius;

    private final byte[] hierarchy;

    public static final byte NOT_COMPUTED = -1, SELECTED = 1, DISMISSED = 0;

    // storing iterations history for each hierarchy
    // list of maps -> entry for each iteration -> entry for each hierarchy 
    private final IterationsHistory decimationHistory = new IterationsHistory();
// ----------------------------------------------------

    public DecimateField(ApproximateDifferentials fieldApproximation, CalculateHierarchy hierarchyCalculator) {
        this.fieldApproximation = fieldApproximation;
        this.nodeNeighbours = fieldApproximation.getNeighbourCounter();
        this.hierarchyCalculator = hierarchyCalculator;
        this.inField = fieldApproximation.getField();
        this.nNodes = (int) inField.getNNodes();
        this.normalized2ndOrdDiffNormForField = fieldApproximation.getNormalized2ndOrdDiffNormForField();
        this.coords = inField.getCurrentCoords();

        this.hierarchy = hierarchyCalculator.getHierarchyArray();
    }
//<editor-fold defaultstate="collapsed" desc="not used in current concept but preserved for alternative implementation">

//calculates geometric radius of the group
    public double calculateGeometricalCentreForGroup(int[] groupOfNodes, FloatLargeArray coords) {
        int nN = groupOfNodes.length;
        double[] coordsOfGroup = new double[3 * nN];
        for (int i = 0; i < nN; i++) {
            int currentNode = groupOfNodes[i];
            for (int j = 0; j < 3; j++) {
                coordsOfGroup[3 * i + j] = coords.getDouble(3 * currentNode + j);
            }
        }
        double[] geometricalCentreCoords = new double[3];
        for (int i = 0; i < geometricalCentreCoords.length; i++) {
            double sum = 0.0;
            for (int j = 0; j < nN; j++) {
                sum += coordsOfGroup[3 * j + i];
            }
            geometricalCentreCoords[i] = sum / nN;
        }

        //finding biggest radius
        double radiusForGroup = 0.0;
        for (int i = 0; i < nN; i++) {
            double r = 0.0;
            for (int j = 0; j < 3; j++) {
                r += (coordsOfGroup[3 * i + j] - geometricalCentreCoords[j]) * (coordsOfGroup[3 * i + j] - geometricalCentreCoords[j]);
            }
            r = Math.sqrt(r);
            if (r > radiusForGroup) {
                radiusForGroup = r;
            }
        }
        return radiusForGroup;
    }

    public boolean compare(int[] nodes, DataArray nodeImportancy, FloatLargeArray coords) {
        boolean bool = false;

        int nN = nodes.length;
        double[] importancyForGroup = new double[nN];
        for (int i = 0; i < nN; i++) {
            importancyForGroup[i] = nodeImportancy.getDoubleElement((long) nodes[i])[0];
        }
        Arrays.sort(importancyForGroup);
        double lowestImportancy = importancyForGroup[0];
        double geometricR = calculateGeometricalCentreForGroup(nodes, coords);
        if (geometricR < lowestImportancy) {
            bool = true;
        }
        return bool;
    }

//</editor-fold>
    public double findFieldSpan() {
        //goal - find space span of the field (maximum distance for any coordinate)

        //is there more elegant way to find max/min of coords? 
        double[] x = inField.getCurrentCoords1D(0).getDoubleData();
        double[] y = inField.getCurrentCoords1D(1).getDoubleData();
        double[] z = inField.getCurrentCoords1D(2).getDoubleData();

        double minX = ArrayUtils.findMin(x);
        double maxX = ArrayUtils.findMax(x);
        double minY = ArrayUtils.findMin(y);
        double maxY = ArrayUtils.findMax(y);
        double minZ = ArrayUtils.findMin(z);
        double maxZ = ArrayUtils.findMax(z);

        double xSpan = maxX - minX;
        double ySpan = maxY - minY;
        double zSpan = maxZ - minZ;

        //find maximal span of the 3
        double maxSpan;
        if (inField.getTrueNSpace() == 2) {
            maxSpan = Math.max(xSpan, ySpan);
        } else {
            maxSpan = Math.max(xSpan, Math.max(ySpan, zSpan));
        }
        return maxSpan;
    }

    private void sortNodesByImportance() {
        IndirectComparator ic = new IndirectComparator() {
            @Override
            public int compare(int i, int j) {
                if (hierarchy[i] > hierarchy[j]) {
                    return 1;
                } else if (hierarchy[i] == hierarchy[j]) {
                    double i_data = normalized2ndOrdDiffNormForField.getDoubleElement((long) i)[0];
                    double j_data = normalized2ndOrdDiffNormForField.getDoubleElement((long) j)[0];
                    if (i_data > j_data) {
                        return 1;
                    } else if (i_data < j_data) {
                        return -1;
                    } else {
                        return 0;
                    }
                } else {
                    return -1;
                }
            }
        };

        int[] sorted = new int[nNodes];
        for (int i = 0; i < sorted.length; i++) {
            sorted[i] = i;
        }
        indirectSort(sorted, ic, false);
        this.sortedNodes = sorted;
    }

    public byte[] getHierarchiesToDecimate() {
        return Hierarchy.getHierarchyValuesForDecimation(getField().getTrueNSpace());
    }

    public int[] getSortedNodes() {
        if (this.sortedNodes == null) {
            sortNodesByImportance();
        }
        return this.sortedNodes;
    }

    public IterationsHistory getDecimationHistory() {
        return decimationHistory;
    }

    /**
     * dataRadius of the field = the more important the point (2nd order
     * derivative value (norm) is high) the lower the radius should be
     *
     * dataRadius proportional to:
     * <ul>
     * <li> fieldSpan - physical size of the field
     * <li> (1 - decimationCoefficient); where decimationCoefficient is target
     * number of elements to be left after decimation for hierarchy H
     * (n_target,H) and total number of elements for hierarchy H (n_H)
     * </ul>
     *
     * Radius inversely proportional to:
     * <ul>
     * <li> 2ndOrderDiffNorm for node
     * </ul>
     *
     * @param targetDecimationCoefficients
     * @param diffNormWeightCoefficients
     * @return dataRadius for selected parameters
     */
    public double[] computeOriginalDataRadiusField(Map<Byte, Double> targetDecimationCoefficients, Map<Byte, Double> diffNormWeightCoefficients) {
        // TODO ? validate decimation coefficients - have to be higher than 0 and less/equal 1
        double span = findFieldSpan();

        // coefficient to match number of elements with respect to data distribution
        // depending of normalized2ndOrded distribution (when the distribution is 
        // far from uniform then the radius will be inproperly calculated?) 
        // 
        // for now only iterative approach, with linear scaling
        // maybe some statistical coefficient of 2nd order differential distribution?
        double[] orgDataRadius = new double[nNodes];

        if (normalized2ndOrdDiffNormForField != null) {
            double delta; // decimation coefficient = nTarget/nOriginal
            double mu; // weight coefficient 

            for (int i = 0; i < orgDataRadius.length; i++) {
                byte nodeHierarchy = hierarchy[i];
                // ERROR WHAT TO DO WITH HIERARCHY OTHER THAN AVAILABLE?
                if (nodeHierarchy != Hierarchy.VERTEX.getValue()) {
                    delta = targetDecimationCoefficients.get(nodeHierarchy);
                    mu = diffNormWeightCoefficients.get(nodeHierarchy);
                    orgDataRadius[i] = (1. / (normalized2ndOrdDiffNormForField.getDoubleElement((long) i)[0] + mu)) * span * (1 - delta);
                } else {
                    // for vertex hierarchy - preserve all elements (radius should be 0)
                    orgDataRadius[i] = 0;
                }
            }
        }

        return orgDataRadius;
    }

    public double[] getOriginalDataRadius() {
        return this.originalDataRadius;
    }

    public void setOriginalDataRadius(double[] dataRadius) {
        this.originalDataRadius = dataRadius;
    }


    /*
    Rescale radius according to input hashMap (every hierarchy by different scaling factor)
    WARNING! Only copy - do not modify original radius.
    
    key -> hierarchy 
    value -> scaling factor 
     */
    public double[] getRescaledRadius(Map<Byte, Double> linearRadiusCorrections) {
        double[] rescaledRadius = getOriginalDataRadius().clone();

        for (Map.Entry<Byte, Double> entry : linearRadiusCorrections.entrySet()) {
            byte selectedHierarchy = entry.getKey();
            double scaleFactor = entry.getValue();

            for (int i = 0; i < rescaledRadius.length; i++) {
                if (hierarchy[i] == selectedHierarchy) {
                    rescaledRadius[i] *= scaleFactor;
                }
            }
        }
        return rescaledRadius;
    }

    public double[] findAverageDistanceToNeighbourForField() {
        double[] averageDistances = new double[nNodes];
        int[] nodes = nodeNeighbours.getNodes();
        for (int i = 0; i < averageDistances.length; i++) {
            averageDistances[i] = findAverageDistanceToNeighbour(nodes[i]);
        }
        return averageDistances;
    }

    /**
     * Find average distance to neighbour for node index.
     *
     * @return
     */
    private double findAverageDistanceToNeighbour(int nodeIndex) {
        double averageDistance = 0.0;

        int[] neighbours = nodeNeighbours.getDirectNeighboursForNode(nodeIndex);
        int nNeighbours = neighbours.length;

        double[] distances = new double[nNeighbours];
        for (int i = 0; i < distances.length; i++) {
            // distance to neighbour i
            distances[i] = findDistance(nodeIndex, neighbours[i]);
        }
        for (double distance : distances) {
            averageDistance += distance;
        }
        averageDistance = averageDistance / nNeighbours;

        return averageDistance;
    }

    /**
     * Find node indices that are located within given radius from selected
     * node.
     *
     * Return array of node indices, whose distance from the selected *node* is
     * less than given radius.
     *
     * The algorithm is based on additions of the subsequent node neighbours
     * layers until no node in the layer is located inside radius.
     *
     * Two class fields are required:
     * <ul>
     * <li> coords - coordinates array (length 3 * nNodes)
     * <li> nodeNeighbours - NodeNeighbours class instance with neighbours
     * already calculated
     * </ul>
     *
     * @param nodeIndex
     * @param radius
     * @return int[] array of node indices within distance of radius from
     * selected node index
     */
    public int[] findNodesWithinRadius(int nodeIndex, double radius) {
        Set<Integer> nodesWithinRadiusSet = new HashSet<>();

        // starting from direct neighbours 
        int[] neighbours = nodeNeighbours.getDirectNeighboursForNode(nodeIndex);
        // init nextLayer the same as neighbours
        int[] nextLayerNeighbours = neighbours;
        int previousSize = -1;

        while (nodesWithinRadiusSet.size() != previousSize) {
            previousSize = nodesWithinRadiusSet.size();
            for (int neighbour : nextLayerNeighbours) {
                double distance = findDistance(nodeIndex, neighbour);
                if (distance < radius) {
                    nodesWithinRadiusSet.add((Integer) neighbour);
                }
            }
            neighbours = nextLayerNeighbours;
            nextLayerNeighbours = nodeNeighbours.subsequentNeighboursForNode(nodeIndex, neighbours);
        }
        return nodesWithinRadiusSet.stream().mapToInt(i -> i).toArray();
    }

    /**
     * Find distance between two nodes (coords).
     *
     * @param node1
     * @param node2
     * @return
     */
    private double findDistance(int node1, int node2) {
        double x1 = coords.get((int) node1 * 3);
        double y1 = coords.get((int) node1 * 3 + 1);
        double z1 = coords.get((int) node1 * 3 + 2);

        double x2 = coords.get((int) node2 * 3);
        double y2 = coords.get((int) node2 * 3 + 1);
        double z2 = coords.get((int) node2 * 3 + 2);

        double distance = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1) + (z2 - z1) * (z2 - z1));
        return distance;
    }

    private byte[] computeSelectionMask(Map<Byte, Double> linearRadiusCorrection) {
        double[] correctedDataRadius = getRescaledRadius(linearRadiusCorrection);
        int[] nodesSorted = getSortedNodes();

        // validate if length of sortedNodes, radius is equal to nNodes
        if (nodesSorted.length != nNodes) {
            throw new InternalError("Fatal error - sorted nodes length != nNodes");
        }

        if (correctedDataRadius.length != nNodes) {
            throw new InternalError("Fatal error - length of radius array for all nodes != nNodes");
        }

        // init with "NOT_COMPUTED"
        // selection mask order is the same as nodes; NOT sorted nodes!!!
        byte[] selectionMask = new byte[nNodes];
        Arrays.fill(selectionMask, NOT_COMPUTED);

        for (int i = 0; i < nodesSorted.length; i++) {
            // iterate over sorted nodes, but the position in nodes/hierarchy array is different due to sort!
            int nodeToSelectIndex = nodesSorted[i];

            if (selectionMask[nodeToSelectIndex] == NOT_COMPUTED) {
                byte nodeToSelectHierarchy = getHierarchy(nodeToSelectIndex);
                selectionMask[nodeToSelectIndex] = SELECTED;

                // vertices should be selected without any conditions
                if (nodeToSelectHierarchy != Hierarchy.VERTEX.getValue()) {
                    double r = correctedDataRadius[i];
                    int[] nodesWithinRadius = findNodesWithinRadius(nodeToSelectIndex, r);
                    // only dismiss nodes with the same hierarchy
                    for (int nodeWithinRadius : nodesWithinRadius) {
                        // nodeWithinRadius is index of node - NOT THE POSITION IN SORTED NODES ARRAY!
                        if (getHierarchy(nodeWithinRadius) == nodeToSelectHierarchy) {
                            if (selectionMask[nodeWithinRadius] != SELECTED) {
                                selectionMask[nodeWithinRadius] = DISMISSED;
                            }
                        }
                    }
                }
            }
        }
        return selectionMask;
    }

    /**
     * Main method to find decimated field indices in form of nodesMask.
     *
     * @param targetElementsToLeave
     * @param linearRadiusCorrection
     * @param diffNormWeightCoefficient
     * 
     * @return selectionMask
     *
     * values:
     * <ul>
     * <li> 0 - not computed
     * <li> 1 - selected
     * <li> 2 - not selected (dismissed = within radius of more important node)
     * </ul>
     *
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public byte[] decimationIteration(Map<Byte, Integer> targetElementsToLeave, Map<Byte, Double> linearRadiusCorrection, Map<Byte, Double> diffNormWeightCoefficient) throws MultipleCellSetsException, ComputationError {
        byte[] selectionMask = computeSelectionMask(linearRadiusCorrection);

        Map<Byte, IterationMeta> iterationInfo = new HashMap<>();
        // report / compute next radius correction
        for (byte ht : getHierarchiesToDecimate()) {
            // n elements that remained after decimation
            // store decimation history, necessary for finding radius correction
            int nLeftAfterDecimation = getNElementsLeft(selectionMask, ht);
            int targetElements = targetElementsToLeave.get(ht);
            int maxN = hierarchyCalculator.getNNodesByHierarchy(ht);

            // save to iteration history
            VNUtils.IterationMeta iterationMeta = new VNUtils.IterationMeta(linearRadiusCorrection.get(ht), diffNormWeightCoefficient.get(ht), nLeftAfterDecimation, targetElements, maxN);
            iterationInfo.put(ht, iterationMeta);
        }

        getDecimationHistory().addIterationInfo(iterationInfo);

        // store amount of selected based on hierarchy
        Map<Byte, Integer> nSelected = new HashMap<>();
        for (byte hierarchyType : Hierarchy.getAllValidHierarchyValues()) {
            nSelected.put(hierarchyType, getNElementsLeft(selectionMask, hierarchyType));
        }

        LOGGER.info("======================================================================================");
        LOGGER.info("ITERATION " + (getDecimationHistory().getNIterations() - 1));
        LOGGER.info("Linear radius corrections: " + linearRadiusCorrection.toString());
        LOGGER.info("SELECTED");
        for (Map.Entry<Byte, Integer> entry : nSelected.entrySet()) {
            LOGGER.info("Hierarchy\t" + entry.getKey() + "\tselected\t" + entry.getValue() + "\tout of \t" + hierarchyCalculator.getNNodesByHierarchy(entry.getKey()));
        }
        LOGGER.info("======================================================================================");
        return selectionMask;
    }

    /**
     * Select best decimation parameters based on iteration history. If not
     * enough elements have been selected enhance it by adding the most
     * important nodes (highest 2nd order diff norm) that have been dismissed.
     *
     * @param targetElementsToLeave
     * @param allowedAbsoluteError
     * @param allowedRelativeError
     *
     * @return selectionMask (0/1)
     */
    public byte[] computeBestSelectionMask(Map<Byte, Integer> targetElementsToLeave, int allowedAbsoluteError, double allowedRelativeError) {
        IterationsHistory iterationsHistory = getDecimationHistory();

        Map<Byte, Double> linearRadiusCorrection = new HashMap<>();
        Map<Byte, Integer> nToAdd = new HashMap<>();

        for (byte ht : getHierarchiesToDecimate()) {
            IterationMeta iterationClosestToTarget = iterationsHistory.getClosestToTargetIterationMeta(ht);
            boolean anotherRequired = iterationClosestToTarget.anotherIterationRequired(allowedAbsoluteError, allowedRelativeError);
            IterationMeta bestIteration = iterationClosestToTarget;
            if (anotherRequired) {
                bestIteration = iterationsHistory.getClosestToTargetLessElementsIterationMeta(ht);
                nToAdd.put(ht, bestIteration.getTargetElements() - bestIteration.getNLeft());
            }
            linearRadiusCorrection.put(ht, bestIteration.getLinearCorrection());
        }

        byte[] selectionMask = computeSelectionMask(linearRadiusCorrection);
        int[] nodesSorted = getSortedNodes();

        // decimation manual enhancement - if not enough points selected
        for (Map.Entry<Byte, Integer> entry : nToAdd.entrySet()) {
            byte h = entry.getKey();
            int nToAddForHierarchy = entry.getValue();

            for (int i = 0; i < nodesSorted.length; i++) {
                // iterate over sorted nodes, but the position in nodes/hierarchy array is different due to sort!
                int nodeToSelectIndex = nodesSorted[i];
                if ((getHierarchy(nodeToSelectIndex) == h) && (selectionMask[nodeToSelectIndex] == DISMISSED)) {
                    selectionMask[nodeToSelectIndex] = SELECTED;
                    nToAddForHierarchy--;
                    if (nToAddForHierarchy == 0) {
                        break;
                    }
                }
            }
        }
        return selectionMask;
    }

    public boolean latestIterationBest() {
        IterationsHistory iterationsHistory = getDecimationHistory();

        int latestIteration = iterationsHistory.getNIterations() - 1;

        for (byte ht : getHierarchiesToDecimate()) {
            if (iterationsHistory.getClosestToTargetIterationNumber(ht) != latestIteration) {
                return false;
            }
        }
        return true;
    }

    /**
     * Calculate linear radius corrections for field decimation.
     *
     * These is a relationship between: nElementsLeft and radius -> f(radius) =
     * nElementsLeft
     *
     * ESSENTIAL ASSUMPTION - This function should be NON-INCREASING so:
     * <ul>
     * <li> - to REDUCE number of elements radius should be INCREASED (not
     * enough elements have been dismissed before)
     * <li> - to INCREASE number of elements radius should be REDUCED (too many
     * elements have been dismissed before)
     * </ul>
     *
     * @param selectedHierarchy
     * @param iterationsHistory
     * @param targetElements
     * @param maxElements
     * @return
     */
    public static Map<String, Double> findLinearRadiusCorrection(byte selectedHierarchy, IterationsHistory iterationsHistory, int targetElements, int maxElements) {

        String linearKey = "linear";
        Map<String, Double> rCorrection = new HashMap<>();

        List<Integer> resultElements = iterationsHistory.getResultElements(selectedHierarchy);
        List<Double> linearRadiusCorrections = iterationsHistory.getLinearCorrections(selectedHierarchy);
        List<Double> diffNormWeightCoefficients = iterationsHistory.getDiffNormWeightCoefficients(selectedHierarchy);

        LOGGER.info("FIND RADIUS CORRECTION FOR - " + Hierarchy.getType(selectedHierarchy).getName());
        LOGGER.info("Target elements (max elements):\t" + targetElements + "(" + maxElements + ")");
        LOGGER.info("Result elements:\t" + resultElements.toString());
        LOGGER.info("Linear radius corrections:\t" + linearRadiusCorrections.toString());
        LOGGER.info("Diff norm weight coefficients:\t" + diffNormWeightCoefficients.toString());

        int N = iterationsHistory.getNIterations();
        int[] orderedByAbsoluteErrorModulo = iterationsHistory.getOrderedByAbsoluteDifferenceModulo(selectedHierarchy);
        int[] orderedByNLeft = iterationsHistory.getOrderedByResultElements(selectedHierarchy);
        int[] orderedByLinearCorrection = iterationsHistory.getOrderedByLinearCorrection(selectedHierarchy);

//        // check if monotonic - order of left elements and corrections should be the same (but reversed)
//        for (int i = 1; i < orderedByLinearCorrection.length; i++) {
//            // larger linearCorrection should result in lower number of elements
//            if (resultElements.get(orderedByLinearCorrection[i]) > resultElements.get(orderedByLinearCorrection[i - 1])) {
//                throw new AssertionError("Function f(radiusCorrection) = nElementsLeft is not monotonic! Required assertion not met.");
//            }
//        }
        // find closest result
        int closestResultIteration = orderedByAbsoluteErrorModulo[0];
        int closestResultNElements = resultElements.get(closestResultIteration);

        // interpolation/extrapolation has no sense, when result elements number does not change
        boolean resultsNotConverging = true;
        for (int i = 0; i < resultElements.size(); i++) {
            if (resultElements.get(i) != closestResultNElements) {
                resultsNotConverging = false;
            }
        }

        if (resultsNotConverging) {
            // exponentiation beyond existing results (aggresive)
            double correction = linearRadiusCorrections.get(closestResultIteration) * Math.pow(((double) closestResultNElements / targetElements), resultElements.size() - 0.5);
            rCorrection.put(linearKey, correction);
        } else {
            // find N results that are closest to target result
            // iterpolate f(corrections) = nLeft
            double[] nLeftLinearCorrectionAscending = new double[N];
            double[] nLeftAscending = new double[N];
            double[] correctionsAscending = new double[N];

            for (int i = 0; i < N; i++) {
                correctionsAscending[i] = linearRadiusCorrections.get(orderedByLinearCorrection[i]);
                nLeftLinearCorrectionAscending[i] = resultElements.get(orderedByLinearCorrection[i]);
                nLeftAscending[i] = resultElements.get(orderedByNLeft[i]);
            }

            // EITHER INTERPOLATE (when targetElements between exisiting iteration results)
            // OR EXTRAPOLATE (when targetElements beyond previous iteration results)
            if ((targetElements > nLeftAscending[0]) && (targetElements < nLeftAscending[N - 1])) {
                // interpolate here

                // linear does not work - it repeats itself
//                LinearInterpolator li = new LinearInterpolator();
//                PolynomialSplineFunction fun = li.interpolate(correctionsAscending, nLeftLinearCorrectionAscending);
                PolynomialFunctionLagrangeForm fun = new PolynomialFunctionLagrangeForm(correctionsAscending, nLeftLinearCorrectionAscending);

                // find closest result from both sides and those arguments
                double interpolatedLinearRadius;

                // 1) find results closest to target from both sides
                int nearestLargerThanTargetIndex = N - 1;
                int nearestSmallerThanTargetIndex = 0;

                for (int i = 1; i < N; i++) {
                    if (nLeftAscending[i] < targetElements) {
                        nearestSmallerThanTargetIndex++;
                    } else {
                        if (nLeftAscending[i] > targetElements) {
                            nearestLargerThanTargetIndex = i;
                            break;
                        }
                    }
                }

                // 2) find their arguments (radiusCorrection)
                // nearestLargeCorrection < nearestSmallerCorrection (-> larger number of elements -> smaller value)
                double nearestLargerCorrection = linearRadiusCorrections.get(orderedByNLeft[nearestLargerThanTargetIndex]);
                double nearestSmallerCorrection = linearRadiusCorrections.get(orderedByNLeft[nearestSmallerThanTargetIndex]);
                // 3) assuming that the function is monotonic interpolate for radius 
                // multiple correction values between those two
                int nTries = 1000; // try to interpolate for 1000 different radiuses and try to find interpolated value closest to target
                double diff = Double.POSITIVE_INFINITY;
                double step = (nearestSmallerCorrection - nearestLargerCorrection) / nTries;

                double argument = nearestLargerCorrection;
                interpolatedLinearRadius = argument; // init 

                for (int i = 0; i < nTries; i++) {
                    double result = Math.abs(fun.value(argument) - targetElements);
                    if (result < diff) {
                        diff = result;
                        interpolatedLinearRadius = argument;
                    }
                    argument += step;
                }
                // 4) searched is radius that gives closest [to target] predicted nLeft 
                // for given interpolation function
                rCorrection.put(linearKey, interpolatedLinearRadius);
            } else {
                // further exponentiation (not so aggresive - function is converging)
                double correction = linearRadiusCorrections.get(closestResultIteration) * Math.pow(((double) closestResultNElements / targetElements), 0.5);
                rCorrection.put(linearKey, correction);
            }

        }
        return rCorrection;
    }

    /**
     * multiply values in double array corresponding to nodes represented by
     * hierarchy
     *
     * @param array
     * @param linearRadiusCorrection
     * @param selectedHierarchy
     * @return
     */
    public double[] correctSelectedHierarchy(double[] array, double linearRadiusCorrection, byte selectedHierarchy) {
        double[] multipliedArray = array.clone();
        if (multipliedArray.length != this.hierarchy.length) {
            throw new InternalError("Internal error - array size must be equal to hierarchy array to map values.");
        } else {
            for (int i = 0; i < multipliedArray.length; i++) {
                if (hierarchy[i] == selectedHierarchy) {
                    multipliedArray[i] *= linearRadiusCorrection;
                }
            }
            return multipliedArray;
        }
    }

    public static IrregularField extractField(IrregularField irrField, byte[] nodesMask) throws ComputationError, IllegalArgumentException {
        // 1) find nodes masked by decimation
        // 2) get data and coords for these nodes
        // 3) recreate irregular field WITHOUT CELL SET - this must be added in subsequent step
        int nTimesteps = irrField.getCoords().getValues().size();
        if (nTimesteps != 1) {
            throw new ComputationError("Only one timestamp allowed");
        } else {
            FloatLargeArray coords = (FloatLargeArray) irrField.getCoords().getValues().get(0);
            if ((nodesMask.length * 3) != coords.length()) {
                throw new ComputationError("Internal error. Coords length does not match required value.");
            } else {
                // ----------------------------------------------------------
                // STEP 1) Count elements to preserve
                int nSelected = ArrayUtils.countByteOccurences(nodesMask, SELECTED);

//                 RETURN irrField or point field?
                if (nSelected == irrField.getNNodes()) {
                    return irrField;
                }
                int[] indicesSelected = new int[nSelected];
                // IMPORTANT - assert nodes are continuous and max = length; no gaps
                // fill original node indices that have been selected
                // indicesSelected is continous array mapping to old node numbers
                // i.e. 
                // oldNodes = {0,1,2,3}
                // nodesMask = {SELECTED,SELECTED,DISMISSED,SELECTED}
                // then indicesSelected = {0,1,3}
                // it is required to get data values from old field and map them to new nodes
                int counter = 0;
                for (int i = 0; i < nodesMask.length; i++) {
                    if (nodesMask[i] == SELECTED) {
                        indicesSelected[counter] = i;
                        counter++;
                    }
                }
                IrregularField extractedField = new IrregularField(nSelected);
                float[] decimatedFieldCoords = new float[(int) nSelected * 3];

                for (int i = 0; i < indicesSelected.length; i++) {
                    // get from 'coords' three values and insert into 'decimatedFieldCoords'
                    int index = indicesSelected[i];
                    for (int ix = 0; ix < 3; ix++) {
                        float coordVal = coords.getFloat(3 * index + ix);
                        decimatedFieldCoords[3 * i + ix] = coordVal;
//                        decimatedFieldCoords.setFloat(i+ix, coordVal);
                    }
                }
                extractedField.setCoords(new FloatLargeArray(decimatedFieldCoords), 0);

                String oldIndicesName = "old_indices";
                if (irrField.getComponent(oldIndicesName) != null) {
                    irrField.removeComponent(oldIndicesName);
                }

                // extract components and assign them to the field
                extractedField = extractComponents(irrField, extractedField, indicesSelected);
                extractedField.addComponent(DataArray.create(indicesSelected, 1, oldIndicesName));
                return extractedField;
            }
        }
    }

    /**
     * Extract IrregularField consisting of only point from nodesMask after
     * decimation.
     *
     * Last step of field decimation (target is triangulation TODO, this is
     * temporary solution)
     *
     *
     * @param irrField
     * @param nodesMask
     * @return
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public static IrregularField extractPointField(IrregularField irrField, byte[] nodesMask) throws ComputationError, IllegalArgumentException {
        IrregularField extractedField = extractField(irrField, nodesMask);
        int nSelected = (int) extractedField.getNNodes();
        int[] nodes = new int[nSelected];
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = i;
        }

        // add only points
        CellSet cs = new CellSet();
        cs.addCells(new CellArray(POINT, nodes, null, null));
        extractedField.addCellSet(cs);
        extractedField.checkTrueNSpace();
        return extractedField;
    }

    public static IrregularField extractFieldWithCellSets(IrregularField irrField, byte[] nodesMask) throws ComputationError, IllegalArgumentException {
        IrregularField extractedField = extractField(irrField, nodesMask);
        int nCellSets = irrField.getNCellSets();
        // TODO check if this operation is possible (check node numbers)
        for (int i = 0; i < nCellSets; i++) {
            extractedField.addCellSet(irrField.getCellSet(i));
        }

        extractedField.checkTrueNSpace();
        return extractedField;
    }

    /**
     * Extract data components from old field and assign them to new field with
     * reduced number of nodes given array of node indices selected.
     *
     * @param sourceField - non decimated field
     * @param targetField - already decimated field (reduced number of nodes)
     * @param indicesSelected - array of node indices from old field
     *
     * @return
     */
    private static IrregularField extractComponents(IrregularField sourceField, IrregularField targetField, int[] indicesSelected) {
        int nSelected = indicesSelected.length;
        IrregularField extractedField = targetField.cloneShallow();
        extractedField.getComponents().clear();

        // here add data - slicing based on selectedIndices
        int nComponents = sourceField.getNComponents();
        for (int i = 0; i < nComponents; i++) {
            DataArray oldDataArr = sourceField.getComponent(i);
            String name = oldDataArr.getName(); // name 
            DataArrayType type = oldDataArr.getType(); // type
            int vecLen = oldDataArr.getVectorLength(); // length of vector
            LargeArray oldData = oldDataArr.getRawArray(); // data
            LargeArrayType laType = oldData.getType();

            int time = 0;
            LargeArray newData = LargeArrayUtils.create(laType, nSelected * vecLen);
            for (int ix = 0; ix < indicesSelected.length; ix++) {
                int index = indicesSelected[ix];
                LargeArrayUtils.arraycopy(oldData, vecLen * index, newData, vecLen * ix, vecLen);
            }
            extractedField.addComponent(DataArray.create(newData, vecLen, name), i);
        }
        return extractedField;
    }

    /**
     * count how many elements were left after selection for given hierarchy.
     *
     * @param nodesMask
     * @param selectedHierarchy
     * @return
     */
    public int getNElementsLeft(byte[] nodesMask, byte selectedHierarchy) {
        if (nodesMask.length == hierarchy.length) {
            int count = 0;
            for (int i = 0; i < nodesMask.length; i++) {
                if ((nodesMask[i] == SELECTED) && (hierarchy[i] == selectedHierarchy)) {
                    count++;
                }
            }
            return count;
        } else {
            throw new InternalError("Internal error. nodesMask.length != hierarchy.length");
        }

    }

    public byte[] getNodesMask() {
        return nodesMask;
    }

    public void setNodesMask(byte[] nodesMask) {
        this.nodesMask = nodesMask;
    }

    /**
     * Get hierarchy for selected node.
     *
     * @param nodeIndex
     * @return
     */
    public byte getHierarchy(int nodeIndex) {
        return hierarchyCalculator.getHierarchy(nodeIndex);
    }

    public IrregularField getField() {
        return this.fieldApproximation.getField();
    }

}
