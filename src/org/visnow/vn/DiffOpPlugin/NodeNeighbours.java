/*
 * Copyright (c) 2018, WIN
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.visnow.vn.DiffOpPlugin;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;

/**
 * Calculate direct neighbours of the nodes in the field.
 *
 * Add additional neighbours whenever necessary.
 *
 *
 * @author MMieczkowski
 */
public class NodeNeighbours {
    
    private static final Logger LOGGER = Logger.getLogger(NodeNeighbours.class);

    private final int nNodes;
    private final int[] nodes;
    private final CellType simplexType;

    private final int[] nodeIndices; // indices corresponding to all class field
    // avoid error when node numbers are not subsequent
    // i.e. if nodeIndices = {1,4,5} directNeighbours length should be 3
    // but direct accessing by ID (i.e. 4) will result in an error
    // thus it is necessary to find index of node 4 in nodeIndices (1) and access 
    // direct neighbour with this index.
    // 
    // IF node's indices were subsequent (0,1,2,...) then this would not be necessary

    // necessary mapping when nodes are not numbered from 0->(nNodes-1)
    private final boolean nodesSubsequent; // check if nodes are subsequent to avoid 
    // computation of indices

    private int[] directNeighbours = null;   //array of the adjacent nodes
    private int[] directNeighboursInfo = null;   //array containing information of nodeNeighbours array
    private int[][] requiredNeighbours;

    public NodeNeighbours(int nNodes, int[] nodes, CellType type) {
        this.nNodes = nNodes;
        this.nodes = nodes;
        this.simplexType = type;
        
        int[] nI = new int[nNodes];
        for (int i = 0; i < nI.length; i++) {
            nI[i] = i; // obvious...
        }
        this.nodeIndices = nI;
        nodesSubsequent = true; // always?
    }

    /**
     * initialize requiredNeighbours array with only direct neighbours.
     */
    private void initRequiredNeighbours() {
        int[][] reqNeighbours = new int[nNodes][];
        for (int i = 0; i < nNodes; i++) {
            int nodeIndex = nodeIndices[i];
            reqNeighbours[i] = getDirectNeighboursForNode(nodeIndex);
        }
        requiredNeighbours = reqNeighbours;
    }

    /**
     * Count maximum amount of potential neighbours for node based on the simplex
     * type.
     *
     * loop over the nodes, adding number of potential neighbours (depending on
     * the type of the simplex) to the referring index of the "tmp" array
     * objective - allocation of the temporary array
     *
     *
     * @param nodes - original array of nodes (with duplicates)
     * @param nNodes
     * @param type
     * @return
     */
    protected int[] maxNeighboursCount(int[] nodes, int nNodes, CellType type) {
        int[] maxPotentialNeighbours = new int[nNodes];
        int nPotentialNeighbours = type.getNVertices() - 1; //number of potential neighbours, different for different CellTypes
        for (int i = 0; i < nodes.length; i++) {
            maxPotentialNeighbours[nodes[i]] += nPotentialNeighbours;
        }
        return maxPotentialNeighbours;
    }

    /**
     * static allocation of neighbours array based on maximum number of
     * neighbours based on the simplex type.
     *
     * @param nNodes
     * @param maxPotentialNeighbours
     * @return
     */
    protected static int[][] allocateMaxNeighbours(int nNodes, int[] maxPotentialNeighbours) {
        if (maxPotentialNeighbours.length != nNodes) {
            throw new InternalError("INTERNAL ERROR - max potential neighbours.length should be equal to nNodes length");
        }
        int[][] neighbours = new int[nNodes][];
        for (int i = 0; i < neighbours.length; i++) {
            neighbours[i] = new int[maxPotentialNeighbours[i]];
        }
        return neighbours;
    }

    protected int[][] fill(int[][] neighbours, int[] nodes, CellType type) {
        int nVertices = type.getNVertices(); // number of vertices in the particular simplex
        int[] counter = new int[nNodes];
        Arrays.fill(counter, 0);

        //loop over cells
        int nCells = nodes.length / nVertices;
        int dbg_cnt = 0;
        for (int i = 0; i < nCells; i++) {
            //temporary array to store node indices of the processed cell
            int[] cellVertices = new int[nVertices];
            for (int j = 0; j < nVertices; j++) {
                cellVertices[j] = nodes[j + i * nVertices]; // real node ID (NOT POSITION)
            }
            //processing each node index and assigning the values to the returned array "neighbours"
            for (int k = 0; k < nVertices; k++) {
                for (int l = 0; l < nVertices; l++) {
                    if (k != l) {
                        int nodeIndex = cellVertices[k];
                        int newNeighbourIndex = cellVertices[l];
                        neighbours[nodeIndex][counter[nodeIndex]] = newNeighbourIndex;
                        counter[nodeIndex]++;
                        dbg_cnt++;
                    }
                }
            }
        }
        return neighbours;
    }

    /**
     * count -> allocate -> fill -> sort -> removeDuplicates
     *
     * more efficient than using Collections
     *
     */
    public void calculateNodeNeigbours() {
        CellType type = this.simplexType;

        int[] potentialNeighboursCount = maxNeighboursCount(this.nodes, nNodes, type); //static allocation based on counting of the potential neighbours
        int[][] neighbours2D = allocateMaxNeighbours(nNodes, potentialNeighboursCount); // allocate 2D array for neighbours
        neighbours2D = fill(neighbours2D, nodes, type); // find neighbours for each node (duplicates here)
        neighbours2D = ArrayUtils.removeDuplicates2D(neighbours2D); // clear duplicates 1st step 
        neighbours2D = ArrayUtils.removeNegatives2D(neighbours2D); // clear duplicates 2nd step

        //nodeNeighbours - 1D array of sequentially positioned subarrays
        //iNb - array of 'metadata' for nodeNeighbours; informs at which positions of nodeNeighbours array
        //do the consecutive vectors of neighbours start and end
        int counter = 0;
        int[] directNeighboursInfoTmp = new int[neighbours2D.length + 1];
        directNeighboursInfoTmp[0] = counter;
        for (int i = 0; i < neighbours2D.length; i++) {
            counter += neighbours2D[i].length;
            directNeighboursInfoTmp[i + 1] = counter;
        }
        this.directNeighboursInfo = directNeighboursInfoTmp;

        // flatten neighbours array
        this.directNeighbours = new int[counter];
        counter = 0;
        for (int[] singleNodeNeighbours : neighbours2D) {
            for (int j = 0; j < singleNodeNeighbours.length; j++) {
                this.directNeighbours[counter++] = singleNodeNeighbours[j];
            }
        }
        //at the end - init required neighbours
        initRequiredNeighbours();
    }

    /**
     * Add additional neighbours in case there is not sufficient amount/singular
     * matrix].
     *
     * currentNeighbours is array of node indices of currently calculated
     * neighbours (either direct or recalculated after adding new later(
     *
     * @param nodeIndex
     * @param neighboursFlattened
     * @return
     */
    public int[] subsequentNeighboursForNode(int nodeIndex, int[] neighboursFlattened) {
        int[][] newNeighbours2D = new int[neighboursFlattened.length][];
        for (int i = 0; i < newNeighbours2D.length; i++) {
            newNeighbours2D[i] = this.getDirectNeighboursForNode(neighboursFlattened[i]);
        }
        int[] newNeighbours1D = ArrayUtils.array2DTo1D(newNeighbours2D);
        for (int j = 0; j < newNeighbours1D.length; j++) {
            if (newNeighbours1D[j] == nodeIndex) {
                newNeighbours1D[j] = -1;
            }
        }
        Arrays.sort(newNeighbours1D);
        newNeighbours1D = ArrayUtils.removeDuplicates1D(newNeighbours1D);
        newNeighbours1D = ArrayUtils.removeNegatives1D(newNeighbours1D);
        return newNeighbours1D;
    }

    /**
     * add next neighbour level and append information to class field
     * "requiredNeighbours"
     *
     * @param nodeIndex
     */
    public void addNextNeighboursLayer(int nodeIndex) {
        int[] currentNodeNeighbours = this.getRequiredNeighboursForNode(nodeIndex);
        requiredNeighbours[nodeIndex] = subsequentNeighboursForNode(nodeIndex, currentNodeNeighbours);
    }

    /**
     * Checks whether the number of nodes is sufficient for further processing
     * (6 for Triangles, 10 for Tetrahedrons). If not, add the next row of
     * following neighbours until the condifion is satisfied while modifying the
     * 'requiredNeighbours' array.
     *
     * @param trueNSpace
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public void preprocessNeighbours(int trueNSpace) throws ComputationError {
        int min;
        switch (trueNSpace) {
            case 2:
                min = 6;
                break;
            case 3:
                min = 10;
                break;
            default:
                throw new AssertionError();
        }
        for (int i = 0; i < nNodes; i++) {
            //risk of infinite loop
            int nodeIndex = nodeIndices[i];
            int[] requiredNeighboursForNode = getRequiredNeighboursForNode(nodeIndex);
            int n = requiredNeighboursForNode.length; //number of neighbours
//            LOGGER.debug("Node: index\t"+nodeIndex+"\trequired neighbours\t"+Arrays.toString(requiredNeighboursForNode));
            int counter = 0;
            while (n < min) {
                if (counter > 4) {
                    // TODO this should throw error?
                    String message = String.format(
                            "ERROR! The required amount of neighbours for node %d could not have been reached!\n "
                            + "Unable to compute node neighbours for given field.", i);
                    LOGGER.error(message);
                    throw new ComputationError(message);
                }
                addNextNeighboursLayer(i);
                requiredNeighboursForNode = getRequiredNeighboursForNode(nodeIndex);
                n = requiredNeighboursForNode.length;
//                LOGGER.debug("\tNode: index\t"+nodeIndex+"\trequired neighbours\t"+Arrays.toString(requiredNeighboursForNode));
                counter++;
            }
        }
    }

    /**
     * node index; not position
     *
     * @param nodeIndex
     * @return
     */
    public int[] getDirectNeighboursForNode(int nodeIndex) {
        int[] nodeXDirectNeighbours = Arrays.copyOfRange(this.directNeighbours, this.directNeighboursInfo[nodeIndex], this.directNeighboursInfo[nodeIndex + 1]);
        return nodeXDirectNeighbours;
    }

    /**
     * Get required neighbours for single node.
     *
     * @param nodeIndex
     * @return
     */
    public int[] getRequiredNeighboursForNode(int nodeIndex) {
        int[] nodeXRequiredNeighbours = this.requiredNeighbours[nodeIndex];
        return nodeXRequiredNeighbours;
    }

    public int[] getDirectNeighbours() {
        return directNeighbours;
    }

    public int[] getDirectNeighboursInfo() {
        return directNeighboursInfo;
    }

    public int[][] getRequiredNeighbours() {
        return requiredNeighbours;
    }


    public int[] getNodes() {
        return nodes;
    }

    public int getNNodes() {
        return nNodes;
    }

    public int[] getNeigboursCount() {
        int[] iNb = getDirectNeighboursInfo();
        int[] neighboursCount = new int[iNb.length - 1];
        for (int i = 0; i < iNb.length - 1; i++) {
            neighboursCount[i] = iNb[i + 1] - iNb[i];
        }
        return neighboursCount;
    }
}
