package org.visnow.vn.DiffOpPlugin.DifferentialsCalculator;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action.CALCULATE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.META_SCALAR_COMPONENT_NAMES;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.META_VECTOR_COMPONENT_NAMES;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.RUNNING_MESSAGE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.SCALAR_ACTIONS;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.VECTOR_ACTIONS;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.RunButton.RunState;
import org.visnow.vn.system.swing.FixedGridBagLayoutPanel;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action;

/**
 *
 * @author MMieczkowski
 */
public final class GUI extends FixedGridBagLayoutPanel {

    protected Parameters parameters = null;
    protected boolean tableActive = false;  // this flag is necessary to distinguish user action on jTable from setter actions.

    protected final static Action[] COMPONENT_COLUMN_ORDER = {null, CALCULATE};
    protected final static int[] COMPONENTS_COLUMN_WIDTHS = new int[]{100, 20};
    protected final static String[] SCALAR_COMPONENT_TABLE_TOOLTIPS = {null, "Select scalar components for differentials approximation."};
    protected final static String[] VECTOR_COMPONENT_TABLE_TOOLTIPS = {null, "Select vector components for differentials approximation."};

    // <editor-fold defaultstate="collapsed" desc="Scalar components table">
    protected DefaultTableModel scalarComponentsTableModel = new DefaultTableModel(new Object[][]{{null, null}},
            new String[]{"Component", "Select"}) {
        Class[] types = new Class[]{java.lang.String.class, java.lang.Boolean.class};
        boolean[] canEdit = new boolean[]{false, true};

        @Override
        public Class getColumnClass(int columnIndex) {
            return types[columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };

    protected javax.swing.JTable scalarComponentTable;

    protected JTableHeader createScalarComponentTableHeader() {
        return new JTableHeader(scalarComponentTable.getColumnModel()) {
            @Override
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realIndex = columnModel.getColumn(index).getModelIndex();
                return SCALAR_COMPONENT_TABLE_TOOLTIPS[realIndex];
            }
        };
    }
    ;

    protected TableModelListener scalarComponentTableListener = new TableModelListener() {
        @Override
        public void tableChanged(TableModelEvent e) {

            if (scalarComponentTable.getModel().getRowCount() == parameters.get(META_SCALAR_COMPONENT_NAMES).length) {
                Action[] actions = new Action[parameters.get(META_SCALAR_COMPONENT_NAMES).length];
                for (int row = 0; row < actions.length; row++) {
                    int column = 1;
                    if ((Boolean) scalarComponentTable.getModel().getValueAt(row, column)) {
                        actions[row] = COMPONENT_COLUMN_ORDER[column];
                    }
                }
                if (tableActive) {
                    runButton.setPendingIfNoAuto();
                }
                parameters.set(SCALAR_ACTIONS, actions);
            }
        }
    };// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Vector components table">
    protected DefaultTableModel vectorComponentsTableModel = new DefaultTableModel(new Object[][]{{null, null}},
            new String[]{"Component", "Select"}) {
        Class[] types = new Class[]{java.lang.String.class, java.lang.Boolean.class};
        boolean[] canEdit = new boolean[]{false, true};

        @Override
        public Class getColumnClass(int columnIndex) {
            return types[columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };

    protected javax.swing.JTable vectorComponentTable;

    protected JTableHeader createVectorComponentTableHeader() {
        return new JTableHeader(vectorComponentTable.getColumnModel()) {
            @Override
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realIndex = columnModel.getColumn(index).getModelIndex();
                return VECTOR_COMPONENT_TABLE_TOOLTIPS[realIndex];
            }
        };
    }
    ;
    
    protected TableModelListener vectorComponentTableListener = new TableModelListener() {
        @Override
        public void tableChanged(TableModelEvent e) {

            if (vectorComponentTable.getModel().getRowCount() == parameters.get(META_VECTOR_COMPONENT_NAMES).length) {
                Action[] actions = new Action[parameters.get(META_VECTOR_COMPONENT_NAMES).length];
                for (int row = 0; row < actions.length; row++) {
                    int column = 1;
                    if ((Boolean) vectorComponentTable.getModel().getValueAt(row, column)) {
                        actions[row] = COMPONENT_COLUMN_ORDER[column];
                    }
                }
                if (tableActive) {
                    runButton.setPendingIfNoAuto();
                }
                parameters.set(VECTOR_ACTIONS, actions);
            }
        }
    };
    // </editor-fold>

    /**
     * Creates new form GUI
     */
    // <editor-fold defaultstate="collapsed" desc="GUI">
    public GUI() {
        initComponents();

        scalarComponentTable = new javax.swing.JTable();
        scalarComponentTable.setModel(scalarComponentsTableModel);
        for (int i = 0; i < COMPONENTS_COLUMN_WIDTHS.length; i++) {
            scalarComponentTable.getColumnModel().getColumn(i).setPreferredWidth(COMPONENTS_COLUMN_WIDTHS[i]);
        }
        scalarComponentTable.getTableHeader().setReorderingAllowed(false);
        scalarComponentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        ((DefaultTableModel) scalarComponentTable.getModel()).addTableModelListener(scalarComponentTableListener);
        JPanel scalarComponentTablePanel = new JPanel();

        scalarComponentTablePanel.setLayout(new BorderLayout());
        scalarComponentTablePanel.add(scalarComponentTable, BorderLayout.CENTER);
        scalarComponentTablePanel.add(createScalarComponentTableHeader(), BorderLayout.NORTH);
        scalarJScrollPane.getViewport().add(scalarComponentTablePanel);

        vectorComponentTable = new javax.swing.JTable();
        vectorComponentTable.setModel(vectorComponentsTableModel);
        for (int i = 0; i < COMPONENTS_COLUMN_WIDTHS.length; i++) {
            vectorComponentTable.getColumnModel().getColumn(i).setPreferredWidth(COMPONENTS_COLUMN_WIDTHS[i]);
        }
        vectorComponentTable.getTableHeader().setReorderingAllowed(false);
        vectorComponentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        ((DefaultTableModel) vectorComponentTable.getModel()).addTableModelListener(vectorComponentTableListener);
        JPanel vectorComponentTablePanel = new JPanel();

        vectorComponentTablePanel.setLayout(new BorderLayout());
        vectorComponentTablePanel.add(vectorComponentTable, BorderLayout.CENTER);
        vectorComponentTablePanel.add(createVectorComponentTableHeader(), BorderLayout.NORTH);
        vectorJScrollPane.getViewport().add(vectorComponentTablePanel);

        runButton.setPendingIfNoAuto();
    }// </editor-fold>

    /**
     * @param p cloned parameters
     * @param resetFully
     * @param setRunButtonPending
     */
    public void updateGUI(final ParameterProxy p, boolean resetFully, boolean setRunButtonPending) {
        tableActive = false;

        scalarPanel.setVisible(p.get(META_SCALAR_COMPONENT_NAMES).length > 0);
        DefaultTableModel scalarComponentTableModel = (DefaultTableModel) scalarComponentTable.getModel();
        scalarComponentTableModel.setRowCount(0);

        Object[] row = new Object[COMPONENT_COLUMN_ORDER.length];
        for (int i = 0; i < p.get(META_SCALAR_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_SCALAR_COMPONENT_NAMES)[i];
            Action action = p.get(SCALAR_ACTIONS)[i];
            if (action == CALCULATE) {
                row[1] = true;
            }
            scalarComponentTableModel.addRow(row);
        }

        vectorPanel.setVisible(p.get(META_VECTOR_COMPONENT_NAMES).length > 0);
        DefaultTableModel vectorComponentTableModel
                = (DefaultTableModel) vectorComponentTable.getModel();
        vectorComponentTableModel.setRowCount(0);

        row = new Object[COMPONENT_COLUMN_ORDER.length];
        for (int i = 0; i < p.get(META_VECTOR_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_VECTOR_COMPONENT_NAMES)[i];
            Action action = p.get(VECTOR_ACTIONS)[i];
            if (action == CALCULATE) {
                row[1] = true;
            }
            vectorComponentTableModel.addRow(row);
        }

        tableActive = true;

        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        filler = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        componentsSelectionPanel = new javax.swing.JPanel();
        vectorPanel = new javax.swing.JPanel();
        vectorJScrollPane = new javax.swing.JScrollPane();
        scalarPanel = new javax.swing.JPanel();
        scalarJScrollPane = new javax.swing.JScrollPane();

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                runButtonUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(filler, gridBagConstraints);

        componentsSelectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "COMPONENTS SELECTION", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        componentsSelectionPanel.setLayout(new java.awt.GridBagLayout());

        vectorPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("VECTOR COMPONENTS"));
        vectorPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        vectorPanel.add(vectorJScrollPane, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        componentsSelectionPanel.add(vectorPanel, gridBagConstraints);

        scalarPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("SCALAR COMPONENTS"));
        scalarPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        scalarPanel.add(scalarJScrollPane, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        componentsSelectionPanel.add(scalarPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(componentsSelectionPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameters.set(RUNNING_MESSAGE, (RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel componentsSelectionPanel;
    private javax.swing.Box.Filler filler;
    private org.visnow.vn.gui.widgets.RunButton runButton;
    private javax.swing.JScrollPane scalarJScrollPane;
    private javax.swing.JPanel scalarPanel;
    private javax.swing.JScrollPane vectorJScrollPane;
    private javax.swing.JPanel vectorPanel;
    // End of variables declaration//GEN-END:variables

}
