//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package org.visnow.vn.DiffOpPlugin.DifferentialsCalculator;

import java.util.Arrays;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.visnow.vn.DiffOpPlugin.ApproximateDifferentials;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.RUNNING_MESSAGE;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.gui.widgets.RunButton.RunState.NO_RUN;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_ONCE;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.META_SCALAR_COMPONENT_NAMES;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.META_VECTOR_COMPONENT_NAMES;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.SCALAR_ACTIONS;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.VECTOR_ACTIONS;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;
import org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException;
import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * Module responsible for differentials approximation for data components in
 * irregular geometry fields (up to 2nd order derivative).
 *
 ** @author MMieczkowski
 */
public class DifferentialsCalculator extends OutFieldVisualizationModule {

    private static final Logger LOGGER = Logger.getLogger(DifferentialsCalculator.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected Field inField = null;
    protected IrregularField triangulatedInField = null;

    private GUI computeUI = null;
    private int runQueue = 0;

    public DifferentialsCalculator() {

        parameters.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                    startIfNotInQueue();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                outObj.setName("cell centers");
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters() {
        return new Parameter[]{
            new Parameter<>(META_SCALAR_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_VECTOR_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(SCALAR_ACTIONS, new Action[]{}),
            new Parameter<>(VECTOR_ACTIONS, new Action[]{}),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    private Action[] findActions(ParameterName actionsName, ParameterName metaName, String[] newComponentNames, boolean resetParameters) {
        boolean isFromVNA = isFromVNA();

        Action[] actions = DiffOpPluginShared.findActions(parameters, actionsName, metaName, newComponentNames, resetParameters, isFromVNA);
        return actions;
    }

    private Action[] getUpdatedScalarActions(String[] newScalarComponentNames, boolean resetParameters) {
        return findActions(SCALAR_ACTIONS, META_SCALAR_COMPONENT_NAMES, newScalarComponentNames, resetParameters);
    }

    private Action[] getUpdatedVectorActions(String[] newVectorComponentNames, boolean resetParameters) {
        return findActions(VECTOR_ACTIONS, META_VECTOR_COMPONENT_NAMES, newVectorComponentNames, resetParameters);
    }

    private void validateParamsAndSetSmart(boolean resetParameters) {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
            parameters.set(RUNNING_MESSAGE, NO_RUN);
        }

        String[] newScalarComponentNames = inField.getScalarComponentNames();
        String[] newVectorComponentNames = inField.getVectorComponentNames();

        Action[] newScalarAction = getUpdatedScalarActions(newScalarComponentNames, resetParameters);
        Action[] newVectorAction = getUpdatedVectorActions(newVectorComponentNames, resetParameters);

        parameters.set(SCALAR_ACTIONS, newScalarAction);
        parameters.set(VECTOR_ACTIONS, newVectorAction);
        parameters.set(META_SCALAR_COMPONENT_NAMES, newScalarComponentNames);
        parameters.set(META_VECTOR_COMPONENT_NAMES, newVectorComponentNames);

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending) {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive() {
        if (getInputFirstValue("inField") != null) {

            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            // TODO different field
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getComponentNames(), newInField.getComponentNames()));
//            boolean isDifferentType = ((inField == null) != (newInField == null)) || ((inField instanceof IrregularField) != (newInField instanceof IrregularField));
//            boolean bothIrregular = (inField != null) && (newInField != null) && inField instanceof IrregularField && newInField instanceof IrregularField;

            boolean isNewField = !isFromVNA() && newInField != inField;
            boolean resetParameters = isNewField; // TODO when to reset parameters??
            inField = newInField;
            triangulatedInField = inField.getTriangulated();

            // TODO
            // new field does not always mean to reset parameters!
            // i.e. newField directly from module with parameters already set!
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(resetParameters); //TO ASK!
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, isFromVNA(), isFromVNA() || isNewField);
            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data

                HashMap<String, Action> toCalculate = new HashMap<>();
                String[] scalarComponents = p.get(META_SCALAR_COMPONENT_NAMES);
                String[] vectorComponents = p.get(META_VECTOR_COMPONENT_NAMES);

                for (int i = 0; i < scalarComponents.length; i++) {
                    String component = scalarComponents[i];
                    toCalculate.put(component, p.get(SCALAR_ACTIONS)[i]);
                }

                for (int i = 0; i < vectorComponents.length; i++) {
                    String component = vectorComponents[i];
                    toCalculate.put(component, p.get(VECTOR_ACTIONS)[i]);
                }

                ApproximateDifferentials differentialsApproximation;
                try {
                    differentialsApproximation = new ApproximateDifferentials(triangulatedInField, toCalculate);
                    differentialsApproximation.computeDifferentialOperators();
                } catch (MultipleCellSetsException | ComputationError ex) {
                    DiffOpPluginShared.handleExceptionVNApp(ex, LOGGER);
                    return;
                }
                //optional - number of neighbours
                differentialsApproximation.assignNrDirNeighbours();
                differentialsApproximation.assignNrReqNeighbours();
                outIrregularField = differentialsApproximation.getField();

                outObj.clearAllGeometry();
                outGroup = null;
                outObj.setName(inField.getName());
                outField = outIrregularField;

                setOutputValue("outField", new VNIrregularField(outIrregularField));

                prepareOutputGeometry();
                show();
            }

        }
    }

}
