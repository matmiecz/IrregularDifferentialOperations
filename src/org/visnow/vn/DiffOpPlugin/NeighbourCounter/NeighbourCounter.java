//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package org.visnow.vn.DiffOpPlugin.NeighbourCounter;

import org.apache.log4j.Logger;
import org.visnow.vn.DiffOpPlugin.NodeNeighbours;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared;
import org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException;
import org.visnow.vn.DiffOpPlugin.VNUtils;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;

/**
 * Calculation of the direct neighbours in the given field.
 *
 ** @author MMieczkowski
 */
//TODO - multiple CellSets
public class NeighbourCounter extends OutFieldVisualizationModule {

    private static final Logger LOGGER = Logger.getLogger(NeighbourCounter.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField = null;
    protected IrregularField triangulatedInField = null;
    protected int trueNSpace = -1;

    public NeighbourCounter() {
        setPanel(ui);
        outObj.setName("cell centers");
    }

    public void computeNodeNeighbours(int trueNSpace) throws MultipleCellSetsException {
        CellType cellType;
        switch (trueNSpace) {
            case 2:
                cellType = CellType.TRIANGLE;
                break;
            case 3:
                cellType = CellType.TETRA;
                break;
            case 1:
                throw new UnsupportedOperationException("1D cases not supported.");
            case -1:
                throw new UnsupportedOperationException("Cases with trueNSpace = -1 not supported.");
            default:
                throw new IllegalArgumentException("Error. Computation failed - trueNSpace not found.");
        }

        if (triangulatedInField == null) {
            throw new NullPointerException("Triangulated field has not been initialized.");
        }
        if (triangulatedInField.getNCellSets() == 1) {
            CellSet cs = triangulatedInField.getCellSet(0);
            // which one ?
            long nNodes = cs.getNActiveNodes(); // TODO ask - why "active nodes" (engine error) 
            nNodes = triangulatedInField.getNElements();
            // todo ? maybe restrict that other cell arrays have to be null...
            CellArray nodesArray = cs.getCellArray(cellType);
            int[] nodes = nodesArray.getNodes();
            NodeNeighbours neighbours = new NodeNeighbours((int) nNodes, nodes, cellType);
            neighbours.calculateNodeNeigbours();
            int[] neighboursCount = neighbours.getNeigboursCount();
            DataArray da = DataArray.create(neighboursCount, 1, "n_neighbours");
            // dataArray.getNElements() != getNElements()
            da.getNElements();
            triangulatedInField.addComponent(da);
            outIrregularField = triangulatedInField;
//            outIrregularField.addComponent(DataArray.create(neighboursCount, 1, "n_neighbours"));
        } else {
            throw new MultipleCellSetsException();
        }

    }

    @Override
    public void onActive() {
        if (getInputFirstValue("inField") == null) {
            return;
        }
        VNField input = ((VNField) getInputFirstValue("inField"));
        Field newInField = input.getField();

        if (newInField != null && inField != newInField) {
            inField = newInField;
            triangulatedInField = inField.getTriangulated();
            
            try {
                triangulatedInField = VNUtils.extractSingleCellSetField(triangulatedInField, VNUtils.getSimplexType(triangulatedInField));
            } catch (Exception ex) {
                DiffOpPluginShared.handleExceptionVNApp(ex, LOGGER);
            }
            
            trueNSpace = triangulatedInField.getTrueNSpace();

            try {
                computeNodeNeighbours(trueNSpace);
            } catch (MultipleCellSetsException ex) {
                String messageTitle = ex.getMessage();
                LOGGER.error(messageTitle);
                VisNow.get().userMessageSend(
                        new UserMessage("VisNow", "Neighbour Counter module", messageTitle, "", Level.ERROR));
                return;
            }

            outObj.clearAllGeometry();
            outGroup = null;
            outObj.setName(inField.getName());
            outIrregularField = triangulatedInField;
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
        }
        setOutputValue("outField", new VNIrregularField(outIrregularField));
    }
}
