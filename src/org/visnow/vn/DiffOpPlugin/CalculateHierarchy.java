package org.visnow.vn.DiffOpPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;
import org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException;
import org.visnow.vn.DiffOpPlugin.FieldDecimation.FieldDecimation;
import org.visnow.vn.lib.utils.numeric.HeapSort;

/**
 * TODO - multiple CellSets
 *
 * @author MMieczkowski
 */
public class CalculateHierarchy {

    private static final Logger LOGGER = Logger.getLogger(FieldDecimation.class);

    private final IrregularField inField;
    private final CellSet cellSet;

    private byte[] hierarchyArray;
    private Map<Byte, Integer> nByHierarchy;

    // information from object after calculation
    private double filteringAngle = -1.0f;
    private double criticalAngle = -1.0f;

    private CellSet cellSetSeparated;

    // PARAMETERS 
    // split by hierarchy - splitting corners takes long time to compute (i.e. 
    // for engine data 1800s vs 25s when false) - TODO optimize
    private final boolean SPLIT_CORNERS = false; // should it not be a GUI parameter?
//    private final boolean SPLIT_CORNERS = true; // should it not be a GUI parameter?
    private final int MIN_EDGE_SIZE = 3;

    /**
     * multiple CellSets - TODO
     *
     * @param inField
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException
     */
    public CalculateHierarchy(IrregularField inField) throws MultipleCellSetsException {
        this.inField = inField;
        if (inField.getCellSets().size() == 1) {
            this.cellSet = inField.getCellSet(0);
        } else {
            throw new MultipleCellSetsException();
        }
        this.nByHierarchy = null;
    }

    public CellArray[] getBoundaryCellArrays() {
        return this.cellSet.getBoundaryCellArrays();
    }

    public CellArray[] getCellArrays() {
        return this.cellSet.getCellArrays();
    }

    public FloatLargeArray getCoords() {
        return this.inField.getCurrentCoords();
    }

    public int getNNodes() {
        return (int) inField.getNNodes();
    }

    public byte[] getHierarchyArray() {
        return hierarchyArray;
    }

    public double getFilteringAngle() {
        return filteringAngle;
    }

    public double getCriticalAngle() {
        return criticalAngle;
    }

    public void setFilteringAngle(double filteringAngle) {
        this.filteringAngle = filteringAngle;
    }

    public void setCriticalAngle(double criticalAngle) {
        this.criticalAngle = criticalAngle;
    }

    public CellSet getCellSetSeparated() {
        return cellSetSeparated;
    }

    public int[] getConnectedComponents() {
        return connectedComponents;
    }
    private int[] connectedComponents;

    /**
     * Return node numbers only for selected hierarchy.
     *
     * @param hierarchy
     * @return int array with indices of nodes for selected hierarchy value.
     *
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public int[] getNodesByHierarchy(byte hierarchy) throws MultipleCellSetsException, ComputationError {
        byte[] hierarchyArr = this.hierarchyArray;

        int n = ArrayUtils.countByteOccurences(hierarchyArr, hierarchy);
        int[] filteredNodes = new int[n];
        int index = 0;

        CellType simplexType = VNUtils.getSimplexType(inField);

        int[] allNodes = VNUtils.getNodeIndicesForSimplexType(inField, simplexType);
        if (allNodes.length != hierarchyArr.length) {
            throw new ComputationError("nodes array length does not match hierarchyArray length.");
        }
        if (hierarchyArr != null) {
            for (int i = 0; i < hierarchyArr.length; i++) {
                if (hierarchyArr[i] == hierarchy) {
                    filteredNodes[index] = allNodes[i];
                    index++;
                }
            }
        } else {
            throw new NullPointerException("Hierarchy array has not been calculated yet.");
        }
        return filteredNodes;
    }

    /**
     * Return number of elements in given field for selected hierarchy.
     *
     * @param hierarchy
     *
     * @return
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public int getNNodesByHierarchy(byte hierarchy) throws MultipleCellSetsException, ComputationError {
        if (nByHierarchy == null) {
            Map<Byte, Integer> nByHierarchyTmp = new HashMap<>();
            byte[] HIERARCHIES = Hierarchy.getAllValidHierarchyValues();
            // calculate
            for (byte h : HIERARCHIES) {
                nByHierarchyTmp.put(h, getNodesByHierarchy(h).length);
            }
            nByHierarchy = nByHierarchyTmp;
        }
        int n = nByHierarchy.get(hierarchy);
        return n;
    }

    public byte getHierarchy(int nodePosition) {
        return hierarchyArray[nodePosition];
    }

    public IrregularField getField() {
        return inField;
    }

    public CellSet getCellSet() {
        return cellSet;
    }

    private int[] getBoundaryPoints() {
        CellArray boundaryTriangles = getBoundaryCellArrays()[CellType.TRIANGLE.getValue()];
        CellArray boundaryQuads = getBoundaryCellArrays()[CellType.QUAD.getValue()];
        int[] boundaryPointsFromTri = null;
        int[] boundaryPointsFromQuad = null;
        int[] boundaryPoints = null;

        if (boundaryTriangles != null) {
            boundaryPointsFromTri = boundaryTriangles.getNodes().clone();
            Arrays.sort(boundaryPointsFromTri);
            boundaryPointsFromTri = ArrayUtils.removeDuplicates1D(boundaryPointsFromTri);
        }
        if (boundaryQuads != null) {
            boundaryPointsFromQuad = boundaryQuads.getNodes().clone();
            Arrays.sort(boundaryPointsFromQuad);
            boundaryPointsFromQuad = ArrayUtils.removeDuplicates1D(boundaryPointsFromQuad);
        }
        if (boundaryPointsFromTri == null && boundaryPointsFromQuad != null) {
            boundaryPoints = boundaryPointsFromQuad;
        } else if (boundaryPointsFromTri != null && boundaryPointsFromQuad == null) {
            boundaryPoints = boundaryPointsFromTri;
        } else if (boundaryPointsFromTri != null && boundaryPointsFromQuad != null) {
            int n1 = boundaryPointsFromTri.length;
            int n2 = boundaryPointsFromQuad.length;
            int[] temp = Arrays.copyOf(boundaryPointsFromTri, n1 + n2);
            System.arraycopy(boundaryPointsFromQuad, 0, temp, n1, n2);
            Arrays.sort(temp);
            temp = ArrayUtils.removeDuplicates1D(temp);
            boundaryPoints = temp;
        } else {
            throw new IllegalArgumentException("No boundaries found!");
        }
        return boundaryPoints;
    }

    private void setHierarchySingleNode(int nodeNumber, byte hierarchyIndex) {
        hierarchyArray[nodeNumber] = hierarchyIndex;
    }

    /**
     * Filters segments based on their dihedrals.
     *
     * If the dihedral of the segment exceeds given threshold, then the segment
     * is considered as edge.
     *
     * @param filteringThreshold
     * @param dihedrals
     * @return int[] array of segment indices to be considered as edges
     */
    private int[] filterSegments(float filteringThreshold, float[] dihedrals) {
        int[] filteredSegmentsTmp = new int[dihedrals.length];
        int counter = 0;
        for (int i = 0; i < dihedrals.length; i++) {
            if (dihedrals[i] > filteringThreshold) {
                filteredSegmentsTmp[counter++] = i;
            }
        }
        // cut filteredSegments
        int[] filteredSegments = Arrays.copyOf(filteredSegmentsTmp, counter);
        return filteredSegments;
    }

    private void assignHierarchy() {
        IrregularField irrField = getField();
        irrField.addComponent(DataArray.create(hierarchyArray, 1, "Hierarchy"));
    }

    /**
     * Calculate angle between two segments (in degrees).
     *
     * @param indexSegment1 - first segment index
     * @param indexSegment2 - second segment index
     * @param boundarySegments
     * @return
     */
    private double calculateAngleBetweenSegments(int indexSegment1, int indexSegment2, CellArray boundarySegments) {
        int[] vertices = new int[4];
        vertices[0] = boundarySegments.getCell(indexSegment1).getVertices()[0];
        vertices[1] = boundarySegments.getCell(indexSegment1).getVertices()[1];
        vertices[2] = boundarySegments.getCell(indexSegment2).getVertices()[0];
        vertices[3] = boundarySegments.getCell(indexSegment2).getVertices()[1];

        double[][] vecCoords = new double[4][3];

        for (int i = 0; i < vecCoords.length; i++) {
            for (int j = 0; j < vecCoords[i].length; j++) {
                vecCoords[i][j] = getCoords().getDouble(vertices[i] * 3 + j);
            }
        }
        double[] vec1 = ArrayUtils.createVector(vecCoords[0], vecCoords[1]);
        double[] vec2 = ArrayUtils.createVector(vecCoords[2], vecCoords[3]);
        double angle = ArrayUtils.calculateAngle(vec1, vec2);
        return angle;
    }

    /**
     * Most important method in the module = calculate hierarchy of nodes in
     * IrregularField passed in class constructor.
     *
     * Algorithm:
     * <p>
     * <ul>
     * <li> 1) get all boundary points and segments IDs
     * <li> 2) assign hierarchy = 0 for each node
     * <li> 3) assign hierarchy = 1 for each node at boundary
     * <li> 4) find edges based on the dihedrals threshold - > edge segments
     * (unique) and their nodes
     * <li> 5) loop over each unique node how many edge segments contain it
     * <li> 6) for 1 and 3+ set hierarchy=3; for 2 set hierarchy=2
     * </ul><p>
     *
     *
     * @param filteringAngle
     * @param criticalAngle if negative value then this part of method is
     * omitted (critical angle not relevant whilst calculating hierarchy)
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public void calculateHierarchyOfNodes(double filteringAngle, double criticalAngle) throws MultipleCellSetsException, ComputationError {
        int[] boundaryPoints = getBoundaryPoints();
        CellArray boundarySegments = getBoundaryCellArrays()[CellType.SEGMENT.getValue()];
        int nBoundaryPoints = boundaryPoints.length;
        int nBoundarySegments = boundarySegments.getNCells();

        //array of hierarchy coefficients reffering to node number
        //assertion -> node indices are set one after another
        byte[] tmpHierarchy = new byte[getNNodes()];
        //all the interior nodes -> lewest hierarchy coefficient (INSIDE)
        Arrays.fill(tmpHierarchy, (byte) Hierarchy.INSIDE.getValue());
        //all the nodes at the external faces -> hierarchy coefficient min SURFACE 
        for (int i = 0; i < boundaryPoints.length; i++) {
            tmpHierarchy[boundaryPoints[i]] = (byte) Hierarchy.SURFACE.getValue();
        }
        hierarchyArray = tmpHierarchy;

        //filter segments based on given dihedral threshold: 
        //if the threshold is exceeded, then the segment is considered as edge
        // edgeSegments - indices of Edges
        // edgeSegmentsNodes - indices of nodes in Segments
        float[] dihedrals = boundarySegments.getCellDihedrals();
        int[] edgeSegments = filterSegments((float) filteringAngle, dihedrals);
        int[] edgeSegmentsNodes = new int[edgeSegments.length * 2];
        for (int i = 0; i < edgeSegments.length; i++) {
            int[] segment = boundarySegments.getCell(edgeSegments[i]).getVertices();
            System.arraycopy(segment, 0, edgeSegmentsNodes, 2 * i, 2);
        }
        //modifying hierarchyArray field by adding boundary nodes(after dihedral filtering) hierarchy:
        // Hierarchy.EDGE - majority of all the nodes at the edges apart from those that would be considered as Hierarchy.VERTEX
        // 
        // Hierarchy.VERTEX - corner points and nodes that form segments above selected critical angle
        int[] uniquePointsAtEdges = Arrays.copyOf(edgeSegmentsNodes, edgeSegmentsNodes.length);
        Arrays.sort(uniquePointsAtEdges);
        uniquePointsAtEdges = ArrayUtils.removeDuplicates1D(uniquePointsAtEdges);

        // segment count
        //counts how many boundary segments does the processed node contain
        //check segments that contain each of unique points at edges 
        // map (nodeAtEdgeIndex - segment indices)
        Map<Integer, List<Integer>> segmentCount = new HashMap<>();

        for (int uniqueEdgePoint : uniquePointsAtEdges) {
            // initial allocated size 10 - should not be exceeded
            segmentCount.put(uniqueEdgePoint, new ArrayList<>(10));
        }

        for (int i = 0; i < edgeSegmentsNodes.length; i++) {
            if (segmentCount.containsKey(edgeSegmentsNodes[i])) {
                segmentCount.get(edgeSegmentsNodes[i]).add(edgeSegments[(i / 2)]); // rounding ?
            }

        }

        debugAddEdgeNodesToOutput(uniquePointsAtEdges);
        // ---------------------------------------------------------------------

        // STEP 1 - SETTING HIERARCHY BASED ONLY ON SEGMENTS AMOUNT FROM NODE
        // ---------------------------------------------------------------------
        for (Map.Entry<Integer, List<Integer>> entry : segmentCount.entrySet()) {
            int count = entry.getValue().size();
            int node = entry.getKey();
            if (count == 2) {
                setHierarchySingleNode(node, Hierarchy.EDGE.getValue());
            } else if (count == 1 || count > 2) {
                setHierarchySingleNode(node, Hierarchy.VERTEX.getValue());
            } else {
                throw new IllegalArgumentException("Some error occured with calculation of node " + node);
            }
        }

        getField().addComponent(DataArray.create(hierarchyArray.clone(), 1, "hierarchyBasedOnSegmentsAmount_Step1"));

        // find edges from vertex to vertex
        List<List<Integer>> edges;
        // structure - ArrayList of LinkedLists?
        // edges = new ArrayList<>(); // new ArrayList<List<Integer>>();

        if (criticalAngle > 0.0f) {
            // create segment pairs:
            // pair -> 3 integers: (segment1, segment2, commonNodeIndex)
            int[][] segmentPairs2D = new int[uniquePointsAtEdges.length][];
            for (int i = 0; i < uniquePointsAtEdges.length; i++) {
                // create pairs
                //node -> index of the common node
                int node = uniquePointsAtEdges[i];
                List<Integer> segments = segmentCount.get(node);

                // how many pairs
                int nPairs = 0;
                for (int j = 1; j < segments.size(); j++) {
                    nPairs += j;
                }
                int[] pairs = new int[3 * nPairs];
                int counter = 0;
                for (int j = 0; j < segments.size(); j++) {
                    for (int k = j + 1; k < segments.size(); k++) {
                        pairs[counter++] = segments.get(j);
                        pairs[counter++] = segments.get(k);
                        pairs[counter++] = node;
                    }
                }
                segmentPairs2D[i] = pairs;
            }

            // STEP 2 - SETTING HIERARCHY BASED ON CRITICAL ANGLE BETWEEN TWO SEGMENTS (CORNERS)
            // ---------------------------------------------------------------------
            // set vertex when between segments is larger than critical angle
            double[] angles = new double[segmentPairs2D.length];
            for (int i = 0; i < angles.length; i++) {
                int[] segmentPair = segmentPairs2D[i];
                // add angle only when two segments (cases with more have already been processed - node set as vertex in STEP 1)
                angles[i] = (segmentPair.length == 3) ? calculateAngleBetweenSegments(segmentPair[0], segmentPair[1], boundarySegments) : -1.0;
            }

            for (int i = 0; i < uniquePointsAtEdges.length; i++) {
                if (angles[i] > criticalAngle) {
                    setHierarchySingleNode(segmentPairs2D[i][2], Hierarchy.VERTEX.getValue());
                }
            }

            getField().addComponent(DataArray.create(hierarchyArray.clone(), 1, "hierarchyBasedOnCorners_Step2"));

            // STEP 3 - SETTING HIERARCHY AT CURVATURES
            // ---------------------------------------------------------------------
            // Every n-th node should be treated as vertex
            // - do not process the same edge twice
            // 
            // Find vertices (vertices are those nodes where segmentCount has 
            // length 3 or more). Each edge starts and ends at vertex, adding 
            // to set to avoid running twice the same edge in both directions
            //
            // vertices - Set of node indices with highest hierarchy
            Set<Integer> vertices = new HashSet<>();
            segmentCount.entrySet().forEach((entry) -> {
                List<Integer> endingSegments = entry.getValue();
                if (endingSegments.size() > 2) {
                    vertices.add(entry.getKey());
                }
            });

            edges = findEdgesBetweenVertices(vertices, segmentCount, boundarySegments);
            debugAddEdgesVertexEnding(edges, boundarySegments);
            // --------------------------------------------------------

            // PREPROCESSING
            // find circular edges (no vertex) and assign one vertex hierarchy
            // ---------------------------------------------------------------------
            CellSet boundaryCellSet = new CellSet();
            CellArray ca = new CellArray(CellType.SEGMENT, edgeSegmentsNodes, null, null);
            boundaryCellSet.addCells(ca);

            // split by hierarchy - splitting corners takes long time to compute
            // CONCEPT = maybe separate module?
            cellSetSeparated = splitCellSetByHierarchy(this.SPLIT_CORNERS);
            connectedComponents = computeConnectedComponents(cellSetSeparated, getField());
            getField().addComponent(DataArray.create(connectedComponents, 1, "connected_components_corners_separated_" + Boolean.toString(SPLIT_CORNERS)));
            // END CONCEPT

            int[] connectedEdges = computeConnectedComponents(boundaryCellSet, getField());
            getField().addComponent(DataArray.create(connectedEdges, 1, "connected edges"));
            // convert connected edges to HashMap (edge_connection_ID = nodes inside)
            Map<Integer, List<Integer>> connectedEdgesMap = new HashMap<>();
            for (int i = 0; i < connectedEdges.length; i++) {
                int connectedEdge = connectedEdges[i];
                if (!connectedEdgesMap.containsKey(connectedEdge)) {
                    connectedEdgesMap.put(connectedEdge, new ArrayList<>());
                }
                connectedEdgesMap.get(connectedEdge).add(i);
            }
            
            connectedEdgesMap.values().removeIf(connections -> connections.size() < this.MIN_EDGE_SIZE);

            // from connected edges' nodes extract those without highest hierarchy vertex
            for (Map.Entry<Integer, List<Integer>> entry : connectedEdgesMap.entrySet()) {
                boolean edgeWithoutVertex = true;
                for (Integer nodeIx : entry.getValue()) {
                    if (getHierarchy(nodeIx) == Hierarchy.VERTEX.getValue()) {
                        edgeWithoutVertex = false;
                        break;
                    }
                }
                // when no vertex found in connected edge -> edge is circular; elevate random node hierarchy
                if (edgeWithoutVertex) {
                    int nodeToElevate = entry.getValue().get(0); // one RANDOM (todo, for reproducibility left 0) node to elevate hierarchy among this group
                    setHierarchySingleNode(nodeToElevate, Hierarchy.VERTEX.getValue());
                    LOGGER.debug("Found circular edge - changed hierarchy of node=" + nodeToElevate);
                    vertices.add(nodeToElevate);
                }
            }

            edges = findEdgesBetweenVertices(vertices, segmentCount, boundarySegments);
            elevateHierarchyAtCurvatures(edges, boundarySegments, criticalAngle);
            getField().addComponent(DataArray.create(hierarchyArray.clone(), 1, "hierarchyBasedOnCurvatureCircular_Step3"));

            debugAddEdgesCircularWithoutVertex(edges, boundarySegments);
            // --------------------------------------------------------

        }

        assignHierarchy();
        setFilteringAngle(filteringAngle);
        setCriticalAngle(criticalAngle);

    }

    private void debugAddEdgeNodesToOutput(int[] uniquePointsAtEdges) {
        // add edge nodes mask (0-1) for ouput field
        // byte array with unique nodes at edges 
        // ---------------------------------------------------------------------
        byte[] edgeNodes = new byte[getNNodes()];
        Arrays.fill(edgeNodes, (byte) 0);
        for (int uniquePointsAtEdge : uniquePointsAtEdges) {
            edgeNodes[uniquePointsAtEdge] = 1;
        }
        getField().addComponent(DataArray.create(edgeNodes, 1, "edgeNodes"));
    }

    private void debugAddEdgesVertexEnding(List<List<Integer>> edges, CellArray boundarySegments) {
        int[] edgesVertexEnding = new int[getNNodes()];
        for (int i = 0; i < edgesVertexEnding.length; i++) {
            edgesVertexEnding[i] = -1;
        }
        int counter = 0;
        for (List<Integer> edgeList : edges) {
            for (Integer edgeSegment : edgeList) {
                for (int node : boundarySegments.getCellVertices(edgeSegment)) {
                    edgesVertexEnding[node] = counter;
                }
            }
            counter++;
        }
        getField().addComponent(DataArray.create(edgesVertexEnding, 1, "edgesWithVertexEnding"));
    }

    private void debugAddEdgesCircularWithoutVertex(List<List<Integer>> edges, CellArray boundarySegments) {
        int[] circularEdges = new int[getNNodes()];
        for (int i = 0; i < circularEdges.length; i++) {
            circularEdges[i] = -1;
        }
        int counter = 0;
        for (List<Integer> edgeList : edges) {
            for (Integer edgeSegment : edgeList) {
                int[] nodes = boundarySegments.getCellVertices(edgeSegment);
                for (int node : nodes) {
                    if (getHierarchy(node) >= 2) {
                        circularEdges[node] = counter;
                    }
                }
            }
            counter++;
        }
        getField().addComponent(DataArray.create(circularEdges, 1, "edgesCircularWithoutVertex"));
    }

    /**
     * Find edges between highest hierarchy nodes.
     *
     * @param vertices - HashMap - (nodeIndex - segments containing this node)
     * @param segmentCount
     * @param boundarySegments
     * @return
     *
     */
    private static List<List<Integer>> findEdgesBetweenVertices(Set<Integer> vertices, Map<Integer, List<Integer>> segmentCount, CellArray boundarySegments) {
        // find edges from vertex to vertex
        // structure - ArrayList of LinkedLists?
        List<List<Integer>> edges = new ArrayList<>(); // new ArrayList<List<Integer>>();

        Set<Integer> processedVertexSegments = new HashSet<>();

        // find edges from each vertex
        for (Integer vertexIndex : vertices) {
            List<Integer> vertexSegments = segmentCount.get(vertexIndex);
            for (int vertexSegment : vertexSegments) {
                // number of iterations = number of possible edges
                if (!processedVertexSegments.contains(vertexSegment)) {
                    List<Integer> edge = new LinkedList<>();

                    int segment = vertexSegment;
                    int[] v = boundarySegments.getCell(segment).getVertices();
                    int lastNode = (v[0] == vertexIndex) ? v[1] : v[0];
                    List<Integer> nextSegments = segmentCount.get(lastNode);

                    edge.add(segment);

                    do {
                        for (int i = 0; i < nextSegments.size(); i++) {
                            // should assume segments are sorted?
                            if (nextSegments.get(i) != segment) {
                                segment = nextSegments.get(i);
                                break;
                            }
                        }
                        v = boundarySegments.getCell(segment).getVertices();
                        // one node should already be, check second
                        lastNode = (lastNode == v[0]) ? v[1] : v[0];
                        // stucks here
                        nextSegments = segmentCount.get(lastNode);
                        edge.add(segment);
                    } while (nextSegments.size() == 2);

                    // dismiss segments shorter than this limit
                    int EDGE_SIZE_LIMIT = 3;
                    if (edge.size() > EDGE_SIZE_LIMIT) {
                        edges.add(edge);
                    }
                    // add first
                    processedVertexSegments.add(vertexSegment);
                    // add last
                    processedVertexSegments.add(segment);
                }
            }
        }
        return edges;
    }

    private void elevateHierarchyAtCurvatures(List<List<Integer>> edges, CellArray boundarySegments, double criticalAngle) {
        // elevate hierarchy to highest possible
        // loop over every edge found
        //      from one segment loop over the rest, if angle is larger than 
        //      criticalAngle then first node of secondSegment is set to be VERTEX
        //
        //      then -> calculate further from this segment on 
        for (List<Integer> edgeList : edges) {
            for (int i = 0; i < edgeList.size(); i++) {
                int firstSegment = edgeList.get(i);
                for (int j = i + 1; j < edgeList.size(); j++) {
                    int nextSegment = edgeList.get(j);
                    double angle = calculateAngleBetweenSegments(firstSegment, nextSegment, boundarySegments);
                    if (angle > criticalAngle) {
                        int node = boundarySegments.getCellVertices(nextSegment)[0];
                        setHierarchySingleNode(node, Hierarchy.VERTEX.getValue());
                        i = j;
                        break;
                    }
                }
            }
        }
    }

    /**
     * Copied directly from ProcessComponents module.
     *
     * @param pNeighb
     * @param neighbInd
     * @param cellArray
     */
    private void addNeighbors(int[] pNeighb, int[] neighbInd, CellArray cellArray) {
        if (cellArray == null && cellArray.getType().getNVertices() <= 1) {
            return;
        }
        int n = cellArray.getType().getNVertices();
        int[] nodes = cellArray.getNodes();
        for (int k = 0; k < nodes.length; k += n) {
            for (int i = k; i < k + n; i++) {
                int i0 = nodes[i];
                for (int j = k; j < k + n; j++) {
                    if (i != j) {
                        int i1 = nodes[j];
                        for (int l = neighbInd[i0]; l < neighbInd[i0 + 1]; l++) {
                            if (pNeighb[l] == i1) {
                                break;
                            }
                            if (pNeighb[l] == -1) {
                                pNeighb[l] = i1;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Split CellArrays of a given CellSet by removing connections on corners,
     * for separation of edges.
     *
     * Example for 2D case:
     * <ul>
     * <li> C
     * <li> / |
     * <li> / |
     * <li> / |
     * <li>A----B
     * </ul>
     * When B has hierarchy VERTEX then connection A<->C should be removed.
     * <ul>
     * <li> For 2D case - remove all segments that form this triangle, as well
     * as this triangle itself
     * <li> Fpr 3D case - remove all segments and triangles that form this
     * tetra, as well as this tetra itself
     * </ul>
     *
     * @return
     */
    private CellSet splitCellSetOnCorners(CellSet separatedCellSet, CellSet originalCellSet) throws MultipleCellSetsException, ComputationError {

        CellSet cs = new CellSet();
        CellArray[] cArrays = new CellArray[8];
        boolean is2D = separatedCellSet.getCellArray(CellType.TETRA) == null;

        // TODO ask - what is the difference between boundary and cell set ? 
        // if cell set set then boundary is the same for <QUAD?
        // copy points without modifications
        // points have been separated before ! - if not then this cell array should be formed from vertex nodes
////        int[] vertexNodes = getNodesByHierarchy(Hierarchy.VERTEX.getValue());
        CellArray pointCellArray = separatedCellSet.getCellArray(CellType.POINT);
        if (pointCellArray != null) {
            cArrays[CellType.POINT.getValue()] = pointCellArray;
        }

        // find triangles for 2D / tetras for 3D that contain vertex node
        CellType largestCT = is2D ? CellType.TRIANGLE : CellType.TETRA;
        // copy this as well
        cArrays[largestCT.getValue()] = separatedCellSet.getCellArray(largestCT);

        CellArray largestElementsArray = originalCellSet.getCellArray(largestCT);

        //VEEEERY LONG - maybe some sort??
        boolean[] containing = VNUtils.cellArrayContainsSubset(largestElementsArray, pointCellArray);
        CellArray largestToRemove = VNUtils.extractCellArrayFromMask(largestElementsArray, containing);
        // preserve rest - are they not already removed?
        boolean[] preserveLargest = containing.clone();
        // negation
        for (int i = 0; i < preserveLargest.length; i++) {
            preserveLargest[i] = !preserveLargest[i];
        }

        // here divide tetras -> triangles and segments (3D case); triangles -> segments; 
        // and remove them
        CellType[] cellTypesToProcess = is2D ? new CellType[]{CellType.SEGMENT} : new CellType[]{CellType.TRIANGLE, CellType.SEGMENT};

        for (CellType ct : cellTypesToProcess) {
            CellArray dividedCellArray = VNUtils.divideCellArrayToSubtype(largestToRemove, ct);
            CellArray subtractedCellArray = VNUtils.subtractCellArray(separatedCellSet.getCellArray(ct), dividedCellArray);
            cArrays[ct.getValue()] = subtractedCellArray;
        }
        cs.setCellArrays(cArrays);
        return cs;
    }

    /**
     * Split CellSet of input irre
     *
     * @param splitCorners
     * @return
     * @throws MultipleCellSetsException
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public CellSet splitCellSetByHierarchy(boolean splitCorners) throws MultipleCellSetsException, ComputationError {
        CellSet csSeparated = new CellSet();

        Map<Byte, CellType> CELL_TYPES_FOR_HIERARCHY = new HashMap<>();
        CELL_TYPES_FOR_HIERARCHY.put(Hierarchy.VERTEX.getValue(), CellType.POINT);
        CELL_TYPES_FOR_HIERARCHY.put(Hierarchy.EDGE.getValue(), CellType.SEGMENT);
        CELL_TYPES_FOR_HIERARCHY.put(Hierarchy.SURFACE.getValue(), CellType.TRIANGLE);
        CELL_TYPES_FOR_HIERARCHY.put(Hierarchy.INSIDE.getValue(), CellType.TETRA);

        byte[] availableHierarchies = Hierarchy.getHierarchyValuesForDecimation(getField().getTrueNSpace());

        // single cell set in prerequisite (TODO?)
        CellSet originalCS = getCellSet();

        CellArray[] cellArrays = new CellArray[8];
        CellType ct;
        int nVertices;
        int[] selectedHierarchyNodes;
        CellArray ca;
        byte hierarchyType; // hierarchy

        // for hierarchy VERTEX - only points 
        hierarchyType = (byte) 3;
        ct = CellType.POINT;
        nVertices = ct.getNVertices();
        selectedHierarchyNodes = getNodesByHierarchy(hierarchyType);

        cellArrays[ct.getValue()] = new CellArray(ct, selectedHierarchyNodes, null, null);
//        cellSetSeparated.addCells(ca);

        for (byte hType : availableHierarchies) {
            //extract from original cell array only cells that do not contain any other hierarchy nodes 
            //(i.e. for EDGE omit segment that has at one end VERTEX hierarchy)
            ct = CELL_TYPES_FOR_HIERARCHY.get(hType);
            CellArray originalCellArray;

            // for hierarchy EDGE - only segments from BOUNDARY CellArray
            // for hierarchy SURFACE - only triangles from BOUNDARY CellArray
            if ((hType == Hierarchy.EDGE.getValue()) || (hType == Hierarchy.SURFACE.getValue())) {
                CellArray boundaryCellArray = originalCS.getBoundaryCellArray(ct);
                originalCellArray = originalCS.getBoundaryCellArray(ct);
                // for hierarchy INSIDE - only TETRAS from whole CellArray
            } else if (hType == Hierarchy.INSIDE.getValue()) {
                originalCellArray = originalCS.getCellArray(ct);
            } else {
                throw new InternalError("Hierarchy not found.");
            }
            // disallow nodes from higher hierarchy
            // TODO ask - boundary nodes surfaces as well for 2D?
            // then inside nodes have to be included
            int nDisallowed = 0;
            for (byte h : Hierarchy.getAllValidHierarchyValues()) {
                if (h != hType) {
                    nDisallowed += getNNodesByHierarchy(h);
                }
            }

            int[] disallowedNodes = new int[nDisallowed];
            int count = 0;
            for (byte h : Hierarchy.getAllValidHierarchyValues()) {
                if (h != hType) {
                    for (int i : getNodesByHierarchy(h)) {
                        disallowedNodes[count++] = i;
                    }
                }
            }
            ca = VNUtils.removeCellArrayElementsContainingNode(disallowedNodes, originalCellArray);
            cellArrays[ct.getValue()] = ca;
        }
        csSeparated.setCellArrays(cellArrays);

        // some corners form connections apart from vertex node
        if (splitCorners) {
            CellSet cellSetSeparatedCornersSplit = splitCellSetOnCorners(csSeparated, getCellSet());
            return cellSetSeparatedCornersSplit;
        } else {
            return csSeparated;
        }
    }

    /**
     * Based on ConnectedComponents module.
     *
     * @param cs
     * @param irrField
     * @return
     */
    public int[] computeConnectedComponents(CellSet cs, IrregularField irrField) {

        // class fields copied - flattened for single cell set
        // ------------------------------------------
        int[] cellSetComponents = new int[(int) inField.getNNodes()]; //holder for component indices: cellSetComponents[i][j] = k iff node j belongs to kth component of cell set i
        int[] cellSetComponentSizes; // = new int[]; //holder for component sizes:   cellSetComponentSizes[i][2*j] is number of vertices in jth component of cell set i,
        //for sorting purposes:         cellSetComponentSizes[i][2*j+1] is component number
        int cellSetNComponents; //holder for component numbers: cellSetNComponents[i] is number of components of cell set i
        // ------------------------------------------

        // split cell set (split function)
        int[] neighb;
        int[] neighbInd;
        int n = 0, k0;

        neighbInd = new int[(int) irrField.getNNodes() + 1];
        for (int i = 0; i < neighbInd.length; i++) {
            neighbInd[i] = 0;
        }

        for (CellArray cellArray : cs.getCellArrays()) {
            if (cellArray != null && cellArray.getType().getNVertices() > 1) {
                int k = cellArray.getType().getNVertices() - 1;
                int[] nodes = cellArray.getNodes();
                for (int i = 0; i < nodes.length; i++) {
                    neighbInd[nodes[i]] += k;
                }
            }
        }

        int k = 0;
        for (int i = 0; i < neighbInd.length; i++) {
            int j = k + neighbInd[i];
            neighbInd[i] = k;
            k = j;
        }

        int[] pNeighb = new int[k];

        for (int i = 0; i < pNeighb.length; i++) {
            pNeighb[i] = -1;
        }

        for (CellArray cellArray : cs.getCellArrays()) {
            if (cellArray != null && cellArray.getType().getNVertices() > 1) {
                addNeighbors(pNeighb, neighbInd, cellArray);
            }
        }

        k = 0;
        for (int i = 0; i < pNeighb.length; i++) {
            if (pNeighb[i] != -1) {
                k += 1;
            }
        }
        neighb = new int[k];

        k = 0;
        for (int i = 0; i < neighbInd.length - 1; i++) {
            k0 = k;
            for (int j = neighbInd[i]; j < neighbInd[i + 1]; j++) {
                if (pNeighb[j] == -1) {
                    break;
                }
                neighb[k] = pNeighb[j];
                k += 1;
            }
            neighbInd[i] = k0;
        }

        neighbInd[neighbInd.length - 1] = k;
        pNeighb = null;
        int nNodes = (int) irrField.getNNodes();

        int[] components = cellSetComponents;
        int[] stack = new int[nNodes];
        int stackSize = -1;
        for (int i = 0; i < nNodes; i++) {
            components[i] = -1;
        }
        int comp = 0;
        for (int seed = 0; seed < components.length; comp++) {
            components[seed] = comp;
            stackSize = 0;
            stack[stackSize] = seed;
            while (stackSize >= 0) {
                int current = stack[stackSize];
                stackSize -= 1;
                components[current] = comp;
                for (int j = neighbInd[current]; j < neighbInd[current + 1]; j++) {
                    k = neighb[j];
                    if (components[k] == -1) {
                        stackSize += 1;
                        stack[stackSize] = k;
                        components[k] = comp;
                    }
                }
            }
            while (seed < nNodes && components[seed] != -1) {
                seed += 1; //looking for first node not yet assigned to any component
            }
        }
        stack = null;
        cellSetNComponents = comp;
        cellSetComponentSizes = new int[2 * comp];
        for (int i = 0; i < comp; i++) {
            cellSetComponentSizes[2 * i] = 0;
            cellSetComponentSizes[2 * i + 1] = i;
        }
        for (int i = 0; i < nNodes; i++) {
            cellSetComponentSizes[2 * components[i]] += 1;
        }
        HeapSort.sort(cellSetComponentSizes, 2, false);

        return cellSetComponents;
    }

}
