/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.RunButton.RunState;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;

/**
 *
 * @author mateuszmieczkowski
 */
public class DiffOpPluginShared {

    public enum Action {
        CALCULATE
    }

    public enum OutputType {
        POINT_FIELD,
        TRIANGULATED_IRREGULAR_FIELD_PARALLEL_REGIONS,
        TRIANGULATED_IRREGULAR_FIELD_QDELAUNAY,
        ORIGINAL_FIELD_DETAILS
    }
    //TODO there is private
    public static final ParameterName<Action[]> SCALAR_ACTIONS
            = new ParameterName<>("Select scalar components to calculate");
    public static final ParameterName<Action[]> VECTOR_ACTIONS
            = new ParameterName<>("Select vector components to calculate");

    public static final ParameterName<Map<Byte, Integer>> MAX_ELEMENTS = new ParameterName<>("Max number of elements for each hierarchy type.");
    public static final ParameterName<Map<Byte, Integer>> ELEMENTS_TO_LEAVE = new ParameterName<>("Set number of elements to retain after field decimation for each hierarchy type.");

    public static final ParameterName<Double> FILTERING_ANGLE = new ParameterName<>("Filtering angle (dihedrals) for hierarchy calculation");
    public static final ParameterName<Double> CRITICAL_ANGLE = new ParameterName<>("Critical angle for hierarchy calculation");
    public static final ParameterName<Double> LOGARITHMIC_ERROR = new ParameterName<>("Relative error - logarithmic difference between target and computed number of remaining elements for each hierarchy.");

    public static final ParameterName<String[]> META_SCALAR_COMPONENT_NAMES = new ParameterName("META Scalar Component names");
    public static final ParameterName<String[]> META_VECTOR_COMPONENT_NAMES = new ParameterName("META Vector Component names");
    public static final ParameterName<RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");

    // output type
    public static final ParameterName<OutputType> OUTPUT_TYPE = new ParameterName<>("Output type");

    // radius corrections
    public static final ParameterName<Map<Byte, Double>> LINEAR_RADIUS_CORRECTION = new ParameterName<>("Linear radius correction.");
    public static final ParameterName<Map<Byte, Double>> DIFF_NORM_WEIGHT_COEFFICIENT = new ParameterName<>("Differential norm weight coefficient");

    // iterations limit
    public static final ParameterName<Integer> ITERATIONS_LIMIT = new ParameterName<>("Iterations limit.");

    // DEFAULTS
    public static final double DEFAULT_FILTERING_ANGLE = 20.0;
    public static final double DEFAULT_CRITICAL_ANGLE = 25.0;

    // limit of iterations - if more then return field decimated after 7th iteration
    public static final int DEFAULT_ITERATIONS_LIMIT = 7;

    // relative difference between N elements intended to leave and computed 
    // max logarithmic error 
    // err = |log10(nLeft)-log10(nTarget)|
    // e.g. target elements are 10000; err = 0.5:
    // 3.5 < log10(nLeft) < 4.5 => 10^(3.5) < nLeft < 10^4.5 => 3162 < nLeft < 31622
    // the smaller the value is, the more iterations is required
    public static final double DEFAULT_LOGARITHMIC_ERROR = 0.2;
    public static final int DEFAULT_ELEMENTS_LEFT_ABSOLUTE_ERROR = 5;

    // default mu - weight coefficient in radius 
    public static final double DEFAULT_DIFF_NORM_WEIGHT_COEFFICIENT = 0.1f;

    // default number of elements to leave for each hierarchy (share) - gui init purposes
    // 0< share<=1
    public static final Map<Byte, Double> DEFAULT_ELEMENTS_TO_LEAVE_SHARE = new HashMap<Byte, Double>() {
        {
            put(Hierarchy.INSIDE.getValue(), 0.08);
            put(Hierarchy.EDGE.getValue(), 0.2);
            put(Hierarchy.SURFACE.getValue(), 0.14);
        }
    };

    public static void handleExceptionVNApp(Exception ex, Logger logger) {
        String messageTitle = ex.getMessage();
        logger.error(messageTitle);
        VisNow.get().userMessageSend(
                new UserMessage("VisNow", "DiffOpPlugin module", messageTitle, "", Level.ERROR));
    }

    public static Action[] findActions(Parameters parameters, ParameterName actionsName, ParameterName metaName, String[] newComponentNames, boolean resetParameters, boolean isFromVNA) {
        // empty action - length same as names of all component (list)
        Action[] newAction = new Action[newComponentNames.length];
        // actions from parameters
        Action[] actionsFromParameters = (Action[]) parameters.get(actionsName);

        // if isFromVNA set (so no directly in gui - then load parameters as row)
        if (isFromVNA) {
            return actionsFromParameters;
        }

        if (!resetParameters) { //try to restore calculations (based on component names)
            // TODO
            // TODO
            // TODO
            // TODO why is it making length 1???
            List<String> componentNames = Arrays.asList((String[]) parameters.get(metaName));
            //if only one component then use the same operations
            if (newComponentNames.length == 1 && componentNames.size() == newComponentNames.length) {
                newAction = actionsFromParameters;
            } //            } else if (scalarActions.length == newScalarAction.length) {
            //                newScalarAction = scalarActions;
            //            } 
            //otherwise restore by component name
            else {
                for (int i = 0; i < newComponentNames.length; i++) {
                    String componentName = newComponentNames[i];
                    int oldPosition = componentNames.indexOf(componentName);
                    if (oldPosition != -1) {
                        newAction[i] = actionsFromParameters[oldPosition];
                    }
                }
            }
        }

        return newAction;
    }
}
