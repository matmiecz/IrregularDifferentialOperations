/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin.Exceptions;

/**
 *
 * @author mateuszmieczkowski
 */
public class MultipleCellSetsException extends Exception {
    
    private static String DEFAULT_MESSAGE = "Multiple cell sets not supported.";

    /**
     * Creates a new instance of <code>MultipleCellSetsException</code> with custom
     * detail message.
     */
    public MultipleCellSetsException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Constructs an instance of <code>MultipleCellSetsException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public MultipleCellSetsException(String msg) {
        super(msg);
    }
}
