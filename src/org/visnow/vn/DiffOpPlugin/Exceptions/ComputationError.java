/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin.Exceptions;

/**
 *
 * @author mateuszmieczkowski
 */
public class ComputationError extends Exception {

    /**
     * Creates a new instance of <code>ComputationError</code> without detail
     * message.
     */
    public ComputationError() {
    }

    /**
     * Constructs an instance of <code>ComputationError</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ComputationError(String msg) {
        super(msg);
    }
}
