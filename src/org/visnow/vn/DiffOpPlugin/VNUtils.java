/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;
import org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException;
import org.visnow.vn.lib.utils.sort.IndirectComparator;
import static org.visnow.vn.lib.utils.sort.IndirectSort.indirectSort;

/**
 *
 * @author mateuszmieczkowski
 */
public class VNUtils {

    // ------------
    // VISNOW UTILS 
    // ==================
    // TODO more cell sets
    public static int[] getNodesForSimplexType(IrregularField field, CellType simplexType) throws MultipleCellSetsException {
        if (field.getNCellSets() == 1) {
            CellSet cs = field.getCellSet(0);
            CellArray ca = cs.getCellArray(simplexType);
            int[] nodes = ca.getNodes();
            return nodes;
        } else {
            throw new MultipleCellSetsException();
        }
    }

    public static int[] getNodeIndicesForSimplexType(IrregularField irregularField, CellType simplexType) throws MultipleCellSetsException, ComputationError {
        int[] cellSetNodes = VNUtils.getNodesForSimplexType(irregularField, simplexType);
        int nNodesInFieldTotal = (int) irregularField.getNElements();
        int[] cellSetNodesUnique = ArrayUtils.removeDuplicates1D(cellSetNodes);
        if (nNodesInFieldTotal == cellSetNodesUnique.length) {
            return cellSetNodesUnique;
        } else {
            throw new ComputationError("nNodesInFieldTotal (" + nNodesInFieldTotal + ") == cellSetNodesUnique.length (" + cellSetNodesUnique.length);
        }
    }

    /**
     * Given irregular field find type of simplex.
     *
     * @param inField
     * @return
     */
    public static CellType getSimplexType(IrregularField inField) {
        int trueNSpace = inField.getTrueNSpace();
        CellType simplexType;
        switch (trueNSpace) {
            case 2:
                simplexType = CellType.TRIANGLE;
                break;
            case 3:
                simplexType = CellType.TETRA;
                break;
            case 1:
                throw new IllegalArgumentException("1D cases not supported.");
            case -1:
                System.out.println("Cases with trueNSpace = -1 not supported.");
            //TODO Logger
            default:
                throw new IllegalArgumentException("Error. Computation failed.");
        }
        return simplexType;
    }

    public static IrregularField extractSingleCellSetField(IrregularField irregularField, CellType simplexType) throws MultipleCellSetsException, ComputationError {
        IrregularField extractedField;
        int[] cellSetNodesUnique = VNUtils.getNodeIndicesForSimplexType(irregularField, simplexType);
        int nNodesInFieldTotal = (int) irregularField.getNNodes();
        // cut field when not active
        if (nNodesInFieldTotal != cellSetNodesUnique.length) {
            // selection mask
            byte[] selectionMask = new byte[nNodesInFieldTotal];
            Arrays.fill(selectionMask, DecimateField.DISMISSED);
            for (int i : cellSetNodesUnique) {
                selectionMask[i] = DecimateField.SELECTED;
            }
            extractedField = DecimateField.extractFieldWithCellSets(irregularField, selectionMask);
        } else {
            extractedField = irregularField;
        }
        return extractedField;
    }

    public static boolean isField2D(IrregularField irrField) {
        FloatLargeArray fieldCoords = (FloatLargeArray) irrField.getCoords().getValues().get(0);
        return isField2D(fieldCoords);
    }

    public static boolean isField2D(float[] fieldCoords) {
        FloatLargeArray fla = new FloatLargeArray(fieldCoords);
        return isField2D(fla);

    }

    public static boolean isField2D(FloatLargeArray fieldCoords) {
        boolean is2D = true;
        for (int i = 2; i < fieldCoords.length(); i += 3) {
            if (fieldCoords.get(i) != 0) {
                is2D = false;
                break;
            }
        }
        return is2D;
    }

    public static boolean[] cellArrayContainsSubset(CellArray originalCellArray, CellArray disallowedCells) {

        boolean[] containing = new boolean[originalCellArray.getNCells()];
        Arrays.fill(containing, false);

        CellType cellType = originalCellArray.getType();
        CellType disallowedType = disallowedCells.getType();

        if ((!cellType.isSimplex()) || (!disallowedType.isSimplex())) {
            return null;
        }

        int cellTypeNVertices = cellType.getNVertices();
        int disallowedTypeNVertices = disallowedType.getNVertices();

        if (disallowedTypeNVertices > cellTypeNVertices) {
            return null;
        }

        // convert disallowed cells nodes to list of sets to ease comparisons
        List<Set<Integer>> disallowedCellsList = new ArrayList<>();
        for (int i = 0; i < disallowedCells.getNCells(); i++) {
            int[] disallowedCellVertices = disallowedCells.getCellVertices(i);
            Set<Integer> s = ArrayUtils.intArrayToSet(disallowedCellVertices);
            disallowedCellsList.add(s);
        }

        // loop over originalCellArray
        // divide it on subelements of disallowedCells' type
        // if found any common then break loop 
        // and set containing to true
        outer_loop:
        for (int i = 0; i < originalCellArray.getNCells(); i++) {
            int[] vertices = originalCellArray.getCellVertices(i);

            List<int[]> tmpElementsToRemove = ArrayUtils.permutationsDecorator(vertices, disallowedTypeNVertices);

            // find if any of tmpElementsToRemove is in disallowedCells
            for (int[] subelement : tmpElementsToRemove) {
                Set<Integer> subelementSet = ArrayUtils.intArrayToSet(subelement);
                if (disallowedCellsList.contains(subelementSet)) {
                    containing[i] = true;
                    continue outer_loop;
                }
            }
        }

        return containing;
    }

    // ------------------------------------------------------
    // ------------------------------------------------------
    // CellArray Utils
    // ------------------------------------------------------
    /**
     * Extract subset of CellArray with all elements that does not contain any
     * NODE from given disallowed nodes array.
     *
     * @param disallowedNodes
     * @param originalCellArray
     */
    public static CellArray removeCellArrayElementsContainingNode(int[] disallowedNodes, CellArray originalCellArray) {
        CellArray ca;
        List<Integer> separatedVertices = new ArrayList<>();
        CellType ct = originalCellArray.getType();
        int nVertices = ct.getNVertices();
        int[] originalVertices = originalCellArray.getNodes();
        int N = originalVertices.length / nVertices;
        outer_loop:
        for (int i = 0; i < N; i++) {
            int[] structure = Arrays.copyOfRange(originalVertices, nVertices * i, nVertices * (i + 1));
            for (int j : structure) {
                for (int disallowedNode : disallowedNodes) {
                    if (j == disallowedNode) {
                        continue outer_loop;
                    }
                }
            }
            for (int k : structure) {
                separatedVertices.add(k);
            }
        }
        ca = new CellArray(ct, separatedVertices.stream().mapToInt((Integer a) -> a).toArray(), null, null);
        return ca;
    }

    /**
     * Extract Cells from CellArray that DO NOT contain any Cells from
     * disallowedCells. Works only for simplices. (CellType.isSimplex() == true)
     *
     * i.e. if disallowedCells is formed of segments, and originalCellArray from
     * triangles then remove all triangles that contain at least one segment
     * from the disallowed set.
     *
     * Example:
     *
     * disallowedSegments={0,1}
     *
     * cellArrayNodes={0,1,0,2,0,3} // segment cell array
     *
     * returns {0,2,0,3}
     *
     * @param originalCellArray - any CellArray containing elements larger or
     * equal than disallowedCells
     * @param disallowedCells - CellArray of elements to remove (AND
     * subelements)
     * @return
     */
    public static CellArray subtractCellArray(CellArray originalCellArray, CellArray disallowedCells) {

        boolean[] containing = VNUtils.cellArrayContainsSubset(originalCellArray, disallowedCells);
        int nToRemove = ArrayUtils.countTrueValues(containing);

        CellType cellType = originalCellArray.getType();
        int nVertices = cellType.getNVertices();
        int[] extractedNodes = new int[originalCellArray.getNodes().length - nToRemove * nVertices];

        int counter = 0;
        for (int i = 0; i < containing.length; i++) {
            if (!containing[i]) {
                int[] vertices = originalCellArray.getCellVertices(i);
                System.arraycopy(vertices, 0, extractedNodes, counter * nVertices, nVertices);
                counter++;
            }

        }
        CellArray extractedCellArray = new CellArray(cellType, extractedNodes, null, null);
        return extractedCellArray;
    }

    /**
     * given mask of boolean values extract CellArray when value is true;
     *
     * @param cellArray
     * @param mask
     * @return
     */
    public static CellArray extractCellArrayFromMask(CellArray cellArray, boolean[] mask) {
        CellType ct = cellArray.getType();

        if (mask.length != cellArray.getNCells()) {
            return null;
        }

        List<Integer> selectedCellsIndices = new ArrayList<>();
        for (int i = 0; i < mask.length; i++) {
            if (mask[i]) {
                selectedCellsIndices.add(i);
            }
        }
        int nSelected = ArrayUtils.countTrueValues(mask);
        int[] selectedCellsNodes = new int[nSelected * ct.getNVertices()];
        for (int i = 0; i < selectedCellsIndices.size(); i++) {
            Cell selectedCell = cellArray.getCell(selectedCellsIndices.get(i));
            System.arraycopy(selectedCell.getVertices(), 0, selectedCellsNodes, i * ct.getNVertices(), ct.getNVertices());
        }

        CellArray selectedCells = new CellArray(ct, selectedCellsNodes, null, null);
        return selectedCells;
    }

    public static class IterationMeta {

        private final double linearCorrection, diffNormWeightCoefficient;
        private final int nLeft;
        private final int targetElements, maxElements;

        public IterationMeta(double linearCorrection, double diffNormWeightCoefficient, int nLeft, int targetElements, int maxElements) {
            this.linearCorrection = linearCorrection;
            this.diffNormWeightCoefficient = diffNormWeightCoefficient;
            this.nLeft = nLeft;

            this.targetElements = targetElements;
            this.maxElements = maxElements;
        }

        public double getLinearCorrection() {
            return linearCorrection;
        }

        public double getDiffNormWeightCoefficient() {
            return diffNormWeightCoefficient;
        }

        public int getNLeft() {
            return nLeft;
        }

        public int getAbsoluteDecimationError() {
            return (this.nLeft - this.targetElements);
        }

        public double getLogarithmicDecimationError() {
            double logNLeftError = Math.log10(getNLeft());
            double logTarget = Math.log10(getTargetElements());
            return Math.abs(logNLeftError - logTarget);
        }

        public boolean anotherIterationRequired(int allowedAbsoluteError, double allowedLogarithmicError) {
            // Stop computations if only one of 2 conditions is satisfied:
            // 1) relative error is less than allowed
            // or
            // 2) absolute error is less than allowed 
            return (Math.abs(getAbsoluteDecimationError()) > allowedAbsoluteError) && (Math.abs(getLogarithmicDecimationError()) > allowedLogarithmicError);
        }

        public int getTargetElements() {
            return targetElements;
        }

        public int getMaxElements() {
            return maxElements;
        }

    }

    public static class IterationsHistory {

        List<Map<Byte, IterationMeta>> iterations;

        public IterationsHistory() {
            this.iterations = new ArrayList<>();
        }

        public void addIterationForHierarchy(int n, byte hierarchy, IterationMeta iterationForHierarchy) {
            Map<Byte, IterationMeta> iteration;
            if (this.iterations.size() > n) {
                iteration = this.iterations.get(n);
                this.iterations.remove(n);
            } else {
                iteration = new HashMap<>();
            }
            iteration.put(hierarchy, iterationForHierarchy);
            this.iterations.add(iteration);
        }

        public List<Map<Byte, IterationMeta>> getIterationsHistory() {
            return this.iterations;
        }

        public int getNIterations() {
            return this.iterations.size();
        }

        public IterationMeta getIteration(int n, byte hierarchy) {
            return getIterationsHistory().get(n).get(hierarchy);
        }

        public List<Double> getLinearCorrections(byte hierarchy) {
            List<Double> linearCorrections = new ArrayList<>();
            iterations.forEach((iteration) -> {
                linearCorrections.add(iteration.get(hierarchy).getLinearCorrection());
            });
            return linearCorrections;
        }

        public List<Double> getDiffNormWeightCoefficients(byte hierarchy) {
            List<Double> diffNormWeightCoefficient = new ArrayList<>();
            iterations.forEach((iteration) -> {
                diffNormWeightCoefficient.add(iteration.get(hierarchy).getDiffNormWeightCoefficient());
            });
            return diffNormWeightCoefficient;
        }

        public List<Integer> getResultElements(byte hierarchy) {
            List<Integer> resultElements = new ArrayList<>();
            iterations.forEach((iteration) -> {
                resultElements.add(iteration.get(hierarchy).getNLeft());
            });
            return resultElements;
        }

        public List<Integer> getAbsoluteDecimationError(byte hierarchy) {
            List<Integer> absErr = new ArrayList<>();
            iterations.forEach((iteration) -> {
                absErr.add(iteration.get(hierarchy).getAbsoluteDecimationError());
            });
            return absErr;
        }

        public List<Double> getRelativeDecimationError(byte hierarchy) {
            List<Double> relErr = new ArrayList<>();
            iterations.forEach((iteration) -> {
                relErr.add(iteration.get(hierarchy).getLogarithmicDecimationError());
            });
            return relErr;
        }

        /**
         * return index of iteration, after which number of elements remained is
         * closest to target
         *
         * @param hierarchy
         * @return
         */
        public int getClosestToTargetIterationNumber(byte hierarchy) {
            List<Integer> absErrors = getAbsoluteDecimationError(hierarchy);
            double[] absDifferencesModulo = new double[absErrors.size()];
            for (int i = 0; i < absDifferencesModulo.length; i++) {
                absDifferencesModulo[i] = Math.abs(absErrors.get(i));
            }
            // find closest result
            int closestResultIteration = ArrayUtils.findNSmallestIndices(absDifferencesModulo, 1)[0];
            return closestResultIteration;
        }

        /**
         * Get iteration number with closest n elements remained but LESS than
         * target.
         *
         * Goal is to use it to add nodes with highest priority but dismissed.
         *
         * @param hierarchy
         * @return number of iteration
         */
        public int getClosestToTargetIterationNumberLessElements(byte hierarchy) {
            List<Integer> absErrors = getAbsoluteDecimationError(hierarchy);
            double[] absDifferences = new double[absErrors.size()];

            // change all positive differences to Double.MIN and then select indice of greatest value
            for (int i = 0; i < absDifferences.length; i++) {
                int absDiff = absErrors.get(i);
                absDifferences[i] = (absDiff > 0) ? Double.MIN_VALUE : absDiff;
            }
            int N = absDifferences.length;
            int closestLessElementsN = ArrayUtils.findNSmallestIndices(absDifferences, N)[N - 1];
            return closestLessElementsN;
        }

        public IterationMeta getClosestToTargetIterationMeta(byte hierarchy) {
            int closestResultIteration = getClosestToTargetIterationNumber(hierarchy);
            return this.iterations.get(closestResultIteration).get(hierarchy);
        }

        public IterationMeta getClosestToTargetLessElementsIterationMeta(byte hierarchy) {
            int closestResultIterationLessElements = getClosestToTargetIterationNumberLessElements(hierarchy);
            return this.iterations.get(closestResultIterationLessElements).get(hierarchy);
        }

        public static <T extends Comparable<T>> int[] getOrderedAscending(List<T> listToOrder) {
            IndirectComparator ic = new IndirectComparator() {
                @Override
                public int compare(int i, int j) {
                    return listToOrder.get(j).compareTo(listToOrder.get(i));
                }
            };

            int[] ascendingOrder = new int[listToOrder.size()];
            for (int i = 0; i < ascendingOrder.length; i++) {
                ascendingOrder[i] = i;
            }
            indirectSort(ascendingOrder, ic, false);
            return ascendingOrder;
        }

        public static int[] getOrderedAscending(double[] arrayToOrder) {

            IndirectComparator ic = new IndirectComparator() {
                @Override
                public int compare(int i, int j) {
                    if (arrayToOrder[i] > arrayToOrder[j]) {
                        return -1;
                    } else if (arrayToOrder[i] < arrayToOrder[j]) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            };
            int[] ascendingOrder = new int[arrayToOrder.length];
            for (int i = 0; i < ascendingOrder.length; i++) {
                ascendingOrder[i] = i;
            }
            indirectSort(ascendingOrder, ic, false);
            return ascendingOrder;
        }

        /**
         * Order iterations from the best to the worst in terms of absolute
         * difference to target.
         *
         * @return
         */
        public int[] getOrderedByAbsoluteDifferenceModulo(byte hierarchy) {
            List<Integer> absErrModulo = getAbsoluteDecimationError(hierarchy).stream().map(a -> Math.abs(a)).collect(Collectors.toList());
            return getOrderedAscending(absErrModulo);
        }

        public int[] getOrderedByLinearCorrection(byte hierarchy) {
            return getOrderedAscending(getLinearCorrections(hierarchy));
        }

        public int[] getOrderedByResultElements(byte hierarchy) {
            return getOrderedAscending(getResultElements(hierarchy));
        }

        public void addIterationInfo(Map<Byte, IterationMeta> iterationInfo) {
            this.iterations.add(iterationInfo);
        }

        public String getIterationReport(int n, byte hierarchy) {
            String report;

            IterationMeta iterationMeta = getIteration(n, hierarchy);
            String typeName = Hierarchy.getType(hierarchy).getName();
            int nLeftAfterDecimation = iterationMeta.getNLeft();
            int targetElements = iterationMeta.getTargetElements();
            int maxN = iterationMeta.getMaxElements();

            int absoluteDecimationError = iterationMeta.getAbsoluteDecimationError();
            double logarithmicDecimationError = iterationMeta.getLogarithmicDecimationError();

            double linearRadiusCorrection = iterationMeta.getLinearCorrection();
            double diffNormWeightCoefficient = iterationMeta.getDiffNormWeightCoefficient();

            report = "Iter=" + n + "; Hierarchy=" + typeName + ";nLeft(target:max)=" + nLeftAfterDecimation + "(" + targetElements + "/" + maxN + ")" + ";AbsErr=" + String.format("%d", absoluteDecimationError) + ";LogErr=" + String.format("%.3f", logarithmicDecimationError);;
            report += ";Linear_radius_correction=" + String.format("%.3f", linearRadiusCorrection) + "; diffNormWeightCoefficient=" + String.format("%.3f", diffNormWeightCoefficient) + "\n";
            return report;
        }

    }

    /**
     * Invoke shell script and return spawned process controller object.
     *
     * @param commandOrScriptPath - can be either command or shell script path
     * (lookup ProcessBuilder doc)
     *
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public static Process callShell(String commandOrScriptPath) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder(commandOrScriptPath);
        Process pr = builder.start();

//        Runtime run = Runtime.getRuntime();
//        Process pr = run.exec(command);
//        Process pr = run.exec("/Users/mateuszmieczkowski/Documents/ICM_studia/PRACA_MAGISTERSKA_ICM/external_libraries/qhull/qhull/bin/qdelaunay .");        
//        Process pr = run.exec("/Users/mateuszmieczkowski/Documents/ICM_studia/PRACA_MAGISTERSKA_ICM/external_libraries/qhull/qhull/bin/qdelaunay i Qt TI '/Users/mateuszmieczkowski/Documents/ICM_studia/PRACA_MAGISTERSKA_ICM/tmp/coords_dumped.txt'");
        pr.waitFor();
        int exitValue = pr.exitValue();
        if (exitValue == 0) {
            return pr;
        } else {
            List<String> error = new ArrayList<>();
            BufferedReader br = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
            String err;
            while ((err = br.readLine()) != null) {
                error.add(err);
                System.out.println(err);
            }

            throw new IOException("Shell script exited with exit value " + Integer.toString(exitValue));
        }
    }

    public static List<String> invokeShellCommand(String command) throws IOException, InterruptedException {
        Process pr = callShell(command);
        return processOutputToList(pr);
    }

    public static List<String> invokeShellScript(String scriptPath) throws IOException, InterruptedException {
        Process pr = callShell(scriptPath);
        return processOutputToList(pr);
    }

    private static List<String> processOutputToList(Process pr) throws IOException {
        InputStream stdin = pr.getInputStream();
        return streamToList(stdin);
    }

    public static List<String> streamToList(InputStream inputStream) throws IOException {
        List<String> output = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        String s;
        while ((s = br.readLine()) != null) {
            output.add(s);
        }
        return output;
    }

    public static void linesToFile(List<String> lines, String filePath) throws IOException {
        File file = new File(filePath);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            for (String line : lines) {
                writer.write(line);
                writer.write('\n');
            }
        }
        file.setExecutable(true, false);
    }

    /**
     * From CellArray formed of larger structures create cell array of smaller
     * structures.
     *
     * i.e. from triangles build segments
     *
     * @param cellArray
     * @param targetCellType
     * @return
     */
    public static CellArray divideCellArrayToSubtype(CellArray cellArray, CellType targetCellType) {

        CellType superCellType = cellArray.getType();

        if (superCellType == targetCellType) {
            return cellArray;
        } else if (targetCellType.getValue() > superCellType.getValue()) {
            return null;
        } else {
            int n = ArrayUtils.getNPermutations(superCellType.getNVertices(), targetCellType.getNVertices());
            int[] dividedCellArrayNodes = new int[cellArray.getNCells() * n * targetCellType.getNVertices()];

            for (int i = 0; i < cellArray.getNCells(); i++) {
                Cell cell = cellArray.getCell(i);
                List<int[]> subcells = ArrayUtils.permutationsDecorator(cell.getVertices(), targetCellType.getNVertices());

                for (int j = 0; j < subcells.size(); j++) {
                    int[] subcellVertices = subcells.get(j);
                    System.arraycopy(subcellVertices, 0, dividedCellArrayNodes, (i * n + j) * targetCellType.getNVertices(), targetCellType.getNVertices());
                }
            }

            // removing duplicates (not directly) - maybe not efficient - TODO
            CellArray dividedCellArray = new CellArray(targetCellType, dividedCellArrayNodes, null, null);
            dividedCellArray.cancelDuplicates();
            return dividedCellArray;

        }

    }

}
