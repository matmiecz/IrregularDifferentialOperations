/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin;

import java.util.Arrays;
import org.apache.log4j.Logger;

/**
 *
 * @author mateuszmieczkowski
 */
public class DebugTools {

    public static double[] sliceByIndices(int[] indices, double[] array) {
        // return array sliced only with indices values 
        double[] slicedArray = new double[indices.length];
        for (int i = 0; i < slicedArray.length; i++) {
            slicedArray[i] = array[indices[i]];
        }
        return slicedArray;
    }

    public static int[] sliceByIndices(int[] indices, int[] array) {
        // return array sliced only with indices values 
        int[] slicedArray = new int[indices.length];
        for (int i = 0; i < slicedArray.length; i++) {
            slicedArray[i] = array[indices[i]];
        }
        return slicedArray;
    }

    public static void logNodesWithinRadius(int nodeIndex, double radius, DecimateField fieldDecimator, Logger logger) {
        if (nodeIndex > (int) fieldDecimator.getField().getNNodes()) {
            logger.debug("Node_number=" + nodeIndex + " does not exist.");
            return;
        }
        int[] neighboursWithinRadius = fieldDecimator.findNodesWithinRadius(nodeIndex, radius);
        logger.debug("Node_number=" + nodeIndex + ";within_radius=" + radius + ";nodes_amount=" + neighboursWithinRadius.length + ";nodes=" + Arrays.toString(neighboursWithinRadius));
    }

}
