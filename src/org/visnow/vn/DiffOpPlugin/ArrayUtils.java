/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularMatrixException;
import static org.apache.commons.math3.util.FastMath.abs;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.utils.sort.IndirectComparator;
import static org.visnow.vn.lib.utils.sort.IndirectSort.indirectSort;

/**
 *
 * @author mateuszmieczkowski
 */
public class ArrayUtils {

    /**
     * Create 1D array by adding sequential elements of 2D array one after
     * another. Attention! To extract elements additional calculation of
     * infoArray necessary (look: from2Dto1DInfo)
     *
     * @param array2D
     * @return
     */
    public static int[] array2DTo1D(int[][] array2D) {
        int sum = 0;
        for (int[] i : array2D) {
            sum += i.length;
        }
        int[] array1D = new int[sum];
        int counter = 0;
        for (int[] arr : array2D) {
            System.arraycopy(arr, 0, array1D, counter, arr.length);
            counter += arr.length;
        }
        return array1D;
    }

    /**
     * Provide information about beforehand converted 2D array.
     *
     * @param array2Dim
     * @return
     */
    public static int[] from2Dto1DInfo(int[][] array2Dim) {
        int[] infoArray = new int[array2Dim.length + 1];
        int counter = 0;
        infoArray[0] = counter;
        for (int i = 1; i < infoArray.length; i++) {
            counter += array2Dim[i - 1].length;
            infoArray[i] = counter;
        }
        return infoArray;
    }

    /**
     * Convert 1D array, into 2D array forming the rows accordingly to the
     * provided arrayInfo.
     *
     * @param array
     * @param arrayInfo
     * @return
     */
    public static int[][] array1Dto2D(int[] array, int[] arrayInfo) {
        int[][] array2D = new int[arrayInfo.length - 1][];
        for (int i = 0; i < array2D.length; i++) {
            array2D[i] = extractRow(array, arrayInfo, i);
        }
        return array2D;
    }

    public static int[] removeNegatives1D(int[] array) {
        int[] a = array.clone();
        //has to be SORTED!
        Arrays.sort(a);
        int counter = 0;
        for (int i : a) {
            if (i < 0) {
                counter++;
            } else {
                break;
            }
        }
        return Arrays.copyOfRange(a, counter, a.length);
    }

    public static int[][] removeNegatives2D(int[][] array) {
        int[][] a = new int[array.length][];
        for (int i = 0; i < a.length; i++) {
            a[i] = removeNegatives1D(array[i]);
        }
        return a;
    }

    /**
     *
     * @param array1D
     * @param infoArray
     * @param rowNumber
     * @return
     */
    public static int[] extractRow(int[] array1D, int[] infoArray, int rowNumber) {
        int[] row = new int[infoArray[rowNumber + 1] - infoArray[rowNumber]];
        System.arraycopy(array1D, infoArray[rowNumber], row, 0, row.length);
        return row;
    }

    public static int[][] sortArray2D(int[][] arrayToSort) {
        int[][] aToSort = arrayToSort;
        for (int[] aToSort1 : aToSort) {
            Arrays.sort(aToSort1);
        }
        return aToSort;
    }

    /**
     * removes duplicates from the given array, while returning ONLY positive
     * values
     *
     * @param array
     * @return
     */
    public static int[] removeDuplicates1D(int[] array) {
        int[] a = array.clone();
        //has to be SORTED!
        Arrays.sort(a);
        if (a.length < 2) {
            return a;
        }
        int counter = 0;
        for (int i = 1; i < a.length; i++) {
            if (a[i] != a[counter]) {
                counter += 1;
                a[counter] = a[i];
            }
        }
        return Arrays.copyOfRange(a, 0, counter + 1);
    }

    public static int[][] removeDuplicates2D(int[][] array) {
        int[][] a = array;
        int[][] b = new int[a.length][];
        for (int i = 0; i < a.length; i++) {
            b[i] = removeDuplicates1D(a[i]);
        }
        return b;
    }

    /**
     * Calculate dot product of two 3D vectors.
     *
     * @param vec1
     * @param vec2
     * @return
     */
    public static double dotProduct(double[] vec1, double[] vec2) {
        if (vec1.length != vec2.length) {
            throw new IllegalArgumentException("vec1.len != vec2.len");
        }
        double a = 0.0;
        for (int i = 0; i < vec1.length; i++) {
            a += vec1[i] * vec2[i];
        }
        return a;
    }

    /**
     * Calculate length of the 3D vector.
     *
     * @param vec
     * @return
     */
    public static double vecLength(double[] vec) {
        return Math.sqrt(dotProduct(vec, vec));
    }

    public static void print2DArray(double[][] arrayToPrint) {
        for (double[] arrayToPrint1 : arrayToPrint) {
            System.out.println(Arrays.toString(arrayToPrint1));
        }
    }

    public static void print2DArray(int[][] arrayToPrint) {
        for (int[] arrayToPrint1 : arrayToPrint) {
            System.out.println(Arrays.toString(arrayToPrint1));
        }
    }

    public static void printCells(CellArray triArray) {
        for (int i = 0; i < triArray.getNCells(); i++) {
            System.out.println(triArray.getCell(i).toString());
        }
    }

    /**
     * Calculate angle between two 3D vectors.
     *
     * @param vec1
     * @param vec2
     * @return
     */
    public static double calculateAngle(double[] vec1, double[] vec2) {
        double cos = dotProduct(vec1, vec2) / (vecLength(vec1) * vecLength(vec2));
        return Math.toDegrees(Math.acos(cos));
    }

    /**
     * Create vector of two points in 3D space.
     *
     * @param coordsP1
     * @param coordsP2
     * @return
     */
    public static double[] createVector(double[] coordsP1, double[] coordsP2) {
        double[] vector = new double[coordsP1.length];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = coordsP2[i] - coordsP1[i];
        }
        return vector;
    }

    public static double calculateGeometricalCentreForGroup(double[] coordsOfGroup) {
        int nN = coordsOfGroup.length / 3;
        double[] geometricalCentreCoords = new double[3];
        for (int i = 0; i < geometricalCentreCoords.length; i++) {
            double sum = 0.0;
            for (int j = 0; j < nN; j++) {
                sum += coordsOfGroup[3 * j + i];
            }
            geometricalCentreCoords[i] = sum / nN;
        }

        //find biggest radius
        double radiusForGroup = 0.0;
        for (int i = 0; i < nN; i++) {
            double r = 0.0;
            for (int j = 0; j < 3; j++) {
                r += (coordsOfGroup[3 * i + j] - geometricalCentreCoords[j]) * (coordsOfGroup[3 * i + j] - geometricalCentreCoords[j]);
            }
            r = Math.sqrt(r);
            if (r > radiusForGroup) {
                radiusForGroup = r;
            }
        }
        return radiusForGroup;
    }

    /**
     * Find largest element among all 1D integer array elements.
     *
     * @param array
     * @return largest element in array
     */
    public static int findLargest(int[] array) {
        int largest = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > largest) {
                largest = array[i];
            }
        }
        return largest;
    }

    //======================================
    //==========MATH UTILS==================
    //======================================
    /**
     * SOLVING LINEAR EQUATIONS with LUDecomposition
     *
     * @param A - left hand side 2D array of coefficents
     * @param B - right hand side - 1D array of constants
     * @return
     */
    public static double[] lsolveLUD(double[][] A, double[] B) {
        double[] result = null;
        try {
            //Start by decomposing the coefficient matrix A (in this case using LU decomposition) and build a solver
            RealMatrix coefficients = new Array2DRowRealMatrix(A, false);
            DecompositionSolver solver = new LUDecomposition(coefficients).getSolver();

            //Next create a RealVector array to represent the constant vector B and use solve(RealVector) to solve the system
            RealVector constants = new ArrayRealVector(B, false);
            RealVector solution = solver.solve(constants);
            result = solution.toArray();
        } catch (SingularMatrixException singularMatrixException) {
            throw new IllegalArgumentException("Exception caught! -> singular matrix");
        }
        return result;
    }

    /**
     * Solves the linear system Ax=b using Gaussian elimination with partial
     * pivoting. Matrix A needs to be square and nonsingular.
     *
     *
     * @param A nonsingular, square matrix
     * @param b right-hand-side.
     *
     * @return x, the solution of Ax=b
     */
    public static double[] lsolve(double[][] A, double[] b) {
        int N = b.length;

        for (int p = 0; p < N; p++) {
            // find pivot row and swap
            int max = p;
            for (int i = p + 1; i < N; i++) {
                if (abs(A[i][p]) > abs(A[max][p])) {
                    max = i;
                }
            }
            double[] temp = A[p];
            A[p] = A[max];
            A[max] = temp;
            double t = b[p];
            b[p] = b[max];
            b[max] = t;

            // singular
            if (A[p][p] == 0.0) {
                throw new IllegalArgumentException("The input matrix is singular");
            }

            // pivot within A and b
            for (int i = p + 1; i < N; i++) {
                double alpha = A[i][p] / A[p][p];
                b[i] -= alpha * b[p];
                for (int j = p; j < N; j++) {
                    A[i][j] -= alpha * A[p][j];
                }
            }
        }
        // back substitution
        double[] x = new double[N];
        for (int i = N - 1; i >= 0; i--) {
            double sum = 0.0f;
            for (int j = i + 1; j < N; j++) {
                sum += A[i][j] * x[j];
            }
            x[i] = (b[i] - sum) / A[i][i];
        }
        return x;
    }

    public static double findMax(double[] array) {
        double max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }

    public static double findMin(double[] array) {
        double min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }

    public static double findMax(double[][] array) {
        double max = array[0][0];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }
            }
        }
        return max;
    }

    public static double findMin(double[][] array) {
        double min = array[0][0];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] < min) {
                    min = array[i][j];
                }
            }
        }
        return min;
    }

    public static double calculateComponentRMS(DataArray componentDataArray) {
        return Math.sqrt(componentDataArray.getMeanSquaredValue());
    }

    //normalize to minValue-1
    public static double[] normalizeArray(double[] arrayToNormalize, double minValue) {
        if ((minValue < 0) || (minValue >= 1)) {
            throw new IllegalArgumentException("minValue argument should be in range 0<=minValue<1.");
        }
        double[] normalizedArray = arrayToNormalize.clone();
        double max = ArrayUtils.findMax(arrayToNormalize);
        double min = ArrayUtils.findMin(arrayToNormalize);
        for (int i = 0; i < arrayToNormalize.length; i++) {
            normalizedArray[i] = ((1 - minValue) * (arrayToNormalize[i] - min) / (max - min)) + minValue;
        }
        return normalizedArray;
    }

    private static DataArray normalizeArrayValue(DataArray dataArrayToNormalize, double normalizationValue, String nameSuffix) {
        int vecLen = dataArrayToNormalize.getVectorLength();
        double[] a = dataArrayToNormalize.getRawArray().getDoubleData();
        DataArray normalizedDataArray;
        if (vecLen > 1) {
            //for vectors
            throw new UnsupportedOperationException("Not implemented yet");
        } else {
            //for scalars
            for (int i = 0; i < a.length; i++) {
                a[i] = a[i] / normalizationValue;
            }
            normalizedDataArray = DataArray.create(a, 1, dataArrayToNormalize.getName() + nameSuffix);
        }
        return normalizedDataArray;
    }

    public static DataArray normalizeArrayMax1(DataArray dataArrayToNormalize) {
        double normalizationValue = dataArrayToNormalize.getMaxValue();
        String nameSuffix = "_normalizedMax1";
        return normalizeArrayValue(dataArrayToNormalize, normalizationValue, nameSuffix);
    }

    public static DataArray normalizeArrayRMS(DataArray dataArrayToNormalize) {
        double RMS = calculateComponentRMS(dataArrayToNormalize);
        String nameSuffix = "_normalizedRMS";
        return normalizeArrayValue(dataArrayToNormalize, RMS, nameSuffix);
    }

    /**
     * Count number of occurences of element in 1D array.
     *
     * @param array
     * @param element
     * @return
     */
    public static int countByteOccurences(byte[] array, byte element) {
        int n = 0;
        for (byte b : array) {
            if (b == element) {
                n++;
            }
        }
        return n;
    }

    public static int countTrueValues(boolean[] arr) {
        int n = 0;
        for (boolean b : arr) {
            if (b == true) {
                n++;
            }
        }
        return n;
    }

    /**
     * Return N elements int array with indices of N smallest elements in array.
     *
     * @param array
     * @param n - number of elements
     * @return
     */
    public static int[] findNSmallestIndices(double[] array, int n) {
        int[] indices = new int[n];

        double[] array_copy = Arrays.copyOf(array, array.length);
        Arrays.sort(array_copy);

        int indicePosition = 0;
        double[] nSmallestArr = Arrays.copyOfRange(array_copy, 0, n);//array_copy.length - n, array_copy.length);
        for (int i = 0; i < array.length; i++) {
            int index = Arrays.binarySearch(nSmallestArr, array[i]);
            if (index < 0) {
                continue;
            }
            indices[indicePosition++] = i;
            if (indicePosition > indices.length - 1) {
                break;
            }
        }
        return indices;
    }

    public static int[] sortIndices(double[] arr) {

        IndirectComparator cmp = new IndirectComparator() {
            @Override
            public int compare(int i, int j) {
                if (arr[i] > arr[j]) {
                    return 1;
                } else if (arr[i] < arr[j]) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };

        int[] indices = new int[arr.length];
        for (int i = 0; i < indices.length; i++) {
            indices[i] = i;
        }

        indirectSort(indices, cmp, true);
        return indices;

    }

    public static int[] reverseIntArray(int[] array) {
        int[] arr_copy = array.clone();
        for (int i = 0; i < array.length / 2; i++) {
            int tmp = arr_copy[i];
            arr_copy[i] = arr_copy[arr_copy.length - i - 1];
            arr_copy[arr_copy.length - i - 1] = tmp;
        }
        return arr_copy;
    }

    public static double linearExtrapolation(double[] x, double[] y, double value) {
        double result = y[0] + (value - x[0]) / (x[1] - x[0]) * (y[1] - y[0]);
        return result;
    }

    /*
    Write:
    nDim
    nPoints
    x1 y1 z1
    ...
    xn yn zn
    
     */
    public static void writeCoords(FloatLargeArray coords, String fileName) throws IOException {
        int nDim = VNUtils.isField2D(coords) ? 2 : 3;

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            long nPoints = coords.length() / 3;
            bw.write(String.valueOf(nDim));
            bw.newLine();
            bw.write(String.valueOf(nPoints));
            bw.newLine();
            for (int i = 0; i < nPoints; i++) {
                String line = "";
                for (int j = 0; j < nDim; j++) {
                    line += coords.get(3 * i + j);
                    line += "\t";
                }
                bw.write(line);
                bw.newLine();
            }
        }
    }

    public static int[] readTriangulationResults(String fileName, boolean is3D) throws FileNotFoundException, IOException {
        InputStream inputStream = new FileInputStream(fileName);
        int[] triangulation = readTriangulationResultsFromStream(inputStream, is3D);
        return triangulation;
    }

    public static int[] readTriangulationResultsFromStream(InputStream inputStream, boolean is3D) throws IOException {
        int nNodesInSimplex = is3D ? 4 : 3; // 4 nodes for tetras, 3 for triangles

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        // read first line to get information about number of elements 
        String headerLine = br.readLine();
        int n = Integer.parseInt(headerLine.split("//s")[0]);

        int[] triangulation = new int[n * nNodesInSimplex];
        for (int i = 0; i < n; i++) {
            String[] line = br.readLine().split("\\s");
            for (int j = 0; j < nNodesInSimplex; j++) {
                triangulation[i * nNodesInSimplex + j] = Integer.parseInt(line[j]);
            }
        }

        return triangulation;
    }

    private static void permutations(int[] possibleValues, int len, int startPosition, int[] singleCombination, List<int[]> results) {
        if (len == 0) {
            results.add(singleCombination.clone());
            return;
        }
        for (int i = startPosition; i <= possibleValues.length - len; i++) {
            singleCombination[singleCombination.length - len] = possibleValues[i];
            permutations(possibleValues, len - 1, i + 1, singleCombination, results);
        }
    }

    public static List<int[]> permutationsDecorator(int[] possibleValues, int len) {
        if ((possibleValues.length < len) || (len < 1)) {
            return null;
        }
        List<int[]> results = new ArrayList<>();
        permutations(possibleValues, len, 0, new int[len], results);
        return results;
    }

    /**
     * Get number of permutations of length='lengthSubset' elements without
     * repetitions from original superset of length='lengthSet'.
     *
     * @param lengthSet
     * @param lengthSubset
     * @return
     */
    public static int getNPermutations(int lengthSet, int lengthSubset) {

        if (lengthSet < lengthSubset) {
            return 0;
        } else {
            int[] exampleSet = new int[lengthSet];
            for (int i = 0; i < exampleSet.length; i++) {
                exampleSet[i] = i;
            }
            List<int[]> result = ArrayUtils.permutationsDecorator(exampleSet, lengthSubset);
            return result.size();

        }
    }

    public static Set<Integer> intArrayToSet(int[] arr) {
        Set<Integer> set = new HashSet<>();
        for (int j : arr) {
            set.add(j);
        }
        return set;
    }
}
