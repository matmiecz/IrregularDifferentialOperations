package org.visnow.vn.DiffOpPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import static org.visnow.jscic.cells.CellType.TRIANGLE;
import static org.visnow.vn.DiffOpPlugin.DecimateField.SELECTED;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.visnow.jscic.cells.CellType;
import static org.visnow.jscic.cells.CellType.TETRA;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;
import org.visnow.vn.DiffOpPlugin.FieldDecimation.FieldDecimation;

import org.visnow.vn.lib.utils.sort.IndirectComparator;
import static org.visnow.vn.lib.utils.sort.IndirectSort.indirectSort;

/**
 *
 * @author mateuszmieczkowski
 */
public class PointFieldTriangulation {

    private static final Logger LOGGER = Logger.getLogger(FieldDecimation.class);

    private static final String Q_DELAUNAY_EXEC_PATH = "/Users/mateuszmieczkowski/Documents/ICM_studia/PRACA_MAGISTERSKA_ICM/external_libraries/qhull/qhull/bin/qdelaunay";
    private static final String TMP_DIR = "/tmp";

    Map<Integer, int[]> regionMap = new HashMap<>();

    public Map<Integer, int[]> getRegionMap() {
        return regionMap;
    }

    /**
     * Data container only enabling to retrieve auxilary data structures
     * <ul>
     * <li> regionMap</li>
     * </ul>
     *
     */
    public PointFieldTriangulation() {
    }

    /**
     * Create triangulation of a set of points
     *
     * @param irrField original field
     * @param hierarchyCalculator instance of the class CalculateHierarchy with
     * hierarchy of nodes already computed
     * @param neighbours - NodeNeighbours instance
     * @param nodesMask - nodes to preserve, byte array
     *
     * @return IrregularField with single CellSet, single CellArray, consisting
     * of:
     * <ul>
     * <li> a) for 2D case - TRIANGLES </li>
     * <li>b) for 3D case - TETRAS </li>
     * </ul>
     *
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public IrregularField triangulationNonConvex(IrregularField irrField, CalculateHierarchy hierarchyCalculator, NodeNeighbours neighbours, byte[] nodesMask) throws InternalError, ComputationError {
        IrregularField decimatedField = DecimateField.extractField(irrField, nodesMask);
        boolean is2D = VNUtils.isField2D(irrField);
        CellType ct = is2D ? TRIANGLE : TETRA;

        // ONLY DIFFERENT PART IN BOTH TRIANGULATION FUNCTIONS - TODO REFACTOR (DECORATOR?)
        // --------------------------------------------------
        int[] indicesSelected = decimatedField.getComponent("old_indices").getRawIntArray().getData();
        // nodes in field have to be numbered from 0 -> (nNodes-1)
        // mapping between old node number to new
        Map<Integer, Integer> mappingOldToNew = new HashMap<>();
        for (int i = 0; i < indicesSelected.length; i++) {
            // key - old, value - new
            mappingOldToNew.put(indicesSelected[i], i);
        }

        // triangulation here
        // given existing field coords and selected indices create CellSet
        byte[] hierarchy = hierarchyCalculator.getHierarchyArray();
        int[] connectedComponents = hierarchyCalculator.getConnectedComponents();
        CellSet csOldIndices = parallelRegionGrowing(nodesMask, hierarchy, connectedComponents, neighbours, is2D);

        int[] nodes = csOldIndices.getCellArray(ct).getNodes();
        for (int i = 0; i < nodes.length; i++) {
            int oldIx = nodes[i];
            int newIx = mappingOldToNew.get(oldIx);
            nodes[i] = newIx;
        }
        // --------------------------------------------------

        CellSet csNewIndices = new CellSet();
        csNewIndices.addCells(new CellArray(ct, nodes, null, null));
        decimatedField.addCellSet(csNewIndices);
        return decimatedField;
    }

    public IrregularField triangulationConvex(IrregularField irrField, byte[] nodesMask) throws InternalError, ComputationError, IOException, InterruptedException {
        IrregularField decimatedField = DecimateField.extractField(irrField, nodesMask);
        boolean is2D = VNUtils.isField2D(irrField);
        CellType ct = is2D ? TRIANGLE : TETRA;

        // ONLY DIFFERENT PART IN BOTH TRIANGULATION FUNCTIONS - TODO REFACTOR (DECORATOR?)
        // --------------------------------------------------
        File tmpDir = new File(TMP_DIR);
        File tmpCoordsFile = new File(tmpDir, "tmp_coords.txt");
        File tmpTriangulationResultsFile = new File(tmpDir, "triangulation_results.txt");
        File tmpShellScriptFile = new File(tmpDir, "tmp_shell_script.sh");

        // write coords to file 
        FloatLargeArray coords = (FloatLargeArray) decimatedField.getCoords().getValues().get(0);
        int nDim = decimatedField.getTrueNSpace();
        ArrayUtils.writeCoords(coords, tmpCoordsFile.getAbsolutePath());

        // TODO 
        // invoke external dependency - qdelaunay 
        // invoke qhull program (external dependency) 
        String[] commands = new String[]{
            Q_DELAUNAY_EXEC_PATH,
            "i",
            "Qt",
            "TI",
            tmpCoordsFile.getAbsolutePath(),
            "TO",
            tmpTriangulationResultsFile.getAbsolutePath()
        };

        String command = "";
        for (String s : commands) {
            command = command.concat(s);
            command = command.concat(" ");
        }

        List<String> script = new ArrayList<>();
        script.add("#!/bin/bash");
        script.add(command);

        VNUtils.linesToFile(script, tmpShellScriptFile.getAbsolutePath());

        int[] triangulation;

        Process pr = VNUtils.callShell(tmpShellScriptFile.getAbsolutePath());
        if (pr.exitValue() == 0) {
            triangulation = ArrayUtils.readTriangulationResults(tmpTriangulationResultsFile.getAbsolutePath(), !is2D);

            // cleanup
            tmpShellScriptFile.delete();
            tmpCoordsFile.delete();
            tmpTriangulationResultsFile.delete();
        } else {
            throw new InternalError("error while executing script");
        }

        // --------------------------------------------------
        CellSet csNewIndices = new CellSet();
        csNewIndices.addCells(new CellArray(ct, triangulation, null, null));
        decimatedField.addCellSet(csNewIndices);
        return decimatedField;
    }

    /**
     * parse connected component - groups of nodes gathered by common
     * edges/faces/body;
     *
     * @param connectedComponents - int[] array of length = nNodes;
     * @returns - HashMap: index of the array is nodeIndex; value is hierarchy
     */
    private Map<Integer, Byte> groupConnectedComponentsByHierarchy(int[] connectedComponents, byte[] hierarchyArray) {
        // key=groupID; value=hierarchy (of all nodes in this group)
        Map<Integer, Byte> connectedComponentsByHierarchy = new HashMap<>();

        for (int i = 0; i < connectedComponents.length; i++) {
            int componentID = connectedComponents[i];
            if (!connectedComponentsByHierarchy.containsKey(componentID)) {
                byte h = hierarchyArray[i];
                connectedComponentsByHierarchy.put(componentID, h);
            }
        }
        return connectedComponentsByHierarchy;
    }

    /**
     * Find regions based on nodesMask (selection), hierarchy and
     * connectedComponents
     *
     * Conditions:
     *
     * 1) connectedComponents condition - nodes from two different edges cannot
     * be grouped together into single region (reason - vertex nodes cannot have
     * less than 2/3 connections, cannot be surrounded by only one region)
     *
     *
     * @param nodesMask
     * @param hierarchyArray
     * @param neighbours
     * @param is2D
     * @return
     */
    private CellSet parallelRegionGrowing(byte[] nodesMask, byte[] hierarchyArray, int[] connectedComponents, NodeNeighbours neighbours, boolean is2D) {
        Map<Integer, Byte> connectedComponentsByHierarchy = groupConnectedComponentsByHierarchy(connectedComponents, hierarchyArray);

        int nSelected = ArrayUtils.countByteOccurences(nodesMask, SELECTED);
        int[] selectedNodeIDs = new int[nSelected];

        // key = region number(same as representative node index); value - int[] of node indices in region
        Map<Integer, int[]> tmpRegionMap = new HashMap<>();

        // pairs of neighbouring regions
        // Set of lists not to repeat connections (int[] has no equals() method)
        Set<List<Integer>> neighbourRegionsPairs = new HashSet<>();

        // helper hashmap (key = node_index / value = region_number_that_it_belongs_to)
        Map<Integer, Integer> processed = new HashMap<>();

        int count = 0;
        for (int i = 0; i < nodesMask.length; i++) {
            if (nodesMask[i] == SELECTED) {
                selectedNodeIDs[count++] = i;
                tmpRegionMap.put(i, new int[]{i});
                processed.put(i, i);
            }
        }

        // i.e. selected nodes are processed in order of importance (2nd order diff)
        while (processed.keySet().size() < nodesMask.length) {
            // regions are indexed in the same way as selected node
            for (int regionID : selectedNodeIDs) {
                int[] regionNodes = tmpRegionMap.get(regionID);
                byte regionMaxHierarchy = hierarchyArray[regionID];
                int[] nextNeighboursLayer = neighbours.subsequentNeighboursForNode(regionID, regionNodes);

                // check if not in processed
                List<Integer> tmpList = new ArrayList<>();
                for (int node : regionNodes) {
                    tmpList.add(node);
                }
                for (int nodeID : nextNeighboursLayer) {
                    if (!processed.keySet().contains(nodeID)) {
                        // IMPORTANT
                        // CONDITION 1 - hierarchy of possible new node has to be 
                        // equal or lower than regionID representative node to avoid edge/contour deformation 
                        if (hierarchyArray[nodeID] <= regionMaxHierarchy) {
                            // CONDITION 2/3 (connected components) - EITHER the same group OR group of lower hierarchy 
                            boolean sameComponent = (connectedComponents[nodeID] == connectedComponents[regionID]);
                            boolean lowerHierarchyRegion = connectedComponentsByHierarchy.get(connectedComponents[nodeID]) < connectedComponentsByHierarchy.get(connectedComponents[regionID]);
                            if (sameComponent || lowerHierarchyRegion) {
                                tmpList.add(nodeID);
                                processed.put(nodeID, regionID);
                            }
                        }
                    } else {
                        // node already in other region
                        // find which region contains processed node
                        int currentlyProcessedRegionID = processed.get(nodeID);
                        // apart from the nodes itself
                        if (currentlyProcessedRegionID != regionID) {
                            List<Integer> pair = new ArrayList<>();
                            if (currentlyProcessedRegionID > regionID) {
                                pair.add(regionID);
                                pair.add(currentlyProcessedRegionID);
                            } else {
                                pair.add(currentlyProcessedRegionID);
                                pair.add(regionID);
                            }
                            neighbourRegionsPairs.add(pair);
                        }
                    }
                }
                tmpRegionMap.put(regionID, tmpList.stream().mapToInt(i -> i).toArray());
            }
        }
        this.regionMap = tmpRegionMap;
        LOGGER.info("regionMap_calculated");

        int[][] contacts = new int[neighbourRegionsPairs.size()][];
        Iterator<List<Integer>> iterator = neighbourRegionsPairs.iterator();

        count = 0;
        while (iterator.hasNext()) {
            contacts[count++] = iterator.next().stream().mapToInt(i -> i).toArray();
        }

        IndirectComparator ic = new IndirectComparator() {
            @Override
            public int compare(int i, int j) {
                if (contacts[i][0] < contacts[j][0]) {
                    return -1;
                }
                if (contacts[i][0] > contacts[j][0]) {
                    return 1;
                }
                if (contacts[i][1] < contacts[j][1]) {
                    return -1;
                }
                if (contacts[i][1] > contacts[j][1]) {
                    return 1;
                }
                return 0;
            }
        };

        int[] contactsSortedIndices = new int[contacts.length];
        for (int i = 0; i < contactsSortedIndices.length; i++) {
            contactsSortedIndices[i] = i;
        }
        indirectSort(contactsSortedIndices, ic, false);

        int[][] contactsSorted = new int[contactsSortedIndices.length][];
        count = contactsSortedIndices.length - 1;
        for (int ix : contactsSortedIndices) {
            contactsSorted[count--] = contacts[ix];
        }

        // find cells (triangles for 2D / tetras for 3D)
        Set<Set<Integer>> triangles = new HashSet<>();

        // hashmap index_id - contacts set (contacts for each node)
        Map<Integer, Set<Integer>> contactsMap = new HashMap<>();
        for (int[] pair : contactsSorted) {
            // iterate over both first and second node of pair 
            for (int i = 0; i < 2; i++) {
                Set<Integer> s = contactsMap.get(pair[i]);
                if (s == null) {
                    s = new HashSet<>();
                    contactsMap.put(pair[i], s);
                }
                s.add(pair[1 - i]); // 0 for key 1; 1 for key 0
            }
        }

        // debug contactsMap 
        // each region should have at least:
        // - FOR 2D - 2 neighbouring regions
        // - FOR 3D - 3 neighbouring regions
        Map<Integer, Set<Integer>> debugContactsMapNotSufficient = new HashMap<>();
        for (Map.Entry<Integer, Set<Integer>> entry : contactsMap.entrySet()) {
            Set<Integer> contactsSet = entry.getValue();
            if (contactsSet.size() < 3) {
                debugContactsMapNotSufficient.put(entry.getKey(), contactsSet);
            }
        }

        // find triangles here
        for (int[] pair : contactsSorted) {
            int i = pair[0];
            // find such j that is neighbour of i
            int[] iNeighbours = contactsMap.get(i).stream().mapToInt(a -> a).toArray();
            for (int j : iNeighbours) {
                int[] jNeighbours = contactsMap.get(j).stream().mapToInt(b -> b).toArray();
                for (int k : jNeighbours) {
                    // check if k in iNeighbours
                    for (int l : iNeighbours) {
                        if (l == k) {
                            Set<Integer> triangle = new HashSet<>();
                            triangle.add(i);
                            triangle.add(j);
                            triangle.add(k);
                            triangles.add(triangle);
                        }
                    }
                }
            }
        }
        LOGGER.info("nTriangles found: " + triangles.size());

        CellType ct;
        int[] nodesOrdered;
        Set<Set<Integer>> cellsSetToFlatten;

        if (is2D) {
            ct = TRIANGLE;
            nodesOrdered = new int[3 * triangles.size()];
            cellsSetToFlatten = triangles;
        } else {
            ct = TETRA;
            Set<Set<Integer>> tetrasSet = new HashSet<>();
            // iterate over all nodes
            // find node that has all triangle's nodes as neighbours - then these four form tetrahedron
            LOGGER.info("Type conversion");
            int[] trianglesArr = new int[triangles.size() * 3];
            int k = 0;
            for (Set<Integer> triangle : triangles) {
                for (Integer node : triangle) {
                    trianglesArr[k++] = node;
                }
            }

            int[] contactsMapKeys = contactsMap.keySet().stream().mapToInt(a -> a).toArray();
            int[][] contactsMapArr = new int[contactsMapKeys.length][];
            for (int i = 0; i < contactsMapArr.length; i++) {
                contactsMapArr[i] = contactsMap.get(contactsMapKeys[i]).stream().mapToInt(a -> a).toArray();
            }

            for (int i = 0; i < trianglesArr.length / 3; i++) {
                int firstTriangleNode = trianglesArr[i * 3];
                Set<Integer> possibleCandidates = contactsMap.get(firstTriangleNode);

                // if neigbours of this contain two other nodes then they form tetra
                for (Integer possibleCandidate : possibleCandidates) {
                    Set<Integer> possibleCandidateNeighbours = contactsMap.get(possibleCandidate);
                    if (possibleCandidateNeighbours.contains(trianglesArr[3 * i + 1]) && possibleCandidateNeighbours.contains(trianglesArr[3 * i + 2])) {
                        Set<Integer> tetra = new HashSet<>();
                        for (int j = 0; j < 3; j++) {
                            tetra.add(trianglesArr[3 * i + j]);
                        }
                        tetra.add(possibleCandidate);
                        tetrasSet.add(tetra);
                    }
                }

                // one triangle can form up to two tetras
//                int tetrasCount = 0;
//                for (int j = 0; j < contactsMapArr.length; j++) {
//                    int[] tetra = new int[4];
//                    for (int l = 0; l < tetra.length; l++) {
//                        tetra[l] = -1;
//                    }
//                    for (int node : contactsMapArr[j]) {
//                        for (int l = 0; l < 3; l++) {
//                            if (node == trianglesArr[3 * i + l]) {
//                                tetra[l] = node;
//                            }
//                        }
//                    }
//                    tetra[3] = contactsMapKeys[j];
//                    boolean add = true;
//                    for (int l : tetra) {
//                        if (l == -1) {
//                            add = false;
//                            break;
//                        }
//                    }
//                    if (add){
//                        tetrasList.add(tetra);
//                        tetrasCount++;
//                    }
//                    if (tetrasCount==2){
//                        break;
//                    }
//                }
            }

            LOGGER.info("starting to process tetras - stucks here");

            // algorithm for finding tetras
            // for each triangle find node that is neighbour of all 3 triangle nodes
//            for (Set<Integer> triangle : triangles) {
//                for (Map.Entry<Integer, Set<Integer>> entry : contactsMap.entrySet()) {
//                    if (entry.getValue().containsAll(triangle)) {
//                        Set<Integer> tetra = new HashSet<>(triangle);
//                        tetra.add(entry.getKey());
//                        tetrasSet.add(tetra);
//                    }
//                }
//            }
            nodesOrdered = new int[4 * tetrasSet.size()];
            cellsSetToFlatten = tetrasSet;
        }
        LOGGER.info("Ended processing tetras");

        // flatten sets to 1D int array for cell set creation
        count = 0;
        for (Set<Integer> cell : cellsSetToFlatten) {
            int[] t = cell.stream().mapToInt(a -> a).toArray();
            // is sorting of visnow cells necessary?
            Arrays.sort(t);
            for (int i = t.length - 1; i >= 0; i--) {
                nodesOrdered[count++] = t[i];
            }
        }

        CellSet cs = new CellSet();
        cs.addCells(new CellArray(ct, nodesOrdered, null, null));

        return cs;
    }

}
