//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package org.visnow.vn.DiffOpPlugin.FieldDecimation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.gui.widgets.RunButton.RunState.NO_RUN;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_ONCE;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.DiffOpPlugin.ApproximateDifferentials;
import org.visnow.vn.DiffOpPlugin.CalculateHierarchy;
import org.visnow.vn.DiffOpPlugin.DecimateField;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action.CALCULATE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.ELEMENTS_TO_LEAVE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.MAX_ELEMENTS;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.META_SCALAR_COMPONENT_NAMES;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.META_VECTOR_COMPONENT_NAMES;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.RUNNING_MESSAGE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.SCALAR_ACTIONS;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.VECTOR_ACTIONS;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.FILTERING_ANGLE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.CRITICAL_ANGLE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DEFAULT_FILTERING_ANGLE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DEFAULT_CRITICAL_ANGLE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.ITERATIONS_LIMIT;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DEFAULT_ITERATIONS_LIMIT;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.OUTPUT_TYPE;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;
import org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException;
import org.visnow.vn.DiffOpPlugin.Hierarchy;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.OutputType;
import org.visnow.vn.DiffOpPlugin.NodeNeighbours;
import org.visnow.vn.DiffOpPlugin.PointFieldTriangulation;
import org.visnow.vn.DiffOpPlugin.VNUtils;
import static org.visnow.vn.DiffOpPlugin.VNUtils.IterationsHistory;
import static org.visnow.vn.DiffOpPlugin.VNUtils.IterationMeta;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.LINEAR_RADIUS_CORRECTION;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DEFAULT_ELEMENTS_TO_LEAVE_SHARE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DEFAULT_LOGARITHMIC_ERROR;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.LOGARITHMIC_ERROR;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DEFAULT_DIFF_NORM_WEIGHT_COEFFICIENT;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DEFAULT_ELEMENTS_LEFT_ABSOLUTE_ERROR;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DIFF_NORM_WEIGHT_COEFFICIENT;

/**
 * Module responsible for adaptive field size reduction based on two factors:
 * <ul>
 * <li> 1) geometric hierarchy </li>
 * <li> 2)) data components hierarchy - based on 2nd order differentials </li>
 * </ul>
 *
 ** @author MMieczkowski
 */
public class FieldDecimation extends OutFieldVisualizationModule {

    private static final Logger LOGGER = Logger.getLogger(FieldDecimation.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField = null;
    protected IrregularField triangulatedInField = null;
    protected GUI computeUI = null;
    private int runQueue = 0;

    private ApproximateDifferentials differentialsApproximator = null;
    private CalculateHierarchy hierarchyCalculator = null;
    private byte[] HIERARCHIES_TO_DECIMATE = null;

    public FieldDecimation() {
        parameters.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                    startIfNotInQueue();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                outObj.setName("cell centers");
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters() {

        Map<Byte, Integer> defaultElementsToLeave = new HashMap<>();
        Map<Byte, Integer> defaultMaxElements = new HashMap<>();
        Map<Byte, Double> initialLinearRadiusCorrection = new HashMap<>();
        Map<Byte, Double> initialDiffNormWeightCoefficient = new HashMap<>();
        // init all hierarchies with -1, no matter if field will be 2D or 3D
        for (byte hierarchyType : Hierarchy.getAllValidHierarchyValues()) {
            // init max elements, elements to leave and radius correction
            defaultMaxElements.put(hierarchyType, -1);
            defaultElementsToLeave.put(hierarchyType, -1);
            initialLinearRadiusCorrection.put(hierarchyType, 1.0);
            initialDiffNormWeightCoefficient.put(hierarchyType, DEFAULT_DIFF_NORM_WEIGHT_COEFFICIENT);
        }

        return new Parameter[]{
            new Parameter<>(META_SCALAR_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_VECTOR_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(SCALAR_ACTIONS, new Action[]{}),
            new Parameter<>(VECTOR_ACTIONS, new Action[]{}),
            new Parameter<>(FILTERING_ANGLE, DEFAULT_FILTERING_ANGLE),
            new Parameter<>(CRITICAL_ANGLE, DEFAULT_CRITICAL_ANGLE),
            new Parameter<>(MAX_ELEMENTS, defaultMaxElements),
            new Parameter<>(ELEMENTS_TO_LEAVE, defaultElementsToLeave),
            new Parameter<>(LINEAR_RADIUS_CORRECTION, initialLinearRadiusCorrection),
            new Parameter<>(DIFF_NORM_WEIGHT_COEFFICIENT, initialDiffNormWeightCoefficient),
            new Parameter<>(LOGARITHMIC_ERROR, DEFAULT_LOGARITHMIC_ERROR),
            new Parameter<>(ITERATIONS_LIMIT, DEFAULT_ITERATIONS_LIMIT),
            new Parameter<>(OUTPUT_TYPE, DiffOpPluginShared.OutputType.TRIANGULATED_IRREGULAR_FIELD_PARALLEL_REGIONS),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    private Action[] findActions(ParameterName actionsName, ParameterName metaName, String[] newComponentNames, boolean resetParameters) {
        boolean isFromVNA = isFromVNA();

        Action[] actions = DiffOpPluginShared.findActions(parameters, actionsName, metaName, newComponentNames, resetParameters, isFromVNA);
        return actions;
    }

    private Action[] getUpdatedScalarActions(String[] newScalarComponentNames, boolean resetParameters) {
        return findActions(SCALAR_ACTIONS, META_SCALAR_COMPONENT_NAMES, newScalarComponentNames, resetParameters);
    }

    private Action[] getUpdatedVectorActions(String[] newVectorComponentNames, boolean resetParameters) {
        return findActions(VECTOR_ACTIONS, META_VECTOR_COMPONENT_NAMES, newVectorComponentNames, resetParameters);
    }

    private void validateParamsAndSetSmart(boolean resetParameters) throws MultipleCellSetsException, ComputationError {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
            parameters.set(RUNNING_MESSAGE, NO_RUN);
        }

        double filteringAngle = parameters.get(FILTERING_ANGLE);
        double criticalAngle = parameters.get(CRITICAL_ANGLE);

        double logarithmicError = parameters.get(LOGARITHMIC_ERROR);
        if (logarithmicError <= 0) {
            logarithmicError = DEFAULT_LOGARITHMIC_ERROR;
        }
        parameters.set(LOGARITHMIC_ERROR, logarithmicError);

        /*
        HANDLE NEW INCOMING GEOMETRIC HIERARCHY PARAMETERS 
        3 options:
            1) hierarchy calculator not computed before
            2) change in filteringAngle
            3) change in criticalAngle
         */
        if ((hierarchyCalculator == null) || (hierarchyCalculator.getFilteringAngle() != filteringAngle) || (hierarchyCalculator.getCriticalAngle() != criticalAngle)) {
            hierarchyCalculator = new CalculateHierarchy(triangulatedInField);
            hierarchyCalculator.calculateHierarchyOfNodes(filteringAngle, criticalAngle);

            Map<Byte, Integer> maxElements = new HashMap<>();
            Map<Byte, Integer> elementsToLeave = new HashMap<>();

            for (byte availableHierarchyType : parameters.get(MAX_ELEMENTS).keySet()) {
                int maxElementsForHierarchy = hierarchyCalculator.getNNodesByHierarchy(availableHierarchyType);
                maxElements.put(availableHierarchyType, maxElementsForHierarchy);
                boolean toBeDecimated = false;
                for (byte hierarchyType : HIERARCHIES_TO_DECIMATE) {
                    if (hierarchyType == availableHierarchyType) {
                        toBeDecimated = true;
                        break;
                    }
                }
                int elementsToLeaveForHierarchy = -1;
                if (toBeDecimated) {
                    elementsToLeaveForHierarchy = parameters.get(ELEMENTS_TO_LEAVE).get(availableHierarchyType);
                    if ((elementsToLeaveForHierarchy < 0) || (elementsToLeaveForHierarchy > maxElementsForHierarchy)) {
                        // init
                        // elementsToLeaveForHierarchy = maxElementsForHierarchy;
                        double shareElementsToLeave = DEFAULT_ELEMENTS_TO_LEAVE_SHARE.get(availableHierarchyType);
                        elementsToLeaveForHierarchy = (int) (maxElementsForHierarchy * shareElementsToLeave);
                    }
                }
                elementsToLeave.put(availableHierarchyType, elementsToLeaveForHierarchy);
            }
            parameters.set(MAX_ELEMENTS, maxElements);
            parameters.set(ELEMENTS_TO_LEAVE, elementsToLeave);
        }

        String[] newScalarComponentNames = inField.getScalarComponentNames();
        String[] newVectorComponentNames = inField.getVectorComponentNames();

        Action[] newScalarAction = getUpdatedScalarActions(newScalarComponentNames, resetParameters);
        Action[] newVectorAction = getUpdatedVectorActions(newVectorComponentNames, resetParameters);

        parameters.set(SCALAR_ACTIONS, newScalarAction);
        parameters.set(VECTOR_ACTIONS, newVectorAction);

        parameters.set(META_SCALAR_COMPONENT_NAMES, newScalarComponentNames);
        parameters.set(META_VECTOR_COMPONENT_NAMES, newVectorComponentNames);
        parameters.setParameterActive(true);
    }
    
    public byte[] getHierarchiesToDecimate(){
        return HIERARCHIES_TO_DECIMATE;
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending
    ) {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive() {
        if (getInputFirstValue("inField") != null) {
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            //boolean isDifferentType = ((inField == null) != (newInField == null)) || ((inField instanceof IrregularField) != (newInField instanceof IrregularField));
            //boolean bothIrregular = (inField != null) && (newInField != null) && inField instanceof IrregularField && newInField instanceof IrregularField;

            boolean isNewField = !isFromVNA() && newInField != inField;
            inField = newInField;
            triangulatedInField = inField.getTriangulated();

            // here extract active nodes - is it required in this module?
            CellType ct = VNUtils.getSimplexType(triangulatedInField);
            IrregularField extractedField;
            try {
                extractedField = VNUtils.extractSingleCellSetField(triangulatedInField, ct);
                triangulatedInField = extractedField;
            } catch (ComputationError | MultipleCellSetsException ex) {
                DiffOpPluginShared.handleExceptionVNApp(ex, LOGGER);
            }

            HIERARCHIES_TO_DECIMATE = Hierarchy.getHierarchyValuesForDecimation(triangulatedInField.getTrueNSpace());

            Parameters p;
            synchronized (parameters) {
                // TODO condition when to reset hierarchy Calculator
                // only with new geometry
                if (isNewField) {
                    hierarchyCalculator = null;
                }
                try {
                    validateParamsAndSetSmart(isNewField); //TO ASK!
                } catch (MultipleCellSetsException | ComputationError ex) {
                    DiffOpPluginShared.handleExceptionVNApp(ex, LOGGER);
                    return;
                }

                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA(), isFromVNA() || isNewField);
            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data

                //selection of components to calculate in GUI
                HashMap<String, Action> toCalculate = new HashMap<>();
                String[] scalarComponents = p.get(META_SCALAR_COMPONENT_NAMES);
                String[] vectorComponents = p.get(META_VECTOR_COMPONENT_NAMES);

                for (int i = 0; i < scalarComponents.length; i++) {
                    String component = scalarComponents[i];
                    toCalculate.put(component, p.get(SCALAR_ACTIONS)[i]);
                }
                for (int i = 0; i < vectorComponents.length; i++) {
                    String component = vectorComponents[i];
                    toCalculate.put(component, p.get(VECTOR_ACTIONS)[i]);
                }

                // omit calculation when no component selected for calculation
                boolean anyFieldSelectedToCompute = false;
                for (Map.Entry<String, Action> entry : toCalculate.entrySet()) {
                    if (entry.getValue() == CALCULATE) {
                        anyFieldSelectedToCompute = true;
                    }
                }
                if (anyFieldSelectedToCompute == true) {
                    //==================================================
                    //1) compute approximated differentials coefficients
                    //==================================================
                    boolean addComponents = false; //add 2nd order diff norm to the data field
                    try {
                        differentialsApproximator = new ApproximateDifferentials(triangulatedInField, toCalculate);
                        differentialsApproximator.compute2ndOrdDiffNormField(addComponents);
                    } catch (MultipleCellSetsException | ComputationError ex) {
                        DiffOpPluginShared.handleExceptionVNApp(ex, LOGGER);
                        return;
                    }
                    //==================================================
                    //2) nodes' hierarchy has already been calculated
                    //==================================================
                    //==================================================
                    //3) decimate field according to parameters from GUI
                    //==================================================
                    Map<Byte, Integer> targetElementsToLeave = new HashMap<>();
                    Map<Byte, Integer> maxElements = new HashMap<>();
                    Map<Byte, Double> targetDecimationCoefficients = new HashMap<>();

                    Map<Byte, Double> linearRadiusCorrection = new HashMap<>();
                    Map<Byte, Double> diffNormWeightCoefficients = new HashMap<>();

                    boolean noElementsToDecimate = true;
                    LOGGER.info("\nDECIMATION START\n<==========================>\n");
                    for (byte ht : getHierarchiesToDecimate()) {
                        targetElementsToLeave.put(ht, p.get(ELEMENTS_TO_LEAVE).get(ht));
                        maxElements.put(ht, p.get(MAX_ELEMENTS).get(ht));
                        double targetDecimationCoefficient = (double) targetElementsToLeave.get(ht) / maxElements.get(ht);
                        targetDecimationCoefficients.put(ht, targetDecimationCoefficient);
                        if (targetDecimationCoefficient < 1.0) {
                            noElementsToDecimate = false;
                        }

                        Hierarchy hType = Hierarchy.getType(ht);
                        LOGGER.info("Leave N " + hType.getName() + " elements:\t" + targetElementsToLeave.get(hType.getValue()));
                        LOGGER.info("Target decimation coefficient " + hType.getName() + ":\t" + String.format("%.3f", targetDecimationCoefficients.get(hType.getValue())));
                    }

                    if (noElementsToDecimate) {
                        //no decimation
                        outIrregularField = triangulatedInField;
                        String messageTitle = "No decimation. All points preserved. ";
                        String descriptionMessage = "Hint - select how many elements to preserve. ";
                        VisNow.get().userMessageSend(
                                new UserMessage("VisNow", "FieldDecimation module", messageTitle, descriptionMessage, Level.INFO));
                    } else {
                        DecimateField fieldDecimator = new DecimateField(differentialsApproximator, hierarchyCalculator);

                        // find average distance for each node to its direct neighbours
                        // and use it later to correct radius (node importance)
                        double[] averageDistanceToNeighbour = fieldDecimator.findAverageDistanceToNeighbourForField();
                        DataArray avDist = DataArray.create(averageDistanceToNeighbour, 1, "average_distance_to_neighbour");
                        double meanDistance = avDist.getMeanValue();
                        LOGGER.info("Mean distance for nodes to its direct neighbours: " + String.format("%.6f", meanDistance));
                        fieldDecimator.getField().addComponent(avDist);

                        // initial radius corrections
                        for (byte ht : getHierarchiesToDecimate()) {
                            linearRadiusCorrection.put(ht, p.get(LINEAR_RADIUS_CORRECTION).get(ht));
                            diffNormWeightCoefficients.put(ht, p.get(DIFF_NORM_WEIGHT_COEFFICIENT).get(ht));
                        }
                        double[] originalDataRadius = fieldDecimator.computeOriginalDataRadiusField(targetDecimationCoefficients, diffNormWeightCoefficients);
                        DataArray dataRadiusDA = DataArray.create(originalDataRadius, 1, "Original_data_radius");
                        // scaling (median would be probably better?)
                        double initialRescale = meanDistance / dataRadiusDA.getMeanValue();
                        for (int i = 0; i < originalDataRadius.length; i++) {
                            originalDataRadius[i] *= initialRescale;
                        }
                        fieldDecimator.setOriginalDataRadius(originalDataRadius);
                        dataRadiusDA = DataArray.create(originalDataRadius, 1, "Original_data_radius");
                        fieldDecimator.getField().addComponent(dataRadiusDA);

                        // max logarithmic error 
                        // err = |log10(nLeft)-log10(nTarget)|
                        // e.g. target elements are 10000; err = 0.5:
                        // 3.5 < log10(nLeft) < 4.5 => 10^(3.5) < nLeft < 10^4.5 => 3162 < nLeft < 31622
                        boolean compute = true;
                        double allowedLogarithmicError = p.get(LOGARITHMIC_ERROR);
                        int allowedAbsoluteError = DEFAULT_ELEMENTS_LEFT_ABSOLUTE_ERROR;

                        LOGGER.info("Allowed remaining elements logarithmic error:\t" + String.format("%.2f", allowedLogarithmicError));
                        LOGGER.info("Allowed remaining elements absolute error:\t" + allowedAbsoluteError);

                        byte[] nodesMask = null;
                        int nIterations = 0;
                        int iterationsLimit = p.get(ITERATIONS_LIMIT);

                        while (compute) {
                            try {
                                nodesMask = fieldDecimator.decimationIteration(targetElementsToLeave, linearRadiusCorrection, diffNormWeightCoefficients);
                            } catch (MultipleCellSetsException | InternalError | ComputationError ex) {
                                DiffOpPluginShared.handleExceptionVNApp((Exception) ex, LOGGER);
                                return;
                            }
                            compute = false;

                            IterationsHistory iterationsHistory = fieldDecimator.getDecimationHistory();

                            // compute next radius correction if necessary; report iteration
                            for (byte ht : getHierarchiesToDecimate()) {
                                String typeName = Hierarchy.getType(ht).getName();
                                int bestIterationForHierarchyIndex = iterationsHistory.getClosestToTargetIterationNumber(ht);
                                IterationMeta bestIterationForHierarchy = iterationsHistory.getIteration(bestIterationForHierarchyIndex, ht);
                                String reportMessage = "Best Iteration: ";
                                reportMessage += iterationsHistory.getIterationReport(bestIterationForHierarchyIndex, ht);

                                boolean anotherIterationForHierarchyRequired = bestIterationForHierarchy.anotherIterationRequired(allowedAbsoluteError, allowedLogarithmicError);
                                if (anotherIterationForHierarchyRequired) {
                                    Map<String, Double> newRadiusCorrections = DecimateField.findLinearRadiusCorrection(ht, iterationsHistory, targetElementsToLeave.get(ht), maxElements.get(ht));
                                    linearRadiusCorrection.replace(ht, newRadiusCorrections.get("linear"));
                                    LOGGER.info("LINEAR RADIUS CORRECTION NECESSARY FOR " + typeName + ":");
                                    reportMessage += ";Next_linear_radius_correction=" + String.format("%.3f", linearRadiusCorrection.get(ht));
                                } else {
                                    linearRadiusCorrection.replace(ht, bestIterationForHierarchy.getLinearCorrection());
                                    LOGGER.info("No linear radius correction for " + typeName);
                                    reportMessage += "Hierarchy=" + typeName + " CONVERGED.";
                                }
                                // stop computation only when 'compute' was set 'false' for all hierarchies before
                                // and for currently processed hierarchy is 'false' as well
                                compute = compute || anotherIterationForHierarchyRequired;
                                LOGGER.info(reportMessage);
                                LOGGER.info("-------------------------------------------------");
                                VisNow.get().userMessageSend(new UserMessage("VisNow", "FieldDecimation module", reportMessage, "", Level.INFO));
                            }

                            nIterations++;
                            if (nIterations >= iterationsLimit) {
                                compute = false;
                                String iterationsLimitMessage = "Iterations limit " + iterationsLimit + " exceeded. Further enhancement required (resulting decimated field may have poor quality). ";
                                LOGGER.info(iterationsLimitMessage);
                                VisNow.get().userMessageSend(
                                        new UserMessage("VisNow", "FieldDecimation module", iterationsLimitMessage, "", Level.WARNING));
                            }
                        }

                        if (!fieldDecimator.latestIterationBest()){
                            nodesMask = fieldDecimator.computeBestSelectionMask(targetElementsToLeave, allowedAbsoluteError, allowedLogarithmicError);
                        }
                        LOGGER.info("N ITERATIONS REQUIRED:\t" + (nIterations - 1));

                        // ------------------------------------------------------------------
                        // OUTPUT TYPE
                        // ------------------------------------------------------------------
                        // select output according to parameter OUTPUT_TYPE
                        OutputType outputType = parameters.get(OUTPUT_TYPE);

                        // NO TRIANGULATION
                        if (outputType == OutputType.POINT_FIELD) {
                            // OPTION 1 - extract points according to nodes mask -> return point field with reducted number of nodes
                            IrregularField extractedPointField;
                            try {
                                // SELECT FIELD HERE FROM NODES MASK
                                extractedPointField = DecimateField.extractPointField(fieldDecimator.getField(), nodesMask);
                            } catch (IllegalArgumentException | ComputationError ex) {
                                DiffOpPluginShared.handleExceptionVNApp(ex, LOGGER);
                                return;
                            }
                            outIrregularField = extractedPointField;
                            // TRIANGULATION
                        } else {
                            // triangulate field
                            IrregularField decimatedField;
                            NodeNeighbours nodeNeighbours = differentialsApproximator.getNeighbourCounter();
                            PointFieldTriangulation triangulator = new PointFieldTriangulation();

                            // OPTION 2 - BEST FOR CONVEX GEOMETRY
                            // return decimated field with new cell set and reduced number of elements 
                            // invoke external dependency qdelaunay
                            if (outputType == OutputType.TRIANGULATED_IRREGULAR_FIELD_QDELAUNAY) {
                                try {
                                    decimatedField = triangulator.triangulationConvex(triangulatedInField, nodesMask);
                                    outIrregularField = decimatedField;
                                } catch (InternalError | ComputationError | IOException | InterruptedException ex) {
                                    DiffOpPluginShared.handleExceptionVNApp((Exception) ex, LOGGER);
                                }
                            } else {
                                try {
                                    decimatedField = triangulator.triangulationNonConvex(triangulatedInField, hierarchyCalculator, nodeNeighbours, nodesMask);
                                } catch (InternalError | ComputationError ex) {
                                    DiffOpPluginShared.handleExceptionVNApp((Exception) ex, LOGGER);
                                    return;
                                }
                                if (outputType == OutputType.TRIANGULATED_IRREGULAR_FIELD_PARALLEL_REGIONS) {
                                    //OPTION 3 - BEST FOR CONVEX GEOMETRY
                                    // return decimated field with new cell set and reduced number of elements
                                    outIrregularField = decimatedField;
                                } else {
                                    // OPTION 4 - DEBUG CUSTOM TRIANGULATION 
                                    // preserve original field and only add new components (i.e.)
                                    // - selected nodes mask 
                                    // - grouping
                                    fieldDecimator.setNodesMask(nodesMask);
                                    DataArray nodesMaskDA = DataArray.create(fieldDecimator.getNodesMask(), 1, "nodes_mask");
                                    triangulatedInField.addComponent(nodesMaskDA);

                                    // region map - optional to visualise with original field
                                    // ------------------------------------------------------------------
                                    Map<Integer, int[]> regionGrowing = triangulator.getRegionMap();
                                    // create field with groups 
                                    int[] grouping = new int[nodesMask.length];
                                    for (Map.Entry<Integer, int[]> entry : regionGrowing.entrySet()) {
                                        int value = entry.getKey();
                                        int[] nodes = entry.getValue();
                                        for (int node : nodes) {
                                            grouping[node] = value;
                                        }
                                    }
                                    triangulatedInField.addComponent(DataArray.create(grouping, 1, "Grouping"));
                                    outIrregularField = triangulatedInField;
                                }
                            }
                        }
                    }
                    outObj.clearAllGeometry();
                    outGroup = null;
                    outObj.setName(inField.getName());
                    outField = outIrregularField;
                    setOutputValue("outField", new VNIrregularField(outIrregularField));

                    prepareOutputGeometry();
                    show();
                } else {
                    String messageTitle = "No data component selected";
                    String descriptionMessage
                            = "User has to select which scalar/vector components should be taken into\n"
                            + "account while decimating field.";
                    VisNow.get().userMessageSend(
                            new UserMessage("VisNow", "FieldDecimation module", messageTitle, descriptionMessage, Level.INFO));
                }
            }
        }
    }
}
