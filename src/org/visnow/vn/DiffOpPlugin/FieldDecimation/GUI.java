package org.visnow.vn.DiffOpPlugin.FieldDecimation;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action.CALCULATE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.OutputType;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.META_SCALAR_COMPONENT_NAMES;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.META_VECTOR_COMPONENT_NAMES;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.RUNNING_MESSAGE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.SCALAR_ACTIONS;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.VECTOR_ACTIONS;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.ELEMENTS_TO_LEAVE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.MAX_ELEMENTS;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.FILTERING_ANGLE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.CRITICAL_ANGLE;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.ITERATIONS_LIMIT;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.OUTPUT_TYPE;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.RunButton.RunState;
import org.visnow.vn.system.swing.FixedGridBagLayoutPanel;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action;
import org.visnow.vn.DiffOpPlugin.Hierarchy;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.LINEAR_RADIUS_CORRECTION;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.LOGARITHMIC_ERROR;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.DIFF_NORM_WEIGHT_COEFFICIENT;

/**
 *
 * @author MMieczkowski
 */
public final class GUI extends FixedGridBagLayoutPanel {

    protected Parameters parameters = null;
    protected boolean tableActive = false;  // this flag is necessary to distinguish user action on jTable from setter actions.

    protected final static Action[] COMPONENT_COLUMN_ORDER = {null, CALCULATE};
    protected final static int[] COMPONENTS_COLUMN_WIDTHS = new int[]{100, 20};
    protected final static String[] SCALAR_COMPONENT_TABLE_TOOLTIPS = {null, "Select scalar components considered for field decimation"};
    protected final static String[] VECTOR_COMPONENT_TABLE_TOOLTIPS = {null, "Select vector components considered for field decimation"};

    // <editor-fold defaultstate="collapsed" desc="Scalar components table">
    protected DefaultTableModel scalarComponentsTableModel = new DefaultTableModel(new Object[][]{{null, null}},
            new String[]{"Component", "Select"}) {
        Class[] types = new Class[]{java.lang.String.class, java.lang.Boolean.class};
        boolean[] canEdit = new boolean[]{false, true};

        @Override
        public Class getColumnClass(int columnIndex) {
            return types[columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };

    protected javax.swing.JTable scalarComponentTable;

    protected JTableHeader createScalarComponentTableHeader() {
        return new JTableHeader(scalarComponentTable.getColumnModel()) {
            @Override
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realIndex = columnModel.getColumn(index).getModelIndex();
                return SCALAR_COMPONENT_TABLE_TOOLTIPS[realIndex];
            }
        };
    }
    ;

    protected TableModelListener scalarComponentTableListener = new TableModelListener() {
        @Override
        public void tableChanged(TableModelEvent e) {

            if (scalarComponentTable.getModel().getRowCount() == parameters.get(META_SCALAR_COMPONENT_NAMES).length) {
                Action[] actions = new Action[parameters.get(META_SCALAR_COMPONENT_NAMES).length];
                for (int row = 0; row < actions.length; row++) {
                    int column = 1;
                    if ((Boolean) scalarComponentTable.getModel().getValueAt(row, column)) {
                        actions[row] = COMPONENT_COLUMN_ORDER[column];
                    }
                }
                if (tableActive) {
                    runButton.setPendingIfNoAuto();
                }
                parameters.set(SCALAR_ACTIONS, actions);
            }
        }
    };// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Vector components table">
    protected DefaultTableModel vectorComponentsTableModel = new DefaultTableModel(new Object[][]{{null, null}},
            new String[]{"Component", "Select"}) {
        Class[] types = new Class[]{java.lang.String.class, java.lang.Boolean.class};
        boolean[] canEdit = new boolean[]{false, true};

        @Override
        public Class getColumnClass(int columnIndex) {
            return types[columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };

    protected javax.swing.JTable vectorComponentTable;

    protected JTableHeader createVectorComponentTableHeader() {
        return new JTableHeader(vectorComponentTable.getColumnModel()) {
            @Override
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realIndex = columnModel.getColumn(index).getModelIndex();
                return VECTOR_COMPONENT_TABLE_TOOLTIPS[realIndex];
            }
        };
    }
    ;
    
    protected TableModelListener vectorComponentTableListener = new TableModelListener() {
        @Override
        public void tableChanged(TableModelEvent e) {

            if (vectorComponentTable.getModel().getRowCount() == parameters.get(META_VECTOR_COMPONENT_NAMES).length) {
                Action[] actions = new Action[parameters.get(META_VECTOR_COMPONENT_NAMES).length];
                for (int row = 0; row < actions.length; row++) {
                    int column = 1;
                    if ((Boolean) vectorComponentTable.getModel().getValueAt(row, column)) {
                        actions[row] = COMPONENT_COLUMN_ORDER[column];
                    }
                }
                if (tableActive) {
                    runButton.setPendingIfNoAuto();
                }
                parameters.set(VECTOR_ACTIONS, actions);
            }
        }
    };
    // </editor-fold>

    /**
     * Creates new form GUI
     */
    // <editor-fold defaultstate="collapsed" desc="GUI">
    public GUI() {
        initComponents();

        scalarComponentTable = new javax.swing.JTable();
        scalarComponentTable.setModel(scalarComponentsTableModel);
        for (int i = 0; i < COMPONENTS_COLUMN_WIDTHS.length; i++) {
            scalarComponentTable.getColumnModel().getColumn(i).setPreferredWidth(COMPONENTS_COLUMN_WIDTHS[i]);
        }
        scalarComponentTable.getTableHeader().setReorderingAllowed(false);
        scalarComponentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        ((DefaultTableModel) scalarComponentTable.getModel()).addTableModelListener(scalarComponentTableListener);
        JPanel scalarComponentTablePanel = new JPanel();

        scalarComponentTablePanel.setLayout(new BorderLayout());
        scalarComponentTablePanel.add(scalarComponentTable, BorderLayout.CENTER);
        scalarComponentTablePanel.add(createScalarComponentTableHeader(), BorderLayout.NORTH);
        scalarComponentsScrollPane.getViewport().add(scalarComponentTablePanel);

        vectorComponentTable = new javax.swing.JTable();
        vectorComponentTable.setModel(vectorComponentsTableModel);
        for (int i = 0; i < COMPONENTS_COLUMN_WIDTHS.length; i++) {
            vectorComponentTable.getColumnModel().getColumn(i).setPreferredWidth(COMPONENTS_COLUMN_WIDTHS[i]);
        }
        vectorComponentTable.getTableHeader().setReorderingAllowed(false);
        vectorComponentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        ((DefaultTableModel) vectorComponentTable.getModel()).addTableModelListener(vectorComponentTableListener);
        JPanel vectorComponentTablePanel = new JPanel();

        vectorComponentTablePanel.setLayout(new BorderLayout());
        vectorComponentTablePanel.add(vectorComponentTable, BorderLayout.CENTER);
        vectorComponentTablePanel.add(createVectorComponentTableHeader(), BorderLayout.NORTH);
        vectorComponentsScrollPane.getViewport().add(vectorComponentTablePanel);

        runButton.setPendingIfNoAuto();
    }// </editor-fold>

    /**
     * @param p cloned parameters
     * @param resetFully
     * @param setRunButtonPending
     */
    public void updateGUI(final ParameterProxy p, boolean resetFully, boolean setRunButtonPending) {
        tableActive = false;

        // GEOMETRY 
        // --------------------------------------------------------
        filteringAngleSlider.setValue(p.get(FILTERING_ANGLE));
        criticalAngleSlider.setValue(p.get(CRITICAL_ANGLE));

        // get from gui how many edge and inside elements to leave;
        Map<Byte, Integer> elementsToLeave = p.get(ELEMENTS_TO_LEAVE);
        Map<Byte, Integer> maxElements = p.get(MAX_ELEMENTS);
        double remainingElementsError = p.get(LOGARITHMIC_ERROR);

        edgeElementsSlider.setMax(maxElements.get(Hierarchy.EDGE.getValue()));
        surfaceElementsSlider.setMax(maxElements.get(Hierarchy.SURFACE.getValue()));
        insideElementsSlider.setMax(maxElements.get(Hierarchy.INSIDE.getValue()));

        vertexHierarchyResult.setText(maxElements.get(Hierarchy.VERTEX.getValue()).toString());

        edgeElementsSlider.setValue(elementsToLeave.get(Hierarchy.EDGE.getValue()));
        surfaceElementsSlider.setValue(elementsToLeave.get(Hierarchy.SURFACE.getValue()));
        insideElementsSlider.setValue(elementsToLeave.get(Hierarchy.INSIDE.getValue()));

        double defaultMaxLogarithmicError = 1.0f;
        double maxRemainingElementsError = (remainingElementsError > defaultMaxLogarithmicError) ? remainingElementsError * 2 : defaultMaxLogarithmicError;
        double defaultMinElementsError = 0.1f;
        double minRemainingElementsError = (remainingElementsError < defaultMaxLogarithmicError) ? remainingElementsError / 2 : defaultMinElementsError;
        remainingElementsErrorSlider.setMin((float) minRemainingElementsError);
        remainingElementsErrorSlider.setMax((float) maxRemainingElementsError);
        remainingElementsErrorSlider.setVal((float) remainingElementsError);

        // disable inside elements' panel for 2D cases; -1 should be set in parameter validation
//        insideElementsPanel.setVisible(elementsToLeave.get(Hierarchy.INSIDE.getValue()) > 0);
        boolean options3DVisible = is3D();

        insideElementsSlider.setEnabled(options3DVisible);
        jLabelCorrInside.setEnabled(options3DVisible);
        linearCorrectionValueInside.setEnabled(options3DVisible);
        diffNormWeightCoefficientInside.setEnabled(options3DVisible);

        maxIterations.setValue(p.get(ITERATIONS_LIMIT));

        // DATA COMPONENTS 
        // --------------------------------------------------------
        scalarPanel.setVisible(p.get(META_SCALAR_COMPONENT_NAMES).length > 0);
        DefaultTableModel scalarComponentTableModel = (DefaultTableModel) scalarComponentTable.getModel();
        scalarComponentTableModel.setRowCount(0);

        Object[] row = new Object[COMPONENT_COLUMN_ORDER.length];
        for (int i = 0; i < p.get(META_SCALAR_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_SCALAR_COMPONENT_NAMES)[i];
            Action action = p.get(SCALAR_ACTIONS)[i];
            if (action == CALCULATE) {
                row[1] = true;
            }
            scalarComponentTableModel.addRow(row);
        }

        vectorPanel.setVisible(p.get(META_VECTOR_COMPONENT_NAMES).length > 0);
        DefaultTableModel vectorComponentTableModel
                = (DefaultTableModel) vectorComponentTable.getModel();
        vectorComponentTableModel.setRowCount(0);

        row = new Object[COMPONENT_COLUMN_ORDER.length];
        for (int i = 0; i < p.get(META_VECTOR_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_VECTOR_COMPONENT_NAMES)[i];
            Action action = p.get(VECTOR_ACTIONS)[i];
            if (action == CALCULATE) {
                row[1] = true;
            }
            vectorComponentTableModel.addRow(row);
        }

        tableActive = true;

        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        componentsSelectionPanel = new javax.swing.JPanel();
        scalarPanel = new javax.swing.JPanel();
        scalarComponentsScrollPane = new javax.swing.JScrollPane();
        vectorPanel = new javax.swing.JPanel();
        vectorComponentsScrollPane = new javax.swing.JScrollPane();
        geometricHierarchyPanel = new javax.swing.JPanel();
        filteringAngleSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        criticalAngleSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        automaticDecimationPanel = new javax.swing.JPanel();
        vertexHierarchyResult = new javax.swing.JLabel();
        edgeElementsSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        surfaceElementsSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        insideElementsSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        remainingElementsErrorPanel = new javax.swing.JPanel();
        remainingElementsErrorDescriptionLabel = new javax.swing.JLabel();
        remainingElementsErrorSlider = new org.visnow.vn.gui.widgets.FloatSlider();
        computationsProperties = new javax.swing.JPanel();
        correctionPanel = new javax.swing.JPanel();
        jLabelCorrectionNames = new javax.swing.JLabel();
        jLabelCorrEdge = new javax.swing.JLabel();
        jLabelCorrSurface = new javax.swing.JLabel();
        jLabelCorrInside = new javax.swing.JLabel();
        jLabelLinearCorrectionValues = new javax.swing.JLabel();
        linearCorrectionValueEdge = new javax.swing.JSpinner();
        linearCorrectionValueSurface = new javax.swing.JSpinner();
        linearCorrectionValueInside = new javax.swing.JSpinner();
        jLabelDiffNormWeightCoefficientValues = new javax.swing.JLabel();
        diffNormWeightCoefficientValueEdge = new javax.swing.JSpinner();
        diffNormWeightCoefficientValueSurface = new javax.swing.JSpinner();
        diffNormWeightCoefficientInside = new javax.swing.JSpinner();
        dataRadiusFormulaLabel = new javax.swing.JLabel();
        maxIterations = new javax.swing.JSpinner();
        outputTypeSelectionPanel = new javax.swing.JPanel();
        triangulateParallelRegionsButton = new javax.swing.JRadioButton();
        triangulateQDelaunayButton = new javax.swing.JRadioButton();
        pointFieldButton = new javax.swing.JRadioButton();
        originalFieldDebugButton = new javax.swing.JRadioButton();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        filler = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        java.awt.GridBagLayout layout = new java.awt.GridBagLayout();
        layout.columnWidths = new int[] {0};
        layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        setLayout(layout);

        componentsSelectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "COMPONENTS SELECTION", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        componentsSelectionPanel.setLayout(new java.awt.GridBagLayout());

        scalarPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("SCALAR COMPONENTS"));
        scalarPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        scalarPanel.add(scalarComponentsScrollPane, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        componentsSelectionPanel.add(scalarPanel, gridBagConstraints);

        vectorPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("VECTOR COMPONENTS"));
        vectorPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        vectorPanel.add(vectorComponentsScrollPane, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        componentsSelectionPanel.add(vectorPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(componentsSelectionPanel, gridBagConstraints);

        geometricHierarchyPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "GEOMETRIC HIERARCHY PARAMETERS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        geometricHierarchyPanel.setLayout(new java.awt.GridBagLayout());

        filteringAngleSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        filteringAngleSlider.setGlobalMax(100

        );
        filteringAngleSlider.setGlobalMin(0.01);
        filteringAngleSlider.setMin(0.1);
        filteringAngleSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        filteringAngleSlider.setShowingFields(true);
        filteringAngleSlider.setSubmitOnAdjusting(false);
        filteringAngleSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("filtering angle"));
        filteringAngleSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                filteringAngleSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        geometricHierarchyPanel.add(filteringAngleSlider, gridBagConstraints);

        criticalAngleSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        criticalAngleSlider.setGlobalMax(100

        );
        criticalAngleSlider.setGlobalMin(0.01);
        criticalAngleSlider.setMin(0.1);
        criticalAngleSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        criticalAngleSlider.setShowingFields(true);
        criticalAngleSlider.setSubmitOnAdjusting(false);
        criticalAngleSlider.setValue(0.1);
        criticalAngleSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("critical angle"));
        criticalAngleSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                criticalAngleSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        geometricHierarchyPanel.add(criticalAngleSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(geometricHierarchyPanel, gridBagConstraints);

        automaticDecimationPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DECIMATION PARAMETERS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        automaticDecimationPanel.setLayout(new java.awt.GridBagLayout());

        vertexHierarchyResult.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        vertexHierarchyResult.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        vertexHierarchyResult.setText("none");
        vertexHierarchyResult.setAlignmentY(0.0F);
        vertexHierarchyResult.setBorder(javax.swing.BorderFactory.createTitledBorder("VERTEX ELEMENTS TO LEAVE"));
        vertexHierarchyResult.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        automaticDecimationPanel.add(vertexHierarchyResult, gridBagConstraints);

        edgeElementsSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        edgeElementsSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        edgeElementsSlider.setShowingFields(true);
        edgeElementsSlider.setSubmitOnAdjusting(false);
        edgeElementsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("EDGE ELEMENTS TO LEAVE"));
        edgeElementsSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                edgeElementsSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        automaticDecimationPanel.add(edgeElementsSlider, gridBagConstraints);
        edgeElementsSlider.getAccessibleContext().setAccessibleName("edgeElementsToLeave");

        surfaceElementsSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        surfaceElementsSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        surfaceElementsSlider.setShowingFields(true);
        surfaceElementsSlider.setSubmitOnAdjusting(false);
        surfaceElementsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("SURFACE ELEMENTS TO LEAVE"));
        surfaceElementsSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                surfaceElementsSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        automaticDecimationPanel.add(surfaceElementsSlider, gridBagConstraints);
        surfaceElementsSlider.getAccessibleContext().setAccessibleName("surface elements to leave");

        insideElementsSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        insideElementsSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        insideElementsSlider.setShowingFields(true);
        insideElementsSlider.setSubmitOnAdjusting(false);
        insideElementsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("INSIDE ELEMENTS TO LEAVE"));
        insideElementsSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                insideElementsSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        automaticDecimationPanel.add(insideElementsSlider, gridBagConstraints);
        insideElementsSlider.getAccessibleContext().setAccessibleName("inside elements to leave");

        remainingElementsErrorPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MAX ERROR FOR REMAINING ELEMENTS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 2, 11))); // NOI18N
        remainingElementsErrorPanel.setMinimumSize(new java.awt.Dimension(320, 50));
        remainingElementsErrorPanel.setPreferredSize(new java.awt.Dimension(321, 50));
        remainingElementsErrorPanel.setLayout(new java.awt.GridBagLayout());

        remainingElementsErrorDescriptionLabel.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
        remainingElementsErrorDescriptionLabel.setText("err = |log10(nLeft)-log10(nTarget)|");
        remainingElementsErrorDescriptionLabel.setAlignmentY(0.0F);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        remainingElementsErrorPanel.add(remainingElementsErrorDescriptionLabel, gridBagConstraints);

        remainingElementsErrorSlider.setMin(0.1F);
        remainingElementsErrorSlider.setVal(0.2F);
        remainingElementsErrorSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                remainingElementsErrorSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 6;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
        remainingElementsErrorPanel.add(remainingElementsErrorSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 41;
        automaticDecimationPanel.add(remainingElementsErrorPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridheight = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(automaticDecimationPanel, gridBagConstraints);

        computationsProperties.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "COMPUTATION PROPERTIES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        computationsProperties.setLayout(new java.awt.GridBagLayout());

        correctionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATA RADIUS PARAMETERS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 2, 11))); // NOI18N
        correctionPanel.setToolTipText("Data radius is computed from formula:\n r = \u00B5 * (1/(D^2+\u03B5))*CONST");
        correctionPanel.setLayout(new java.awt.GridBagLayout());

        jLabelCorrectionNames.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        jLabelCorrectionNames.setText("Hierarchy:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        correctionPanel.add(jLabelCorrectionNames, gridBagConstraints);

        jLabelCorrEdge.setText("Edge correction:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        correctionPanel.add(jLabelCorrEdge, gridBagConstraints);

        jLabelCorrSurface.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelCorrSurface.setText("Surface correction:");
        jLabelCorrSurface.setToolTipText("");
        jLabelCorrSurface.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        correctionPanel.add(jLabelCorrSurface, gridBagConstraints);

        jLabelCorrInside.setText("Inside correction:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        correctionPanel.add(jLabelCorrInside, gridBagConstraints);

        jLabelLinearCorrectionValues.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        jLabelLinearCorrectionValues.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelLinearCorrectionValues.setText("\u00B5");
        jLabelLinearCorrectionValues.setToolTipText("Initial linear correction (this value is changed after every iteration)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        correctionPanel.add(jLabelLinearCorrectionValues, gridBagConstraints);

        linearCorrectionValueEdge.setModel(new javax.swing.SpinnerNumberModel(1.0d, 0.0d, null, 1.0d));
        linearCorrectionValueEdge.setPreferredSize(new java.awt.Dimension(80, 26));
        linearCorrectionValueEdge.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                linearCorrectionValueEdgeStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.3;
        correctionPanel.add(linearCorrectionValueEdge, gridBagConstraints);

        linearCorrectionValueSurface.setModel(new javax.swing.SpinnerNumberModel(1.0d, 0.0d, null, 1.0d));
        linearCorrectionValueSurface.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                linearCorrectionValueSurfaceStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.3;
        correctionPanel.add(linearCorrectionValueSurface, gridBagConstraints);

        linearCorrectionValueInside.setModel(new javax.swing.SpinnerNumberModel(1.0d, 0.0d, null, 1.0d));
        linearCorrectionValueInside.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        linearCorrectionValueInside.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                linearCorrectionValueInsideStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.3;
        correctionPanel.add(linearCorrectionValueInside, gridBagConstraints);

        jLabelDiffNormWeightCoefficientValues.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        jLabelDiffNormWeightCoefficientValues.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelDiffNormWeightCoefficientValues.setText("\u03B5");
        jLabelDiffNormWeightCoefficientValues.setToolTipText("user parameter - CONSTANT for every iteration");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        correctionPanel.add(jLabelDiffNormWeightCoefficientValues, gridBagConstraints);

        diffNormWeightCoefficientValueEdge.setModel(new javax.swing.SpinnerNumberModel(0.1d, 0.0d, null, 0.1d));
        diffNormWeightCoefficientValueEdge.setPreferredSize(new java.awt.Dimension(80, 26));
        diffNormWeightCoefficientValueEdge.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                diffNormWeightCoefficientValueEdgeStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.3;
        correctionPanel.add(diffNormWeightCoefficientValueEdge, gridBagConstraints);

        diffNormWeightCoefficientValueSurface.setModel(new javax.swing.SpinnerNumberModel(0.1d, 0.0d, null, 0.1d));
        diffNormWeightCoefficientValueSurface.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                diffNormWeightCoefficientValueSurfaceStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.3;
        correctionPanel.add(diffNormWeightCoefficientValueSurface, gridBagConstraints);

        diffNormWeightCoefficientInside.setModel(new javax.swing.SpinnerNumberModel(0.1d, 0.0d, null, 0.1d));
        diffNormWeightCoefficientInside.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        diffNormWeightCoefficientInside.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                diffNormWeightCoefficientInsideStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.3;
        correctionPanel.add(diffNormWeightCoefficientInside, gridBagConstraints);

        dataRadiusFormulaLabel.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
        dataRadiusFormulaLabel.setText("R = \u00B5 * (1/(D^2+\u03B5))*CONST");
        dataRadiusFormulaLabel.setAlignmentY(0.0F);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        correctionPanel.add(dataRadiusFormulaLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        computationsProperties.add(correctionPanel, gridBagConstraints);
        correctionPanel.getAccessibleContext().setAccessibleName("DATA RADIUS SETTINGS");

        maxIterations.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        maxIterations.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MAX ITERATIONS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 2, 11))); // NOI18N
        maxIterations.setMinimumSize(new java.awt.Dimension(80, 48));
        maxIterations.setPreferredSize(new java.awt.Dimension(120, 45));
        maxIterations.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                maxIterationsStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        computationsProperties.add(maxIterations, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 25;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(computationsProperties, gridBagConstraints);

        outputTypeSelectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "OUTPUT TYPE", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        outputTypeSelectionPanel.setLayout(new java.awt.GridBagLayout());

        triangulateParallelRegionsButton.setSelected(true);
        triangulateParallelRegionsButton.setText("triangulated irregular field (non-convex geometry)");
        triangulateParallelRegionsButton.setToolTipText("imperfect method - may cause geoemtry distortion");
        triangulateParallelRegionsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                triangulateParallelRegionsButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        outputTypeSelectionPanel.add(triangulateParallelRegionsButton, gridBagConstraints);

        triangulateQDelaunayButton.setText("triangulated irregular field (convex geometry)");
        triangulateQDelaunayButton.setToolTipText("Based on convex hull algorithm (qhull). Works well, but only for CONVEX geometries.");
        triangulateQDelaunayButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                triangulateQDelaunayButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        outputTypeSelectionPanel.add(triangulateQDelaunayButton, gridBagConstraints);

        pointFieldButton.setText("irregular point field");
        pointFieldButton.setToolTipText("Only points selection, do not triangulate field");
        pointFieldButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pointFieldButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        outputTypeSelectionPanel.add(pointFieldButton, gridBagConstraints);

        originalFieldDebugButton.setFont(new java.awt.Font("Lucida Grande", 2, 13)); // NOI18N
        originalFieldDebugButton.setText("original field with triangulation results mapped (debug)");
        originalFieldDebugButton.setToolTipText("Map debugging components to field");
        originalFieldDebugButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                originalFieldDebugButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        outputTypeSelectionPanel.add(originalFieldDebugButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 26;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(outputTypeSelectionPanel, gridBagConstraints);

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                runButtonUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 28;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 30;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(filler, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameters.set(RUNNING_MESSAGE, (RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    private void remainingElementsErrorSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_remainingElementsErrorSliderStateChanged
        parameters.set(LOGARITHMIC_ERROR, (double) remainingElementsErrorSlider.getVal());
    }//GEN-LAST:event_remainingElementsErrorSliderStateChanged

    private void linearCorrectionValueEdgeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_linearCorrectionValueEdgeStateChanged
        Map<Byte, Double> radiusCorrection = parameters.get(LINEAR_RADIUS_CORRECTION);
        radiusCorrection.replace(Hierarchy.EDGE.getValue(), (double) linearCorrectionValueEdge.getValue());
        parameters.set(LINEAR_RADIUS_CORRECTION, radiusCorrection);
    }//GEN-LAST:event_linearCorrectionValueEdgeStateChanged

    private void linearCorrectionValueSurfaceStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_linearCorrectionValueSurfaceStateChanged
        Map<Byte, Double> radiusCorrection = parameters.get(LINEAR_RADIUS_CORRECTION);
        radiusCorrection.replace(Hierarchy.SURFACE.getValue(), (double) linearCorrectionValueSurface.getValue());
        parameters.set(LINEAR_RADIUS_CORRECTION, radiusCorrection);
    }//GEN-LAST:event_linearCorrectionValueSurfaceStateChanged

    private void linearCorrectionValueInsideStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_linearCorrectionValueInsideStateChanged
        Map<Byte, Double> radiusCorrection = parameters.get(LINEAR_RADIUS_CORRECTION);
        radiusCorrection.replace(Hierarchy.INSIDE.getValue(), (double) linearCorrectionValueInside.getValue());
        parameters.set(LINEAR_RADIUS_CORRECTION, radiusCorrection);
    }//GEN-LAST:event_linearCorrectionValueInsideStateChanged

    private boolean is3D() {
        Map<Byte, Integer> elementsToLeave = parameters.get(ELEMENTS_TO_LEAVE);
        // disable inside elements' panel for 2D cases; -1 should be set in parameter validation
        return elementsToLeave.get(Hierarchy.INSIDE.getValue()) > 0;
    }

    private void maxIterationsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_maxIterationsStateChanged
        parameters.set(ITERATIONS_LIMIT, (int) maxIterations.getValue());
    }//GEN-LAST:event_maxIterationsStateChanged

    private void setSelected(OutputType outputType) {
        // reset all buttons
        triangulateParallelRegionsButton.setSelected(false);
        pointFieldButton.setSelected(false);
        originalFieldDebugButton.setSelected(false);

        // select right one
        if (outputType == OutputType.ORIGINAL_FIELD_DETAILS) {
            originalFieldDebugButton.setSelected(true);
        }

        if (outputType == OutputType.POINT_FIELD) {
            pointFieldButton.setSelected(true);
        }

        if (outputType == OutputType.TRIANGULATED_IRREGULAR_FIELD_PARALLEL_REGIONS) {
            triangulateParallelRegionsButton.setSelected(true);
        }

        if (outputType == OutputType.TRIANGULATED_IRREGULAR_FIELD_QDELAUNAY) {
            triangulateQDelaunayButton.setSelected(true);
        }

        parameters.set(OUTPUT_TYPE, outputType);
    }

    private void triangulateParallelRegionsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_triangulateParallelRegionsButtonActionPerformed
        setSelected(OutputType.TRIANGULATED_IRREGULAR_FIELD_PARALLEL_REGIONS);
    }//GEN-LAST:event_triangulateParallelRegionsButtonActionPerformed

    private void pointFieldButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pointFieldButtonActionPerformed
        setSelected(OutputType.POINT_FIELD);
    }//GEN-LAST:event_pointFieldButtonActionPerformed

    private void originalFieldDebugButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_originalFieldDebugButtonActionPerformed
        setSelected(OutputType.ORIGINAL_FIELD_DETAILS);
    }//GEN-LAST:event_originalFieldDebugButtonActionPerformed

    private void diffNormWeightCoefficientValueEdgeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_diffNormWeightCoefficientValueEdgeStateChanged
        Map<Byte, Double> epsilonRadiusCorrection = parameters.get(DIFF_NORM_WEIGHT_COEFFICIENT);
        epsilonRadiusCorrection.replace(Hierarchy.EDGE.getValue(), (double) diffNormWeightCoefficientValueEdge.getValue());
        parameters.set(DIFF_NORM_WEIGHT_COEFFICIENT, epsilonRadiusCorrection);
    }//GEN-LAST:event_diffNormWeightCoefficientValueEdgeStateChanged

    private void diffNormWeightCoefficientValueSurfaceStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_diffNormWeightCoefficientValueSurfaceStateChanged
        Map<Byte, Double> epsilonRadiusCorrection = parameters.get(DIFF_NORM_WEIGHT_COEFFICIENT);
        epsilonRadiusCorrection.replace(Hierarchy.SURFACE.getValue(), (double) diffNormWeightCoefficientValueSurface.getValue());
        parameters.set(DIFF_NORM_WEIGHT_COEFFICIENT, epsilonRadiusCorrection);
    }//GEN-LAST:event_diffNormWeightCoefficientValueSurfaceStateChanged

    private void diffNormWeightCoefficientInsideStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_diffNormWeightCoefficientInsideStateChanged
        Map<Byte, Double> epsilonRadiusCorrection = parameters.get(DIFF_NORM_WEIGHT_COEFFICIENT);
        epsilonRadiusCorrection.replace(Hierarchy.INSIDE.getValue(), (double) diffNormWeightCoefficientInside.getValue());
        parameters.set(DIFF_NORM_WEIGHT_COEFFICIENT, epsilonRadiusCorrection);
    }//GEN-LAST:event_diffNormWeightCoefficientInsideStateChanged

    private void triangulateQDelaunayButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_triangulateQDelaunayButtonActionPerformed
        setSelected(OutputType.TRIANGULATED_IRREGULAR_FIELD_QDELAUNAY);
    }//GEN-LAST:event_triangulateQDelaunayButtonActionPerformed

    private void filteringAngleSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_filteringAngleSliderUserChangeAction
        parameters.set(FILTERING_ANGLE, filteringAngleSlider.getValue().doubleValue());
    }//GEN-LAST:event_filteringAngleSliderUserChangeAction

    private void criticalAngleSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_criticalAngleSliderUserChangeAction
        parameters.set(CRITICAL_ANGLE, criticalAngleSlider.getValue().doubleValue());
    }//GEN-LAST:event_criticalAngleSliderUserChangeAction

    private void edgeElementsSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_edgeElementsSliderUserChangeAction
        Map<Byte, Integer> elementsToLeave = parameters.get(ELEMENTS_TO_LEAVE);
        elementsToLeave.replace(Hierarchy.EDGE.getValue(), edgeElementsSlider.getValue().intValue());
        parameters.set(ELEMENTS_TO_LEAVE, elementsToLeave);
    }//GEN-LAST:event_edgeElementsSliderUserChangeAction

    private void surfaceElementsSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_surfaceElementsSliderUserChangeAction
        Map<Byte, Integer> elementsToLeave = parameters.get(ELEMENTS_TO_LEAVE);
        elementsToLeave.replace(Hierarchy.SURFACE.getValue(), surfaceElementsSlider.getValue().intValue());
        parameters.set(ELEMENTS_TO_LEAVE, elementsToLeave);
    }//GEN-LAST:event_surfaceElementsSliderUserChangeAction

    private void insideElementsSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_insideElementsSliderUserChangeAction
        Map<Byte, Integer> elementsToLeave = parameters.get(ELEMENTS_TO_LEAVE);
        elementsToLeave.replace(Hierarchy.INSIDE.getValue(), insideElementsSlider.getValue().intValue());
        parameters.set(ELEMENTS_TO_LEAVE, elementsToLeave);
    }//GEN-LAST:event_insideElementsSliderUserChangeAction


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel automaticDecimationPanel;
    private javax.swing.JPanel componentsSelectionPanel;
    private javax.swing.JPanel computationsProperties;
    protected javax.swing.JPanel correctionPanel;
    private org.visnow.vn.gui.widgets.ExtendedSlider criticalAngleSlider;
    private javax.swing.JLabel dataRadiusFormulaLabel;
    private javax.swing.JSpinner diffNormWeightCoefficientInside;
    private javax.swing.JSpinner diffNormWeightCoefficientValueEdge;
    private javax.swing.JSpinner diffNormWeightCoefficientValueSurface;
    private org.visnow.vn.gui.widgets.ExtendedSlider edgeElementsSlider;
    private javax.swing.Box.Filler filler;
    private org.visnow.vn.gui.widgets.ExtendedSlider filteringAngleSlider;
    private javax.swing.JPanel geometricHierarchyPanel;
    private org.visnow.vn.gui.widgets.ExtendedSlider insideElementsSlider;
    private javax.swing.JLabel jLabelCorrEdge;
    private javax.swing.JLabel jLabelCorrInside;
    private javax.swing.JLabel jLabelCorrSurface;
    private javax.swing.JLabel jLabelCorrectionNames;
    private javax.swing.JLabel jLabelDiffNormWeightCoefficientValues;
    private javax.swing.JLabel jLabelLinearCorrectionValues;
    private javax.swing.JSpinner linearCorrectionValueEdge;
    private javax.swing.JSpinner linearCorrectionValueInside;
    private javax.swing.JSpinner linearCorrectionValueSurface;
    private javax.swing.JSpinner maxIterations;
    private javax.swing.JRadioButton originalFieldDebugButton;
    private javax.swing.JPanel outputTypeSelectionPanel;
    private javax.swing.JRadioButton pointFieldButton;
    private javax.swing.JLabel remainingElementsErrorDescriptionLabel;
    private javax.swing.JPanel remainingElementsErrorPanel;
    private org.visnow.vn.gui.widgets.FloatSlider remainingElementsErrorSlider;
    private org.visnow.vn.gui.widgets.RunButton runButton;
    private javax.swing.JScrollPane scalarComponentsScrollPane;
    private javax.swing.JPanel scalarPanel;
    private org.visnow.vn.gui.widgets.ExtendedSlider surfaceElementsSlider;
    private javax.swing.JRadioButton triangulateParallelRegionsButton;
    private javax.swing.JRadioButton triangulateQDelaunayButton;
    private javax.swing.JScrollPane vectorComponentsScrollPane;
    private javax.swing.JPanel vectorPanel;
    private javax.swing.JLabel vertexHierarchyResult;
    // End of variables declaration//GEN-END:variables

}
