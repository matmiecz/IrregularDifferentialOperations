package org.visnow.vn.DiffOpPlugin;

/**
 * TODO maybe in2D / in3D
 *
 * @author mateuszmieczkowski
 */
public enum Hierarchy {

    VERTEX((byte) 3, "vertex"),
    EDGE((byte) 2, "edge"),
    SURFACE((byte) 1, "surface"),
    INSIDE((byte) 0, "inside"),
    UNSPECIFIED((byte) -1, "unspecified");

    private final byte value;
    private final String name;

    Hierarchy(byte value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * Returns the value of the type.
     *
     * @return value of the type
     */
    public byte getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public static Hierarchy getType(byte value) {
        switch (value) {
            case 0:
                return INSIDE;
            case 1:
                return SURFACE;
            case 2:
                return EDGE;
            case 3:
                return VERTEX;
            default:
                return UNSPECIFIED;
        }
    }

    /**
     * Return hierarchies for decimation for given field's trueNSpace 
     * (omit VERTICES)
     * 
     * @param trueNSpace
     * @return 
     */
    public static byte[] getHierarchyValuesForDecimation(int trueNSpace) {
        byte[] hierarchyTypes = null;
        if (trueNSpace == 2) {
            hierarchyTypes = new byte[]{EDGE.getValue(), SURFACE.getValue()};
        } else {
            hierarchyTypes = new byte[]{EDGE.getValue(), SURFACE.getValue(), INSIDE.getValue()};
        }
        return hierarchyTypes;
    }
    
    public static byte[] getAllValidHierarchyValues(){
        byte[] hierarchyTypes = {INSIDE.getValue(), SURFACE.getValue(), EDGE.getValue(), VERTEX.getValue()};
        return hierarchyTypes;
    }

}
