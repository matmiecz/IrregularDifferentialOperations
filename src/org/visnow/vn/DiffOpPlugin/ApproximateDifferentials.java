package org.visnow.vn.DiffOpPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action;
import static org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action.CALCULATE;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;
import org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException;

/**
 * Set of utilities to compute differentials up to 2nd order for
 * IrregularFields.
 *
 * @author MMieczkowski
 */
public class ApproximateDifferentials {

    // input field
    private final int nNodes;
    private final int trueNSpace;
    private final CellType simplexType;

    private final int[] cellSetNodes;
    private final IrregularField inField;
    private final HashMap<String, Action> toCalculate;
    private final FloatLargeArray coords;

    // calculation of direct neighbours, one-time only operation
    private final NodeNeighbours neighbourCounter;
    private final int[] directNeighbours;
    private final int[] directNeighboursInfo;
    // neighbours of neighbours inculded after recalculations, necessary to reach sufficient amount of nodes to calculate approximated differentials' coefficients;
    private int[][] requiredNeighbours;

    // coords(x,y,z), data (d), weight(w) for each node, parameters of the function CalculateFunctionForNeighbour
    private double[][] x, y, z, d, w;
    private DataArray currentComponent;
    // results, coefficients
    private double[] a, b0, b1, b2, c00, c11, c22, c01, c02, c12;

    // optional - number of direct and required neighbours added as field
    private final int[] nrReqNghbrs;
    private final int[] nrDirNghbrs;
    private boolean finished;

    private final HashMap<String, DataArray> normalizedDiffNorms;
    private DataArray normalized2ndOrdDiffNormField;

    /**
     * Input is IrregularField (CellSet structure) and selection of components
     * to calculate.
     *
     * @param inField
     * @param toCalculate
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.MultipleCellSetsException
     * @throws org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError
     */
    public ApproximateDifferentials(IrregularField inField, HashMap<String, Action> toCalculate) throws MultipleCellSetsException, ComputationError {
        this.toCalculate = toCalculate;
        this.simplexType = VNUtils.getSimplexType(inField);

        //TODO - ONLY 1 CELL SET!
        this.inField = VNUtils.extractSingleCellSetField(inField, simplexType);
        this.nNodes = (int) this.inField.getNNodes();
        this.trueNSpace = this.inField.getTrueNSpace();
        this.coords = this.inField.getCurrentCoords();
        this.cellSetNodes = VNUtils.getNodesForSimplexType(inField, simplexType);

        //assign final variables
        neighbourCounter = new NodeNeighbours(nNodes, cellSetNodes, simplexType);
        neighbourCounter.calculateNodeNeigbours();
        this.directNeighbours = neighbourCounter.getDirectNeighbours();
        this.directNeighboursInfo = neighbourCounter.getDirectNeighboursInfo();

        this.requiredNeighbours = neighbourCounter.getRequiredNeighbours();

        this.nrReqNghbrs = new int[nNodes];
        this.nrDirNghbrs = new int[nNodes];

        this.normalizedDiffNorms = new HashMap<>();
    }

    public double[] getA() {
        return a;
    }

    public double[] getB0() {
        return b0;
    }

    public double[] getB1() {
        return b1;
    }

    public double[] getB2() {
        return b2;
    }

    public double[] getC00() {
        return c00;
    }

    public double[] getC11() {
        return c11;
    }

    public double[] getC22() {
        return c22;
    }

    public double[] getC01() {
        return c01;
    }

    public double[] getC02() {
        return c02;
    }

    public double[] getC12() {
        return c12;
    }

    public CellType getSimplexType() {
        return simplexType;
    }

    public NodeNeighbours getNeighbourCounter() {
        return neighbourCounter;
    }

    public IrregularField getField() {
        return inField;
    }

    public DataArray getNormalized2ndOrdDiffNormForField() {
        return normalized2ndOrdDiffNormField;
    }

    private void allocateMatrices(int nodeNumber) {
        // auxiliary variable necessary for arrays' allocation
        int ln = this.requiredNeighbours[nodeNumber].length + 1; //all the neighbours + processed node itself
        x[nodeNumber] = new double[ln];
        y[nodeNumber] = new double[ln];
        d[nodeNumber] = new double[ln];
        w[nodeNumber] = new double[ln];
        if (this.trueNSpace == 3) {
            z[nodeNumber] = new double[ln];
        }
    }

    //loop over each node in the field:
    //  - fill coords and data for all neigbours of the processed node
    //  - as well as the processed node's itself at the end
    private void fillMatrices(int nodeNumber, DataArray data1D) {
        // length depending on the number of neighbours of the processed node
        int nNeighbours = x[nodeNumber].length - 1;

        double r = 0, rMax = 0;  // rMax will be squared radius of the neighbourhood

        for (int j = 0; j < nNeighbours; j++) {
            int neighbour = this.requiredNeighbours[nodeNumber][j];
            x[nodeNumber][j] = coords.getDouble(neighbour * 3) - coords.getDouble(nodeNumber * 3);
            r += x[nodeNumber][j] * x[nodeNumber][j];
            y[nodeNumber][j] = coords.getDouble(neighbour * 3 + 1) - coords.getDouble(nodeNumber * 3 + 1);
            r += y[nodeNumber][j] * y[nodeNumber][j];
            if (this.trueNSpace == 3) {
                z[nodeNumber][j] = coords.getDouble(neighbour * 3 + 2) - coords.getDouble(nodeNumber * 3 + 2);
                r += z[nodeNumber][j] * z[nodeNumber][j];
            }
            d[nodeNumber][j] = data1D.getDoubleElement(neighbour)[0]; //only for 1D data  //TODO

            w[nodeNumber][j] = r; // temporary - square of 
            if (r > rMax) {
                rMax = r;
            }
        }
        // TODO WEIGHT PARAMETERIZATION
        float DISTANCE_WEIGHT_PARAMETER = 4.0f; // todo range up from 0 -> 100? only positive values
        for (int j = 0; j < nNeighbours; j++) {
            w[nodeNumber][j] = Math.exp(-DISTANCE_WEIGHT_PARAMETER * w[nodeNumber][j] / rMax); // probably greater value (up to 100) put as a parameter?
        }
        //data and coords of processed node itself
        x[nodeNumber][nNeighbours] = 0.0;
        y[nodeNumber][nNeighbours] = 0.0;
        if (this.trueNSpace == 3) {
            z[nodeNumber][nNeighbours] = 0.0;
        }
        d[nodeNumber][nNeighbours] = data1D.getDoubleElement(nodeNumber)[0];
        w[nodeNumber][nNeighbours] = 1.0;
    }

    /**
     * Allocate the arrays, depending on the number of nodes. Additional
     * allocation in case of 3D elements.
     */
    private void prepareData() throws ComputationError {
        //coords
        x = new double[nNodes][];
        y = new double[nNodes][];
        w = new double[nNodes][];

        //data
        d = new double[nNodes][];
        DataArray data1D = this.currentComponent;

        a = new double[nNodes];
        b0 = new double[nNodes];
        b1 = new double[nNodes];
        c00 = new double[nNodes];
        c11 = new double[nNodes];
        c01 = new double[nNodes];

        //additional data for tetras
        if (this.trueNSpace == 3) {
            b2 = new double[nNodes];
            c22 = new double[nNodes];
            c02 = new double[nNodes];
            c12 = new double[nNodes];
            z = new double[nNodes][];
        }
        //check if minimum required neighbour amount is reached (6 for 2D and 10 for 3D), 
        //add subsequent neighbours' layers until the condition is met
        neighbourCounter.preprocessNeighbours(this.trueNSpace);
        this.requiredNeighbours = neighbourCounter.getRequiredNeighbours();
        for (int i = 0; i < nNodes; i++) {
            allocateMatrices(i);
            //filling the coords and data
            fillMatrices(i, data1D);
        }
    }

    private void recalculateNode(int nodeNumber) {
        //1) recalculate neighbours 
        neighbourCounter.addNextNeighboursLayer(nodeNumber);
        //2) allocate once again x,y,w,d matrices and fill them with new values
        allocateMatrices(nodeNumber);
        fillMatrices(nodeNumber, currentComponent);
        //3) calculate differential function
        switch (this.trueNSpace) {
            case 2:
                calculateDifferentialsSingleNode2D(this.x[nodeNumber], this.y[nodeNumber], this.w[nodeNumber], this.d[nodeNumber], nodeNumber);
                break;
            case 3:
                calculateDifferentialsSingleNode3D(this.x[nodeNumber], this.y[nodeNumber], this.z[nodeNumber], this.w[nodeNumber], this.d[nodeNumber], nodeNumber);
                break;
            default:
                throw new IllegalArgumentException("1D cases not supported.");
        }
    }

    /**
     * Calculate nNodes times, assigning values to the array "results"
     * [nNodes][6]; TRIANGLES (2D)
     *
     * Target is to find the coefficients of 2nd order function that would be
     * the best approximation of the values for all nodes in the group (least
     * square error)
     *
     * @param x - x coordinate of all neighbours (length=nNeighbours)
     * @param y - y coordinate of all neighbours (length=nNeighbours)
     * @param w - weight of each neighbour (importancy) (length=nNeighbours)
     * @param d - value of data for each neighbour (length=nNeighbours)
     * @param nodeNumber - index of processed node
     */
    private void calculateDifferentialsSingleNode2D(double[] x, double[] y, double[] w, double[] d, int nodeNumber) {
        //for node number nodeNumber

        //unknowns: coefficients -> a,b0,b1,c00,c01,c11
        //known x,y(position),w(weight),d(data) - for each of nNodes separately
        //objective - allocate arrays A and B to calculate the coefficients.
        double[][] A = new double[6][6];
        double[] B = new double[6];
        double[] result;
        int nNeighbours = x.length;

        for (int n = 0; n < nNeighbours; n++) {
            double[] t = new double[]{1, x[n], y[n], x[n] * x[n], y[n] * y[n], x[n] * y[n]};
            // A array is symmetric, so only diagonal and right upper triangle is computed
            for (int i = 0; i < t.length; i++) {
                for (int j = 0; j < t.length; j++) {
                    // upper right triangle + diagonal
                    if (i <= j) {
                        A[i][j] += w[n] * t[i] * t[j];
                    }
                }
                B[i] += w[n] * d[n] * t[i];
            }
            // mirror symmetric elements from upper right triangle to lower left
            for (int i = 0; i < t.length; i++) {
                for (int j = 0; j < t.length; j++) {
                    // lower left triangle without diagonal
                    if (i > j) {
                        A[i][j] = A[j][i];
                    }
                }
            }
        }

        finished = false;
        while (!finished) {
            try {
                result = ArrayUtils.lsolveLUD(A, B);
                finished = true;

                this.a[nodeNumber] = result[0];
                this.b0[nodeNumber] = result[1];
                this.b1[nodeNumber] = result[2];
                this.c00[nodeNumber] = 2 * result[3];
                this.c11[nodeNumber] = 2 * result[4];
                this.c01[nodeNumber] = result[5];

                this.nrReqNghbrs[nodeNumber] = requiredNeighbours[nodeNumber].length;
                this.nrDirNghbrs[nodeNumber] = neighbourCounter.getDirectNeighboursForNode(nodeNumber).length;
            } catch (IllegalArgumentException e) {
                recalculateNode(nodeNumber);
            }
        }
    }

    /**
     * Calculates nNodes times, assigning values to the matrix "results"
     * [nNodes][10]; TETRAHEDRONS (3D)
     *
     * Target is to find the coefficients of 2nd order function that would be
     * the best approximation of the values for all nodes in the group (least
     * square error)
     *
     * @param x
     * @param y
     * @param z
     * @param w
     * @param d
     * @param nodeNumber
     */
    private void calculateDifferentialsSingleNode3D(double[] x, double[] y, double[] z, double[] w, double[] d, int nodeNumber) {
        //for node number nodeNumber        

        //unknowns: coefficients -> a,b0,b1,b2,c00,c11,c22,c01,c02,c12;
        //known x,y,z(positions),w(weight),d(data) for each of nNodes separately
        //objective - allocate matrices A and B to calculate the coefficients.
        double[][] A = new double[10][10];
        double[] B = new double[10];
        double[] result;
        int nNeighbours = x.length; //number of neighbours

        for (int n = 0; n < nNeighbours; n++) {
            double[] t = new double[]{1, x[n], y[n], z[n], x[n] * x[n], y[n] * y[n], z[n] * z[n], x[n] * y[n], x[n] * z[n], y[n] * z[n]};
            // A array is symmetric, so only diagonal and right upper triangle is computed
            for (int i = 0; i < t.length; i++) {
                for (int j = 0; j < t.length; j++) {
                    // upper right triangle + diagonal
                    if (i <= j) {
                        A[i][j] += w[n] * t[i] * t[j];
                    }
                }
                B[i] += w[n] * d[n] * t[i];
            }
            // mirror symmetric elements from upper right triangle to lower left
            for (int i = 0; i < t.length; i++) {
                for (int j = 0; j < t.length; j++) {
                    // lower left triangle without diagonal
                    if (i > j) {
                        A[i][j] = A[j][i];
                    }
                }
            }
        }

        finished = false;
        while (!finished) {
            try {
                result = ArrayUtils.lsolveLUD(A, B);
                finished = true;

                this.a[nodeNumber] = result[0];
                this.b0[nodeNumber] = result[1];
                this.b1[nodeNumber] = result[2];
                this.b2[nodeNumber] = result[3];
                this.c00[nodeNumber] = 2 * result[4];
                this.c11[nodeNumber] = 2 * result[5];
                this.c22[nodeNumber] = 2 * result[6];
                this.c01[nodeNumber] = result[7];
                this.c02[nodeNumber] = result[8];
                this.c12[nodeNumber] = result[9];

                this.nrReqNghbrs[nodeNumber] = requiredNeighbours[nodeNumber].length;
                this.nrDirNghbrs[nodeNumber] = neighbourCounter.getDirectNeighboursForNode(nodeNumber).length;
            } catch (IllegalArgumentException e) {
                recalculateNode(nodeNumber);
            }
        }
    }

    /**
     * MOST IMPORTANT METHOD - Checks the trueNSpace of the problem and invoke
     * the right method
     */
    private void calculateDifferentialsField() throws ComputationError {
        prepareData();
        // TODO refactor - instead of freeing x, y, z, w, d this function can 
        // return hashmap

        if (this.trueNSpace == 2) {
            for (int i = 0; i < nNodes; i++) {
                calculateDifferentialsSingleNode2D(x[i], y[i], w[i], d[i], i);
            }
        } else {
            for (int i = 0; i < nNodes; i++) {
                calculateDifferentialsSingleNode3D(x[i], y[i], z[i], w[i], d[i], i);
            }
        }
        // free memory for x, y, z, w, d after computations
        x = null;
        y = null;
        z = null;
        w = null;
        d = null;
    }

    public void assignNrDirNeighbours() {
        IrregularField field = this.getField();
        field.addComponent(DataArray.create(this.nrDirNghbrs, 1, "nrDirectNeighbours"));
    }

    public void assignNrReqNeighbours() {
        IrregularField fieldToAssign = this.getField();
        fieldToAssign.addComponent(DataArray.create(this.nrReqNghbrs, 1, "nrRequiredNeighbours"));
    }

    /**
     * Assign differentials components to the field after performed calculations;
     *
     */
    public void assignDifferentials() {
        IrregularField field = this.getField();
        field.addComponent(DataArray.create(this.a, 1, currentComponent.getName() + "_a"));
        field.addComponent(DataArray.create(this.b0, 1, currentComponent.getName() + "_b0"));
        field.addComponent(DataArray.create(this.b1, 1, currentComponent.getName() + "_b1"));
        field.addComponent(DataArray.create(this.c00, 1, currentComponent.getName() + "_c00"));
        field.addComponent(DataArray.create(this.c11, 1, currentComponent.getName() + "_c11"));
        field.addComponent(DataArray.create(this.c01, 1, currentComponent.getName() + "_c01"));

        if (this.trueNSpace == 3) {
            field.addComponent(DataArray.create(this.b2, 1, currentComponent.getName() + "_b2"));
            field.addComponent(DataArray.create(this.c02, 1, currentComponent.getName() + "_c02"));
            field.addComponent(DataArray.create(this.c12, 1, currentComponent.getName() + "_c12"));
            field.addComponent(DataArray.create(this.c22, 1, currentComponent.getName() + "_c22"));
        }
    }

    private double[] calculate2ndOrdDiffNorm() {
        double[] diffNorm = new double[nNodes];
        switch (simplexType) {
            case TRIANGLE:
                for (int i = 0; i < nNodes; i++) {
                    diffNorm[i] = Math.sqrt((c00[i] * c00[i] + c01[i] * c01[i] + c11[i] * c11[i]) / 3);
                }
                break;
            case TETRA:
                for (int i = 0; i < nNodes; i++) {
                    diffNorm[i] = Math.sqrt((c00[i] * c00[i] + c11[i] * c11[i] + c22[i] * c22[i] + c01[i] * c01[i] + c02[i] * c02[i] + c12[i] * c12[i]) / 6);
                }
                break;
        }
        return diffNorm;
    }

    public void computeDifferentialOperators() throws ComputationError {
        if (toCalculate != null) {
            for (Map.Entry<String, Action> entry : toCalculate.entrySet()) {
                if (entry.getValue() == CALCULATE) {
                    currentComponent = this.getField().getComponent(entry.getKey());
                    calculateDifferentialsField();
                    assignDifferentials();
                }
            }
        }
    }

    public void compute2ndOrdDiffNormField(boolean addComponents) throws ComputationError {
        if (toCalculate != null) {
            for (Map.Entry<String, Action> entry : toCalculate.entrySet()) {
                if (entry.getValue() == CALCULATE) {
                    currentComponent = this.getField().getComponent(entry.getKey());
                    calculateDifferentialsField();
                    double[] diffNorm = calculate2ndOrdDiffNorm();
                    DataArray diffNormDA = DataArray.create(diffNorm, 1, currentComponent.getName() + "_2ndOrdDiffNorm");
                    DataArray normalizedDiffNorm;
                    
                    //normalize RMS
//                    normalizedDiffNorm = ArrayUtils.normalizeArrayRMS(diffNormDA);
                    // normalize to max 1 
                    normalizedDiffNorm = ArrayUtils.normalizeArrayMax1(diffNormDA);
                    
                    normalizedDiffNorms.put(currentComponent.getName() + "_2ndOrdDiffNorm", normalizedDiffNorm);
                    if (addComponents) {
                        this.getField().addComponent(normalizedDiffNorm);
                        this.getField().addComponent(diffNormDA);
                    }
                }
            }

        }
        Set<String> keySet = normalizedDiffNorms.keySet();
        int nComponents = keySet.size();
        //assert that all DataArrays have the same length
        long len = -1;
        if (!keySet.isEmpty()) {
            for (String key : keySet) {
                long nE = normalizedDiffNorms.get(key).getNElements();
                //first iteration - initalize
                if (len == -1) {
                    len = nE;
                }
                if (nE != len) {
                    throw new IllegalArgumentException("Input DataArrays do not have the same length");
                }
            }
            double[] normalizedField = new double[(int) len];
            for (long i = 0; i < len; i++) {
                double sum = 0.0;
                for (String key : keySet) {
                    //only for 1D - TODO VECTOR SUPPORT 
                    sum += normalizedDiffNorms.get(key).getDoubleElement(i)[0];
                }
                double average = sum / nComponents;
                normalizedField[(int) i] = average;
            }
            normalized2ndOrdDiffNormField = DataArray.create(normalizedField, 1, "normalized2ndOrdDiffNorm");
            this.getField().addComponent(normalized2ndOrdDiffNormField);
        }
    }

}
