/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin.FieldDecimation;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingUtilities;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action;
import org.visnow.vn.DiffOpPlugin.Hierarchy;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.commands.LinkAddCommand;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.gui.widgets.RunButton.RunState;
import org.visnow.vn.lib.basic.testdata.TestField.TestField;
import org.visnow.vn.lib.basic.testdata.TestField.TestFieldShared;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;
import org.visnow.vn.lib.utils.tests.application.SimpleApplication;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;
import static org.visnow.vn.gui.widgets.RunButton.RunState.NO_RUN;
import org.visnow.vn.lib.basic.filters.ComponentCalculator.ComponentCalculator;
import org.visnow.vn.lib.basic.utilities.FieldStats.FieldStats;
import org.visnow.vn.lib.types.VNIrregularField;

/**
 *
 * Direct tests of app with GUI.
 *
 * TODO list of params
 *
 * @author mateuszmieczkowski
 */
@RunWith(Parameterized.class)
public class FieldDecimationTest {

    //TODO necessary fields
    private static UserMessage currentErrorMessage = null;
    private static final int SLEEP_TIME = 100;
    private static final RunState RUNNING_MESSAGE = NO_RUN;

    private static Class SOURCE_CLASS; // data source class (test field/component calculator)
    private static int N_DIMS; // test data 2d or 3d
    private static int RESOLUTION; // resolution of test data
    private static Action[] SELECTED_SCALAR_ACTIONS; // array of Action objects to perform over scalar components
    private static Action[] SELECTED_VECTOR_ACTIONS; // array of Action objects to perform over vector components
    private static String[] SELECTED_SOURCE_EXPRESSION; // source expression i.e. f=x^3+y^3+z^3 (component calculator)

    private static int[] SELECTED_TEST_FIELD_COMPONENTS; // which fields from "test field" module to select as integer array 
    private static float[][] EXTENTS; //extents of component calculator (alike ComponentCalculatorShared) 
    //extents only for component calculator
    private static Map<Byte, Integer> ELEMENTS_TO_LEAVE;

    // ----------------------
    // accessing the parameters
    private static final String SCALAR_OPERATIONS_REG_DIFFOPS = "Operations for every scalar component";

    //ComponentCalculatorShared
    private static final String COMPONENT_CALCULATOR_EXPRESSION_LINES = "Expression lines";
    private static final String COMPONENT_CALCULATOR_GENERATOR_DIMENSION_LENGTHS = "Generator dimensions";
    private static final String COMPONENT_CALCULATOR_GENERATOR_EXTENTS = "Generator extents";
    private static final String COMPONENT_CALCULATOR_RETAIN = "Retain";
    // change aliases to make it more generic (not prone to source function name changes)
    private static final String COMPONENT_CALCULATOR_INPUT_ALIASES = "Input field component aliases";

    // ----------------------
    // static input parameters
    private static final String[] INPUT_ALIASES = new String[]{
        "f", // function
        "h", // hierarchy
        "n", // normalized2ndOrderDiffNorm
        "a", // average distance to neighbours
        "nm" // nodes mask (after decimation)
    };

    // ----------------------
    /**
     * Create new instance of FieldDecimationTest
     *
     *
     * @param sourceClass either TestField.class or ComponentCalculator.class
     * @param nDims
     * @param resolution
     * @param scalarActions
     * @param vectorActions
     * @param selectedTestFieldComponents if source class is test field pass
     * here selected components (int[]) else if source class is component
     * calculator leave null
     * @param genExpression if source class is component calculator pass here
     * expression in form of String[] else null
     * @param extents
     * @param nEdge
     * @param nSurface
     * @param nInside
     *
     */
    public FieldDecimationTest(Object sourceClass, Object nDims, Object resolution, Object scalarActions, Object vectorActions, Object selectedTestFieldComponents, Object genExpression, Object extents, Object nEdge, Object nSurface, Object nInside) {
        SOURCE_CLASS = (Class) sourceClass;
        N_DIMS = (Integer) nDims;
        RESOLUTION = (Integer) resolution;
        SELECTED_SCALAR_ACTIONS = (Action[]) scalarActions;
        SELECTED_VECTOR_ACTIONS = (Action[]) vectorActions;

        //casting unncecessary ?
        SELECTED_TEST_FIELD_COMPONENTS = (selectedTestFieldComponents == null) ? null : (int[]) selectedTestFieldComponents;
        SELECTED_SOURCE_EXPRESSION = (genExpression == null) ? null : (String[]) genExpression;
        EXTENTS = (extents == null) ? null : (float[][]) extents;

        Map<Byte, Integer> elementsToLeave = new HashMap<>();
        elementsToLeave.put(Hierarchy.EDGE.getValue(), (Integer) nEdge);
        elementsToLeave.put(Hierarchy.SURFACE.getValue(), (Integer) nSurface);
        elementsToLeave.put(Hierarchy.INSIDE.getValue(), (Integer) nInside);
        ELEMENTS_TO_LEAVE = elementsToLeave;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> test_parameters() {
        Object[][] list = new Object[][]{
            // 2D gaussians test field
            // with 10 resolution problems - changed to 30
            //             {TestField.class, 2, 30, new Action[]{Action.CALCULATE}, new Action[]{}, new int[]{0}, null, null, 30, 200, -1}, // 2D gaussians test field
            //            {TestField.class, 2, 30, new Action[]{Action.CALCULATE}, new Action[]{}, new int[]{0}, null, null, 15, 40, -1}, // 2D gaussians test field
            //            {TestField.class, 3, 30, new Action[]{Action.CALCULATE}, new Action[]{}, new int[]{2}, null, null, 45, 400, 2000}, // gaussians is 2 in test field 3D
            // 2D custom function
//            {ComponentCalculator.class, 2, 50, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3"}, new float[][]{{1.0f, 1.0f}, {4.0f, 4.0f}}, 20, 250, -1}, // 3D custom function
                    {ComponentCalculator.class, 3, 25, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3+_z^3"}, new float[][]{{1.0f, 1.0f, 1.0f}, {4.0f, 4.0f, 4.0f}}, 60, 300, 800}, //            {ComponentCalculator.class, 3, 25, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3+_z^3"}, new float[][]{{1.0f, 1.0f, 1.0f}, {4.0f, 4.0f, 4.0f}}, 30, 100, 300}, 
        //            {ComponentCalculator.class, 3, 50, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3+_z^3"}, new float[][]{{1.0f, 1.0f, 1.0f}, {4.0f, 4.0f, 4.0f}}, 30, 100, 300}, //            {ComponentCalculator.class, 3, 80, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3+_z^3"}, new float[][]{{1.0f, 1.0f, 1.0f}, {4.0f, 4.0f, 4.0f}}, 30, 100, 300}, 
        // try 
        //            {ComponentCalculator.class, 3, 25, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3*_y^3*_z^3"}, new float[][]{{1.0f, 1.0f, 1.0f}, {4.0f, 4.0f, 4.0f}}, 30, 400, 1500},
        //--------------------------------------------------------
        // ERRORS BELOW
        //--------------------------------------------------------
        // NOT CONVERGING
        //                    {TestField.class, 3, 30, new Action[]{Action.CALCULATE}, new Action[]{}, new int[]{0}, null, null, 65, 700, 5000}, // trig function stucks; trig function is 1 in test field 3D
        };
        return Arrays.asList(list);

    }

    /**
     * Avoid repeating code - create links between the modules in the same
     * manner.
     *
     */
    private void createLink(Application application, String moduleNameFrom, String portFrom, String moduleNameTo, String portTo) throws InterruptedException {
        try {
            // connect output of diffops to viewer
            application.getReceiver().receive(new LinkAddCommand(new LinkName(moduleNameFrom, portFrom, moduleNameTo, portTo), true));
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
        } catch (InterruptedException ex) {
            Thread.sleep(10000);
            application.getReceiver().receive(new LinkAddCommand(new LinkName(moduleNameFrom, portFrom, moduleNameTo, portTo), true));
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            ex.printStackTrace();
        }
    }

    /**
     * Test of onActive method, of class FieldDecimation.
     *
     * @throws java.lang.InterruptedException
     * @throws java.lang.reflect.InvocationTargetException
     */
    @Test
    public void testFieldDecimation() throws InterruptedException, InvocationTargetException {
        try {
            try {
                VisNow.mainBlocking(new String[]{}, true);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
            VisNow.initLogging(true);
            VisNow.get().addUserMessageListener((UserMessage message) -> {
                if (message.getLevel() == Level.ERROR) {
                    currentErrorMessage = message;
                }
            });
            Application application = new Application("TestFieldDecimation", true);
            VisNow.get().getMainWindow().getApplicationsPanel().addApplication(application);
            /*
            adding 3 modules:
            1) source - TestField OR ComponentCalculator
            2) field decimation class
            3) Component Calculator - calculate mask for decimated nodes
            4) Field statistics
            5) Viewer
            
            Connections:
            1->2->3->4->5
             */

            // ---------------------------------------------------------------------
            //1 
            String sourceModuleVNName = SimpleApplication.addModule(application, SOURCE_CLASS.getName(), 10);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            //rename to avoid conficts with other component calculator
            String sourceModuleNewName = "sourceModule";
            ModuleBox sourceModule = application.getEngine().getModule(sourceModuleVNName);

            application.getEngine().getExecutor().renameModule(sourceModuleVNName, sourceModuleNewName);

            //2
            String fieldDecimationModuleVNName = SimpleApplication.addModule(application, FieldDecimation.class.getName(), 60);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            // 3) Component calculator - multiply field and mask
            String componentCalculatorModuleVNName = SimpleApplication.addModule(application, ComponentCalculator.class.getName(), 110);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            // 4 Field Statistics
            String fieldStatisticsModuleVNName = SimpleApplication.addModule(application, FieldStats.class.getName(), 160);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            //5
            String viewerModuleVNName = SimpleApplication.addModule(application, Viewer3D.class.getName(), 210);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            // ---------------------------------------------------------------------
            // 1) GENERATE SOURCE DATA
            // ##########################
            final org.visnow.vn.engine.core.Parameters paramsSource = application.getEngine().getModule(sourceModuleNewName).getCore().getParameters();

            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (SOURCE_CLASS == TestField.class) {
                            paramsSource.setValue(TestFieldShared.NUMBER_OF_DIMENSIONS.getName(), N_DIMS);
                            paramsSource.setValue(TestFieldShared.DIMENSION_LENGTH.getName(), RESOLUTION);
                            paramsSource.setValue(TestFieldShared.SELECTED_COMPONENTS.getName(), SELECTED_TEST_FIELD_COMPONENTS);
                        } else if (SOURCE_CLASS == ComponentCalculator.class) {
                            // set dimensions - must be integer array 
                            int[] dimLengths = new int[N_DIMS];
                            for (int i = 0; i < N_DIMS; i++) {
                                dimLengths[i] = RESOLUTION;
                            }
                            paramsSource.set(new ParameterName(COMPONENT_CALCULATOR_GENERATOR_DIMENSION_LENGTHS), dimLengths);
                            paramsSource.set(new ParameterName(COMPONENT_CALCULATOR_GENERATOR_EXTENTS), EXTENTS);
                            paramsSource.set(new ParameterName(COMPONENT_CALCULATOR_EXPRESSION_LINES), SELECTED_SOURCE_EXPRESSION);
                            paramsSource.set(DiffOpPluginShared.RUNNING_MESSAGE, RunButton.RunState.RUN_DYNAMICALLY);
                        } else {
                            fail("Unsupported source class");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        fail("AD: " + ex.toString());
                    }
                }
            });

            application.getEngine().getModule(sourceModuleNewName).getCore().startAction();

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 5);

            currentErrorMessage = null;
            paramsSource.fireStateChanged();
            Thread.sleep(SLEEP_TIME);

            // 2) COMPUTE FIELD DECIMATION
            // ######################################################
            // output from sourceModule is outRegularField
            String sourceFieldName = "outRegularField";
            createLink(application, sourceModuleNewName, sourceFieldName, fieldDecimationModuleVNName, "inField");

            final org.visnow.vn.engine.core.Parameters paramsFieldDecimation = application.getEngine().getModule(fieldDecimationModuleVNName).getCore().getParameters();
            application.getEngine().getModule(fieldDecimationModuleVNName).getCore().increaseFromVNA();
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    try {
                        paramsFieldDecimation.setValue(DiffOpPluginShared.SCALAR_ACTIONS.getName(), SELECTED_SCALAR_ACTIONS);
                        paramsFieldDecimation.setValue(DiffOpPluginShared.VECTOR_ACTIONS.getName(), SELECTED_VECTOR_ACTIONS);

                        paramsFieldDecimation.setValue(DiffOpPluginShared.ELEMENTS_TO_LEAVE.getName(), ELEMENTS_TO_LEAVE);

                        paramsFieldDecimation.set(DiffOpPluginShared.RUNNING_MESSAGE, RunButton.RunState.RUN_ONCE);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        fail("AD: " + ex.toString());
                    }
                }
            });

            application.getEngine().getModule(fieldDecimationModuleVNName).getCore().startAction();

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 5);

            currentErrorMessage = null;
            paramsSource.fireStateChanged();
            Thread.sleep(SLEEP_TIME);

            // 3) Component calculator - calculate difference between regular and irregular diff ops
            // ##########################################################################################################
            createLink(application, fieldDecimationModuleVNName, "outField", componentCalculatorModuleVNName, "inField");

            final org.visnow.vn.engine.core.Parameters paramsComponentCalculator = application.getEngine().getModule(componentCalculatorModuleVNName).getCore().getParameters();
            application.getEngine().getModule(componentCalculatorModuleVNName).getCore().startAction();

            String[] expressions = new String[]{
                "decimated=f*nm",
                "decimated_h=h*nm"
            }; // nm - nodes mask ( 1 - selected, 0 - dismissed)
            for (int i = 0; i < 2; i++) {

                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            // setup parameters in Component Calculator
                            paramsComponentCalculator.setValue(COMPONENT_CALCULATOR_INPUT_ALIASES, INPUT_ALIASES);
                            paramsComponentCalculator.setValue(COMPONENT_CALCULATOR_EXPRESSION_LINES, expressions);
                            paramsComponentCalculator.setValue(COMPONENT_CALCULATOR_RETAIN, true);

                            paramsComponentCalculator.set(DiffOpPluginShared.RUNNING_MESSAGE, RunButton.RunState.RUN_DYNAMICALLY);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            fail("AD: " + ex.toString());
                        }
                    }
                });
                application.getEngine().getModule(componentCalculatorModuleVNName).getCore().startAction();

                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                Thread.sleep(SLEEP_TIME * 5);

                currentErrorMessage = null;
                paramsSource.fireStateChanged();
                Thread.sleep(SLEEP_TIME);
            }

            // 4) Optional field statistics
            createLink(application, componentCalculatorModuleVNName, "outIrregularField", fieldStatisticsModuleVNName, "inField");

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 5);

            currentErrorMessage = null;
//            paramsSource.fireStateChanged();
            Thread.sleep(SLEEP_TIME);

            // 5) Connect to viewer
            // ######################
            createLink(application, componentCalculatorModuleVNName, "outObj", viewerModuleVNName, "inObject");

            // ---------------------------------------------------------------------
            // ---------------------------------------------------------------------
            VNIrregularField outputDiffOps = (VNIrregularField) application.getEngine().getModule(fieldDecimationModuleVNName).getCore().getOutputs().getOutput("outField").getData().getValue();
            String error = "";
            if (outputDiffOps == null) {
                error = "no_output_";
            }
            VisNow.get().getMainWindow().getApplicationsPanel().removeApplication(application, true);
            VisNow.get().getMainWindow().dispose();
            if (!error.equalsIgnoreCase("") || currentErrorMessage != null) {
                fail(error + currentErrorMessage);
            }
        } catch (InterruptedException | InvocationTargetException ex) {
            String error = "exception_";
            ex.printStackTrace();
            fail(error + ex.toString());
        }

    }
}
