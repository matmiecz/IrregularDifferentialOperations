/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.Field;

/**
 *
 * @author mateuszmieczkowski
 */
public class DecimateFieldTest {

    RegularField regularTestField = null;
    DecimateField decimator = null;

    static Field generatedTestField = null;

    public DecimateFieldTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        String fieldType1 = "gaussians";
        String fieldType2 = "component_calculator_output";
//        String fieldType2 = "gaussias"; // test if module works without graphics

        String fieldType = fieldType2;
        TestData testData = new TestData(fieldType);

        generatedTestField = testData.getField();

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        // prepare data:
        // 1) read test data
        // 2) compute differentials - instance of ApproximateDifferentials
        // 3) calculate hierarchy in field - instance of CalculateHierarchy

        // return instance of DecimateField
        regularTestField = (RegularField) generatedTestField;

        HashMap<String, DiffOpPluginShared.Action> toCalculate = new HashMap<>();

        String[] scalarComponents = regularTestField.getScalarComponentNames();

        for (String component : scalarComponents) {
            toCalculate.put(component, DiffOpPluginShared.Action.CALCULATE);
        }

        IrregularField triangulatedTestField = regularTestField.getTriangulated();

        ApproximateDifferentials differentialsApproximator = new ApproximateDifferentials(triangulatedTestField, toCalculate);
        differentialsApproximator.computeDifferentialOperators();
        differentialsApproximator.compute2ndOrdDiffNormField(true);

        float filteringAngle = 10.f;
        float criticalAngle = -1.0f;
        CalculateHierarchy hierarchy = new CalculateHierarchy(triangulatedTestField);
        hierarchy.calculateHierarchyOfNodes(filteringAngle, criticalAngle);

        this.decimator = new DecimateField(differentialsApproximator, hierarchy);

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testClass() {
        boolean isinstance = this.regularTestField instanceof RegularField;
        assertTrue(isinstance);
    }

    @Test
    public void testFindMaxRadius() {
        // test 1 - finding radius 
        double radius = decimator.findFieldSpan();

        double targetRadius = 4.0;
        assertEquals(radius, targetRadius, 0.000001);

    }

    @Test
    public void testComputeRadiusForField() {
        // test 3 - compute radius for field 
        double decimationCoefficientEdge = 0.2;
        double decimationCoefficientSurface = 0.2;
        double decimationCoefficientInside = 0.2;

        try {
            System.out.println("Not implemented");
//            decimator.computeRadiusForField(decimationCoefficientEdge, decimationCoefficientSurface, decimationCoefficientInside);
        } catch (Exception ex) {
            Logger.getLogger(DecimateFieldTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
