/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin.DifferentialsCalculator;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.SwingUtilities;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared;
import org.visnow.vn.DiffOpPlugin.DiffOpPluginShared.Action;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.commands.LinkAddCommand;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.gui.widgets.RunButton.RunState;
import org.visnow.vn.lib.basic.testdata.TestField.TestField;
import org.visnow.vn.lib.basic.testdata.TestField.TestFieldShared;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;
import org.visnow.vn.lib.utils.tests.application.SimpleApplication;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;
import org.visnow.vn.system.utils.usermessage.UserMessageListener;

import static org.visnow.vn.gui.widgets.RunButton.RunState.NO_RUN;
import org.visnow.vn.lib.basic.filters.ComponentCalculator.ComponentCalculator;
import org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperations;
import org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperations;
import org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.ScalarOperation;
import org.visnow.vn.lib.basic.filters.FieldCombiner.FieldCombiner;
import org.visnow.vn.lib.basic.utilities.FieldStats.FieldStats;
import org.visnow.vn.lib.types.VNIrregularField;

/**
 *
 * Direct tests of app with GUI.
 *
 * TODO list of params
 *
 * @author mateuszmieczkowski
 */
@RunWith(Parameterized.class) // TODO what is this?
public class DifferentialsCalculatorTest {

    //TODO necessary fields
    private static UserMessage currentErrorMessage = null;
    private static final int SLEEP_TIME = 100;
    private static final RunState RUNNING_MESSAGE = NO_RUN;

    private static final ScalarOperation[] REGULAR_DIFFOPS_SCALAR_OPERATIONS = new ScalarOperation[]{ScalarOperation.GRADIENT_COMPONENTS, ScalarOperation.HESSIAN};

    private static Class SOURCE_CLASS; // data source class (test field/component calculator)
    private static int N_DIMS; // test data 2d or 3d
    private static int RESOLUTION; // resolution of test data
    private static Action[] SELECTED_SCALAR_ACTIONS; // array of Action objects to perform over scalar components
    private static Action[] SELECTED_VECTOR_ACTIONS; // array of Action objects to perform over vector components
    private static int[] SELECTED_TEST_FIELD_COMPONENTS; // which fields from "test field" module to select as integer array 
    private static String[] SELECTED_SOURCE_EXPRESSION; // source expression i.e. f=x^3+y^3+z^3 (component calculator)
    private static float[][] EXTENTS; //extents of component calculator (alike ComponentCalculatorShared) 
    //extents only for component calculator

    private static ScalarOperation[][] REGULAR_DIFFOPS_SCALAR_OPERATIONS_PARAM;

    // ----------------------
    // accessing the parameters
    private static final String SCALAR_OPERATIONS_REG_DIFFOPS = "Operations for every scalar component";

    //ComponentCalculatorShared
    private static final String COMPONENT_CALCULATOR_EXPRESSION_LINES = "Expression lines";
    private static final String COMPONENT_CALCULATOR_GENERATOR_DIMENSION_LENGTHS = "Generator dimensions";
    private static final String COMPONENT_CALCULATOR_GENERATOR_EXTENTS = "Generator extents";
    private static final String COMPONENT_CALCULATOR_RETAIN = "Retain";
    // change aliases to make it more generic (not prone to source function name changes)
    private static final String COMPONENT_CALCULATOR_INPUT_ALIASES = "Input field component aliases";

    //ComponentOperationsShared
    private static final String COMPONENT_OPERATIONS_VCSPLIT = "VCSplit"; // split hessian

    // ----------------------
    // static input parameters
    private static final String[] INPUT_ALIASES_2D = new String[]{
        "f",
        "fa",
        "f_d_dx_reg",
        "f_d_dy_reg",
        "f_d2_dx2_reg",
        "f_d2_dy2_reg",
        "f_d2_dxdy_reg",
        "n", // directNeighbours
        "nr", // requiredNeighbours
        "f_irr", // same as f
        "f_d_dx_irr",
        "f_d_dy_irr",
        "f_hess3d", // hessian vector (TODO drop)
        "f_d2_dx2_irr",
        "f_d2_dxdy_irr",
        "f_d2_dy2_irr",};

    private static final String[] INPUT_ALIASES_3D = new String[]{
        "f",
        "fa",
        "f_d_dx_reg",
        "f_d_dy_reg",
        "f_d_dz_reg",
        "f_d2_dx2_reg",
        "f_d2_dy2_reg",
        "f_d2_dz2_reg",
        "f_d2_dxdy_reg",
        "f_d2_dxdz_reg",
        "f_d2_dydz_reg",
        "n", // directNeighbours
        "nr", // requiredNeighbours
        "f_irr", // same as f
        "f_d_dx_irr",
        "f_d_dy_irr",
        "f_d_dz_irr",
        "f_hess3d", // hessian vector (TODO drop)
        "f_d2_dx2_irr",
        "f_d2_dxdy_irr",
        "f_d2_dxdz_irr",
        "f_d2_dy2_irr",
        "f_d2_dydz_irr",
        "f_d2_dz2_irr",};

    private static String[] EXPRESSIONS_2D = new String[]{
        // ORIGINAL 
        //        "diff_d_dx=gb-gdd", // gb - irr; gdd - reg;
        //        "diff_d_dy=gs-gddd",
        //        "diff_d2/dx2=gc-ghd",
        //        "diff_d2/dy2=gss-gssns",
        //        "diff_d2/dxdy=gssn-ghdd",};
        "diff_d_dx=f_d_dx_irr-f_d_dx_reg",
        "diff_d_dy=f_d_dy_irr-f_d_dy_reg",
        "diff_d2_dx2=f_d2_dx2_irr-f_d2_dx2_reg",
        "diff_d2_dy2=f_d2_dy2_irr-f_d2_dy2_reg",
        "diff_d2_dxdy=f_d2_dxdy_irr-f_d2_dxdy_reg",
        // relative errors [%]
        "err_d_dx=((f_d_dx_irr-f_d_dx_reg)/f_d_dx_reg)*100",
        "err_d_dy=((f_d_dy_irr-f_d_dy_reg)/f_d_dy_reg)*100",
        "err_d2_dx2=((f_d2_dx2_irr-f_d2_dx2_reg)/f_d2_dx2_reg)*100",
        "err_d2_dy2=((f_d2_dy2_irr-f_d2_dy2_reg)/f_d2_dy2_reg)*100",
        "err_d2_dxdy=((f_d2_dxdy_irr-f_d2_dxdy_reg)/f_d2_dxdy_reg)*100"
    };

    private static String[] EXPRESSIONS_3D = new String[]{
        "diff_d_dx=f_d_dx_irr-f_d_dx_reg",
        "diff_d_dy=f_d_dy_irr-f_d_dy_reg",
        "diff_d_dz=f_d_dz_irr-f_d_dz_reg",
        "diff_d2_dx2=f_d2_dx2_irr-f_d2_dx2_reg",
        "diff_d2_dy2=f_d2_dy2_irr-f_d2_dy2_reg",
        "diff_d2_dz2=f_d2_dz2_irr-f_d2_dz2_reg",
        "diff_d2_dxdy=f_d2_dxdy_irr-f_d2_dxdy_reg",
        "diff_d2_dxdz=f_d2_dxdz_irr-f_d2_dxdz_reg",
        "diff_d2_dydz=f_d2_dydz_irr-f_d2_dydz_reg",
        // relative errors [%]
        "err_d_dx=(f_d_dx_irr-f_d_dx_reg)/f_d_dx_reg*100",
        "err_d_dy=(f_d_dy_irr-f_d_dy_reg)/f_d_dy_reg*100",
        "err_d_dz=(f_d_dz_irr-f_d_dz_reg)/f_d_dz_reg*100",
        "err_d2_dx2=(f_d2_dx2_irr-f_d2_dx2_reg)/f_d2_dx2_reg*100",
        "err_d2_dy2=(f_d2_dy2_irr-f_d2_dy2_reg)/f_d2_dy2_reg*100",
        "err_d2_dz2=(f_d2_dz2_irr-f_d2_dz2_reg)/f_d2_dz2_reg*100",
        "err_d2_dxdy=(f_d2_dxdy_irr-f_d2_dxdy_reg)/f_d2_dxdy_reg*100",
        "err_d2_dxdz=(f_d2_dxdz_irr-f_d2_dxdz_reg)/f_d2_dxdz_reg*100",
        "err_d2_dydz=(f_d2_dydz_irr-f_d2_dydz_reg)/f_d2_dydz_reg*100",};

    /**
     * Create new instance of DifferentialsCalculatorTest
     *
     *
     * @param sourceClass either TestField.class or ComponentCalculator.class
     * @param nDims
     * @param resolution
     * @param scalarActions
     * @param vectorActions
     * @param selectedTestFieldComponents if source class is test field pass
     * here selected components (int[]) else if source class is component
     * calculator leave null
     * @param genExpression if source class is component calculator pass here
     * expression in form of String[] else null
     * @param extents
     *
     */
    public DifferentialsCalculatorTest(Object sourceClass, Object nDims, Object resolution, Object scalarActions, Object vectorActions, Object selectedTestFieldComponents, Object genExpression, Object extents) {
        SOURCE_CLASS = (Class) sourceClass;
        N_DIMS = (Integer) nDims;
        RESOLUTION = (Integer) resolution;
        SELECTED_SCALAR_ACTIONS = (Action[]) scalarActions;
        SELECTED_VECTOR_ACTIONS = (Action[]) vectorActions;
        //casting unncecessary ?
        SELECTED_TEST_FIELD_COMPONENTS = (selectedTestFieldComponents == null) ? null : (int[]) selectedTestFieldComponents;
        SELECTED_SOURCE_EXPRESSION = (genExpression == null) ? null : (String[]) genExpression;
        EXTENTS = (extents == null) ? null : (float[][]) extents;

        // compute only gradient_components and hessian from scalar
        // TODO hardcoded shape
        REGULAR_DIFFOPS_SCALAR_OPERATIONS_PARAM = new ScalarOperation[][]{REGULAR_DIFFOPS_SCALAR_OPERATIONS};
    }

    @Parameterized.Parameters
    public static Collection<Object[]> test_parameters() {
        Object[][] list = new Object[][]{
            // 2D gaussians test field
            // with 10 resolution problems - changed to 30
            //            {TestField.class, 2, 30, new Action[]{Action.CALCULATE}, new Action[]{}, new int[]{0}, null, null},

            // 3D gaussians test field
            //            {TestField.class, 3, 30, new Action[]{Action.CALCULATE}, new Action[]{}, new int[]{2}, null, null}, // gaussians is 2 in test field 3D
            // 2D custom function
            //            {ComponentCalculator.class, 2, 50, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3"}, new float[][]{{1.0f, 1.0f}, {4.0f, 4.0f}}}, 
            // 3D custom function
            {ComponentCalculator.class, 3, 25, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3+_z^3"}, new float[][]{{1.0f, 1.0f, 1.0f}, {4.0f, 4.0f, 4.0f}}},
            // same problem 
            {ComponentCalculator.class, 3, 25, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^4+_y^4+_z^4"}, new float[][]{{1.0f, 1.0f, 1.0f}, {4.0f, 4.0f, 4.0f}}},
            // try 
            {ComponentCalculator.class, 3, 25, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3*_y^3*_z^3"}, new float[][]{{1.0f, 1.0f, 1.0f}, {4.0f, 4.0f, 4.0f}}},};
        //--------------------------------------------------------
        // ERRORS
        //            {2, 10, new Action[]{}, new Action[]{Action.CALCULATE}, new int[]{1}}, // TODO? vector - trig function
        //            {ComponentCalculator.class, 2, 20, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3"}, new float[][]{{1.0f, 4.0f}, {1.0f, 4.0f}}}, // TODO StackOverFlowError
        //            {ComponentCalculator.class, 3, 10, new Action[]{Action.CALCULATE}, new Action[]{}, null, new String[]{"f=_x^3+_y^3+_z^3"}, new float[][]{{1.0f, 4.0f}, {1.0f, 4.0f}, {1.0f, 4.0f}}},};
        return Arrays.asList(list);

    }

    /**
     * Avoid repeating code - create links between the modules in the same
     * manner.
     *
     */
    private void createLink(Application application, String moduleNameFrom, String portFrom, String moduleNameTo, String portTo) throws InterruptedException {
        try {
            // connect output of diffops to viewer
            application.getReceiver().receive(new LinkAddCommand(new LinkName(moduleNameFrom, portFrom, moduleNameTo, portTo), true));
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
        } catch (Throwable ex) {
            Thread.sleep(10000);
            application.getReceiver().receive(new LinkAddCommand(new LinkName(moduleNameFrom, portFrom, moduleNameTo, portTo), true));
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            ex.printStackTrace();
        }
    }

    /**
     * Test of onActive method, of class DifferentialsCalculator.
     *
     * @throws java.lang.InterruptedException
     * @throws java.lang.reflect.InvocationTargetException
     */
    @Test
    public void testDiffOps() throws InterruptedException, InvocationTargetException {
        try {
            try {
                VisNow.mainBlocking(new String[]{}, true);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
            VisNow.initLogging(true);
            VisNow.get().addUserMessageListener(new UserMessageListener() {
                @Override
                public void newMessage(UserMessage message) {
                    if (message.getLevel() == Level.ERROR) {
                        currentErrorMessage = message;
                    }
                }
            });
            Application application = new Application("TestIrregularDifferentialOperations", true);
            VisNow.get().getMainWindow().getApplicationsPanel().addApplication(application);
            /*
            adding 7 modules:
            1) source - TestField OR ComponentCalculator
            
            2) irregular differential operations
            
            3) regular differential operations
            4) <module name="modify components/coordinates" class="ComponentOperations"> 
            - split hessian
            - calculate difference between regular and irregular
            
            5) Combine Field Components (combine into one output)
            <module name="combine field components" class="FieldCombiner">
            
            6) Component Calculator - calculate difference between regular and irregular
            
            7) Viewer
            
            8) Field Statistics
            
            Connections:
            1->2----\      /->7
                     5->6-
            1->3->4-/      \->8
             */

            // ---------------------------------------------------------------------
            //1 
            String sourceModuleVNName = SimpleApplication.addModule(application, SOURCE_CLASS.getName(), 10);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            //rename to avoid conficts with other component calculator
            String sourceModuleNewName = "sourceModule";
            ModuleBox sourceModule = application.getEngine().getModule(sourceModuleVNName);

            application.getEngine().getExecutor().renameModule(sourceModuleVNName, sourceModuleNewName);

            //2
            String irregularDiffOpsModuleVNName = SimpleApplication.addModule(application, DifferentialsCalculator.class.getName(), 60);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            //3
            String regularDiffOpsModuleVNName = SimpleApplication.addModule(application, DifferentialOperations.class.getName(), 110);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            //4
            String componentOperationsModuleVNName = SimpleApplication.addModule(application, ComponentOperations.class.getName(), 160);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            //5 
            String combineFieldsModuleVNName = SimpleApplication.addModule(application, FieldCombiner.class.getName(), 210);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            //6 - added below to avoid conflicts with component calculator source module
            //7
            String viewerModuleVNName = SimpleApplication.addModule(application, Viewer3D.class.getName(), 360);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            // 8 Field Statistics
            String fieldStatisticsModuleVNName = SimpleApplication.addModule(application, FieldStats.class.getName(), 310);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            // ---------------------------------------------------------------------
            // 1) GENERATE SOURCE DATA
            // ##########################
            final org.visnow.vn.engine.core.Parameters paramsSource = application.getEngine().getModule(sourceModuleNewName).getCore().getParameters();

            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (SOURCE_CLASS == TestField.class) {
                            paramsSource.setValue(TestFieldShared.NUMBER_OF_DIMENSIONS.getName(), N_DIMS);
                            paramsSource.setValue(TestFieldShared.DIMENSION_LENGTH.getName(), RESOLUTION);
                            paramsSource.setValue(TestFieldShared.SELECTED_COMPONENTS.getName(), SELECTED_TEST_FIELD_COMPONENTS);
                        } else if (SOURCE_CLASS == ComponentCalculator.class) {
                            // set dimensions - must be integer array 
                            int[] dimLengths = new int[N_DIMS];
                            for (int i = 0; i < N_DIMS; i++) {
                                dimLengths[i] = RESOLUTION;
                            }

//                            paramsSource.setValue(COMPONENT_CALCULATOR_GENERATOR_DIMENSION_LENGTHS, dimLengths);
//                            paramsSource.setValue(COMPONENT_CALCULATOR_GENERATOR_EXTENTS, EXTENTS);
//                            paramsSource.setValue(COMPONENT_CALCULATOR_EXPRESSION_LINES, SELECTED_SOURCE_EXPRESSION);
                            paramsSource.set(new ParameterName(COMPONENT_CALCULATOR_GENERATOR_DIMENSION_LENGTHS), dimLengths);
                            paramsSource.set(new ParameterName(COMPONENT_CALCULATOR_GENERATOR_EXTENTS), EXTENTS);
                            paramsSource.set(new ParameterName(COMPONENT_CALCULATOR_EXPRESSION_LINES), SELECTED_SOURCE_EXPRESSION);
                            paramsSource.set(DiffOpPluginShared.RUNNING_MESSAGE, RunButton.RunState.RUN_DYNAMICALLY);
                        } else {
                            fail("Unsupported source class");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        fail("AD: " + ex.toString());
                    }
                }
            });

            application.getEngine().getModule(sourceModuleNewName).getCore().startAction();

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 5);

            currentErrorMessage = null;
            paramsSource.fireStateChanged();
            Thread.sleep(SLEEP_TIME);

            // 2) COMPUTE DIFFERENTIAL OPERATIONS - IRREGULAR MODULE
            // ######################################################
            //source 
            //application.getEngine().getModule(testModuleVNName).getOutputs()
            // output can be outIrregularField or outRegularField (in this case hardcoded regular)
            String sourceFieldName = "outRegularField";
            createLink(application, sourceModuleNewName, sourceFieldName, irregularDiffOpsModuleVNName, "inField");

            final org.visnow.vn.engine.core.Parameters paramsIrregularDiffOps = application.getEngine().getModule(irregularDiffOpsModuleVNName).getCore().getParameters();
            application.getEngine().getModule(irregularDiffOpsModuleVNName).getCore().increaseFromVNA();
            // run two times to set parameters - first resets parameters (TODO - newInField)
            for (int i = 0; i < 2; i++) {

                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                        paramsIrregularDiffOps.set(new ParameterName(DiffOpPluginShared.SCALAR_ACTIONS.getName()), SCALAR_ACTIONS);
                            // dont know why sometimes params are not set 
                            paramsIrregularDiffOps.setValue(DiffOpPluginShared.SCALAR_ACTIONS.getName(), SELECTED_SCALAR_ACTIONS);
                            paramsIrregularDiffOps.setValue(DiffOpPluginShared.VECTOR_ACTIONS.getName(), SELECTED_VECTOR_ACTIONS);
                            paramsIrregularDiffOps.set(DiffOpPluginShared.RUNNING_MESSAGE, RunButton.RunState.RUN_DYNAMICALLY);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            fail("AD: " + ex.toString());
                        }
                    }
                });
            }

            application.getEngine().getModule(irregularDiffOpsModuleVNName).getCore().startAction();

//            application.getEngine().getModule(irregularDiffOpsModuleVNName).getCore();
//            ModuleCore irregularDiffOpsModuleCore = application.getEngine().getModule(irregularDiffOpsModuleVNName).getCore();
//
//            DifferentialsCalculator irregularDiffOpsModule = (DifferentialsCalculator) irregularDiffOpsModuleCore;
//            GUI gui = irregularDiffOpsModule.getGUI();
//            FieldVisualizationGUI ui = irregularDiffOpsModule.getUI();
            // here modify PresentationParams
            // i.e. select different field to present
            //from PresentationGUI class
//            PresentationParams new_params = new PresentationParams();// ui.getPresentationGUI().params;
//            
//            ui.getPresentationGUI().setPresentationParams(new_params);
//            gui.setParameters(paramsSource);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 5);

            currentErrorMessage = null;
            paramsSource.fireStateChanged();
            Thread.sleep(SLEEP_TIME);

            // 3) COMPUTE DIFFERENTIAL OPERATIONS - REGULAR MODULE
            // #####################################################
            createLink(application, sourceModuleNewName, sourceFieldName, regularDiffOpsModuleVNName, "inField");
            final org.visnow.vn.engine.core.Parameters paramsRegularDiffOps = application.getEngine().getModule(regularDiffOpsModuleVNName).getCore().getParameters();
            //run it twice because with the first run parameters are not set - resetParameters == true; ??
            // and no matter incoming parameters - all are set to null
            for (int i = 0; i < 2; i++) {
                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            // FIXME - IS THIS ONLY WAY TO SET VALUE WHEN SCALAR_OPERATIONS not accessible?? (DifferentialOperationsShared)
                            paramsRegularDiffOps.setValue(SCALAR_OPERATIONS_REG_DIFFOPS, new ScalarOperation[][]{{ScalarOperation.GRADIENT_COMPONENTS, ScalarOperation.HESSIAN}});

                            paramsRegularDiffOps.set(new ParameterName(SCALAR_OPERATIONS_REG_DIFFOPS), new ScalarOperation[][]{{ScalarOperation.GRADIENT_COMPONENTS, ScalarOperation.HESSIAN}});
//                        paramsRegularDiffOps.set(SCALAR_OPERATIONS_REG_DIFFOPS, new Object[][]{{ScalarOperation.GRADIENT_COMPONENTS, ScalarOperation.HESSIAN}});
                            // omit vector actions
//                        paramsRregularDiffOps.setValue("Operations for every vector component", VECTOR_ACTIONS);
                            paramsRegularDiffOps.set(DiffOpPluginShared.RUNNING_MESSAGE, RunButton.RunState.RUN_DYNAMICALLY);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            fail("AD: " + ex.toString());
                        }
                    }
                });

                application.getEngine().getModule(regularDiffOpsModuleVNName).getCore().startAction();
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                Thread.sleep(SLEEP_TIME * 5);

                currentErrorMessage = null;
                paramsSource.fireStateChanged();
                Thread.sleep(SLEEP_TIME);
            }

            // 4) SPLIT HESSIAN COMPONENTS in COMPONENT OPERATIONS
            // #####################################################
            createLink(application, regularDiffOpsModuleVNName, sourceFieldName, componentOperationsModuleVNName, "inField");

            final org.visnow.vn.engine.core.Parameters paramsComponentOperations = application.getEngine().getModule(componentOperationsModuleVNName).getCore().getParameters();
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    try {
                        paramsComponentOperations.setValue(COMPONENT_OPERATIONS_VCSPLIT, new boolean[]{true});
                        paramsComponentOperations.set(DiffOpPluginShared.RUNNING_MESSAGE, RunButton.RunState.RUN_DYNAMICALLY);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        fail("AD: " + ex.toString());
                    }
                }
            });

            application.getEngine().getModule(componentOperationsModuleVNName).getCore().startAction();

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 5);

            currentErrorMessage = null;
            paramsSource.fireStateChanged();
            Thread.sleep(SLEEP_TIME);

            // 5) Combine fields from regular and irregular output
            // #####################################################
            createLink(application, irregularDiffOpsModuleVNName, "outField", combineFieldsModuleVNName, "inFields");
            createLink(application, componentOperationsModuleVNName, "regularOutField", combineFieldsModuleVNName, "inFields");

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 5);

            currentErrorMessage = null;
//            paramsSource.fireStateChanged();
            Thread.sleep(SLEEP_TIME);

            // 6) Component calculator - calculate difference between regular and irregular diff ops
            // ##########################################################################################################
            String componentCalculatorModuleVNName = SimpleApplication.addModule(application, ComponentCalculator.class.getName(), 260);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            createLink(application, combineFieldsModuleVNName, "outIrregularField", componentCalculatorModuleVNName, "inField");

            final String[] expressions;
            final String[] aliases;

            switch (N_DIMS) {
                case 2:
                    expressions = EXPRESSIONS_2D;
                    aliases = INPUT_ALIASES_2D;
                    break;
                case 3:
                    expressions = EXPRESSIONS_3D;
                    aliases = INPUT_ALIASES_3D;
                    break;
                default:
                    expressions = null;
                    aliases = null;
                    fail("N_DIMS error");
                    break;
            }

            final org.visnow.vn.engine.core.Parameters paramsComponentCalculator = application.getEngine().getModule(componentCalculatorModuleVNName).getCore().getParameters();
            application.getEngine().getModule(componentCalculatorModuleVNName).getCore().startAction();
            for (int i = 0; i < 2; i++) {

                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            // setup parameters in Component Calculator
                            // 1 - aliases
                            paramsComponentCalculator.setValue(COMPONENT_CALCULATOR_INPUT_ALIASES, aliases);
                            paramsComponentCalculator.setValue(COMPONENT_CALCULATOR_EXPRESSION_LINES, expressions);
                            paramsComponentCalculator.setValue(COMPONENT_CALCULATOR_RETAIN, true);

                            paramsComponentCalculator.set(DiffOpPluginShared.RUNNING_MESSAGE, RunButton.RunState.RUN_DYNAMICALLY);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            fail("AD: " + ex.toString());
                        }
                    }
                });
                application.getEngine().getModule(componentCalculatorModuleVNName).getCore().startAction();

                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                Thread.sleep(SLEEP_TIME * 5);

                currentErrorMessage = null;
                paramsSource.fireStateChanged();
                Thread.sleep(SLEEP_TIME);
            }

            // 7) Connect to viewer
            createLink(application, componentCalculatorModuleVNName, "outObj", viewerModuleVNName, "inObject");

            // 8) Optional field statistics
            createLink(application, componentCalculatorModuleVNName, "outIrregularField", fieldStatisticsModuleVNName, "inField");

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 5);

            currentErrorMessage = null;
//            paramsSource.fireStateChanged();
            Thread.sleep(SLEEP_TIME);

            // ---------------------------------------------------------------------
            // ---------------------------------------------------------------------
            VNIrregularField outputDiffOps = (VNIrregularField) application.getEngine().getModule(irregularDiffOpsModuleVNName).getCore().getOutputs().getOutput("outField").getData().getValue();
            String error = "";
            if (outputDiffOps == null) {
                error = "no_output_";
            }
            VisNow.get().getMainWindow().getApplicationsPanel().removeApplication(application, true);
            VisNow.get().getMainWindow().dispose();
            if (!error.equalsIgnoreCase("") || currentErrorMessage != null) {
                fail(error + currentErrorMessage);
            }
        } catch (InterruptedException | InvocationTargetException ex) {
            String error = "exception_";
            ex.printStackTrace();
            fail(error + ex.toString());
        }

    }
}
