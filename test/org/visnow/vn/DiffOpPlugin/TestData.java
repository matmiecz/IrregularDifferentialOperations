/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.basic.testdata.TestField.TestFieldShared;
import static org.visnow.vn.lib.basic.testdata.TestField.TestFieldShared.GAUSSIANS;
import org.visnow.vn.lib.utils.expressions.ArrayExpressionParser;

/**
 * Generate test data like in TestField (but without GUI).
 *
 * Idea: - alike ComponentCalculator - alike TestField
 *
 * @author mateuszmieczkowski
 */
public class TestData {

    Field field = null;

    public TestData(String fieldType) throws Exception {
        if (fieldType.equals("gaussians")) {
            field = gaussian2D();
        } else if (fieldType.equals("component_calculator_output")) {
            field = fieldFromComponentCalculator();

        } else {
            System.out.println("not found");
        }

    }

    public Field getField() {
        return field;
    }

    //generate gaussian field TestField alike
    public Field gaussian2D() {
        // schema of creating test field same as in TestField
        int dimNum = 2;
        int resolution = 50;

        int[] dims = new int[dimNum];
        Arrays.fill(dims, resolution);

        RegularField testField = new RegularField(dims);
        testField.setName("test regular field");

        TestFieldShared.GeometryType geometryType = TestFieldShared.GeometryType.TRIVIAL_GEOM;
        float[][] affine = new float[4][3];
        for (float[] aff : affine) {
            Arrays.fill(aff, 0);
        }

        // switch geometry type = TRIVIAL hardcoded copied
        for (int i = 0; i < dimNum; i++) {
            affine[i][i] = 1;
        }

        testField.setAffine(affine);
        // TODO set coords?

        FloatLargeArray crds = testField.getCoordsFromAffine();
        testField.setCoords(crds, 0);

        testField.checkTrueNSpace();

        int n = dims[0] * dims[1];
        FloatLargeArray gaussianArray = new FloatLargeArray(n, false);

        for (int j = 0; j < dims[1]; j++) {
            //this made into threads in testField (omitted) - single thread here
            for (int i = 0, l = dims[0] * j; i < dims[0]; i++, l++) {
                gaussianArray.setFloat((long) l, l * l);

            }

        }

        List<DataArray> dataArrays = new ArrayList<>();
        dataArrays.add(DataArray.create(gaussianArray, 1, GAUSSIANS));

        for (DataArray dataArray : dataArrays) {
            testField.addComponent(dataArray);
        }

        return testField;
    }

    public RegularField fieldFromComponentCalculator() throws Exception {
        // nearly copied from Component Calulator::generateOutField

        // --------------------------------------
        // param 1 - extent
        float[][] extents = {{1, 1, 1}, {5, 5, 5}};
        // param 2 - size
        int[] dims = {10, 10, 10};

        //param 3 - expression to evaluate
        String expr = "_x^3+_y^3";
        expr = "coords.0^3+coords.1^3"; // TODO name it x and y, not coords

        //not necessary - name and unit
        String name = "f", unit = "1";
        // --------------------------------------

        int dim = dims.length;

        RegularField testField = new RegularField(dims);
        testField.setName("out Field");

        float[][] affine = new float[4][3];
        for (int i = 0; i < dim; i++) {
            affine[3][i] = extents[0][i];
            affine[i][i] = (extents[1][i] - extents[0][i]) / (dims[i] - 1);
        }
        testField.setAffine(affine);
        long nNodes = testField.getNNodes();

        boolean doublePrecision = false;
        boolean ignoreUnits = true;

        Map<String, DataArray> vars = new HashMap<>();

        // extract only indices - _i, _j, _k and coords _x, _y, _z
        if (testField instanceof RegularField) {
            for (int i = 0; i < dims.length; i++) {
                String index_name = "index." + i; // index name i.e. index.0 - first index
                vars.put(index_name, DataArray.create(testField.getIndices(i), 1, index_name));

                String coord_name = "coords." + i;

                // create coordinate data - extracted from createCoordinateData 
                FloatLargeArray coord = new FloatLargeArray(nNodes);

                switch (dims.length) {
                    case 1:
                        for (long x = 0; x < dims[0]; x++) {
                            coord.setFloat(x, affine[3][0] + x * affine[0][i]);
                        }
                        break;
                    case 2:
                        for (long y = 0, l = 0; y < dims[1]; y++) {
                            for (long x = 0; x < dims[0]; x++, l++) {
                                coord.setFloat(l, affine[3][i] + x * affine[0][i] + y * affine[1][i]);
                            }
                        }
                        break;
                    case 3:
                        for (long z = 0, l = 0; z < dims[2]; z++) {
                            for (long y = 0; y < dims[1]; y++) {
                                for (long x = 0; x < dims[0]; x++, l++) {
                                    coord.setFloat(l, affine[3][i] + x * affine[0][i] + y * affine[1][i] + z * affine[2][i]);
                                }
                            }
                        }
                        break;
                }
                DataArray da = DataArray.create(coord, 1, coord_name, testField.getCoordsUnit(i), null);
                vars.put(coord_name, da);

            }
        }

        ArrayExpressionParser parser;

        parser = new ArrayExpressionParser(nNodes, doublePrecision, ignoreUnits, vars);
        DataArray result = parser.evaluateExpr(expr);
        if (result != null) {
            result.setName(name);
            result.setUnit(unit);
            testField.addComponent(result);
        }

        return testField;
    }

}
