package org.visnow.vn.DiffOpPlugin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.DiffOpPlugin.Exceptions.ComputationError;

/**
 *
 * @author mateuszmieczkowski
 */
public class NodeNeighboursTest {

    public NodeNeighboursTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCountTriangle() {
        int[] nodes = {1, 2, 3, 2, 3, 4, 2, 4, 5, 3, 4, 6, 4, 6, 9, 4, 8, 9, 4, 5, 8, 5, 8, 10, 8, 9, 10, 5, 10, 0, 5, 7, 0, 2, 5, 7};
        int nNodes = 11;
        CellType simplexType = CellType.TRIANGLE;

        NodeNeighbours nn = new NodeNeighbours(nNodes, nodes, simplexType);
        int[] computed_count = nn.maxNeighboursCount(nodes, nNodes, simplexType);
        int[] expected_count = {4, 2, 8, 6, 12, 12, 4, 4, 8, 6, 6};
        assertArrayEquals(expected_count, computed_count);
    }

    // TODO changed static to instance method
    @Test
    public void testAllocateFillTriangle() {
        int[] nodes = {1, 2, 3, 2, 3, 4, 2, 4, 5, 3, 4, 6, 4, 6, 9, 4, 8, 9, 4, 5, 8, 5, 8, 10, 8, 9, 10, 5, 10, 0, 5, 7, 0, 2, 5, 7};
        int nNodes = 11;
        CellType simplexType = CellType.TRIANGLE;

        int[] count = {4, 2, 8, 6, 12, 12, 4, 4, 8, 6, 6};
        int[][] neighbours = NodeNeighbours.allocateMaxNeighbours(nNodes, count);

        NodeNeighbours nn = new NodeNeighbours(nNodes, nodes, simplexType);
        int[][] computed_neighbours = nn.fill(neighbours, nodes, simplexType);

        int[][] expected_neighbours = {
            {5, 10, 5, 7},
            {2, 3},
            {1, 3, 3, 4, 4, 5, 5, 7},
            {1, 2, 2, 4, 4, 6},
            {2, 3, 2, 5, 3, 6, 6, 9, 8, 9, 5, 8},
            {2, 4, 4, 8, 8, 10, 10, 0, 7, 0, 2, 7},
            {3, 4, 4, 9},
            {5, 0, 2, 5},
            {4, 9, 4, 5, 5, 10, 9, 10},
            {4, 6, 4, 8, 8, 10},
            {5, 8, 8, 9, 5, 0}
        };
        assertArrayEquals(expected_neighbours, computed_neighbours);
    }

    @Test
    public void testDirectNeighboursTriangles() {
        int[] nodes = {1, 2, 3, 2, 3, 4, 2, 4, 5, 3, 4, 6, 4, 6, 9, 4, 8, 9, 4, 5, 8, 5, 8, 10, 8, 9, 10, 5, 10, 0, 5, 7, 0, 2, 5, 7};
        int nNodes = 11;
        CellType simplexType = CellType.TRIANGLE;

        NodeNeighbours nn = new NodeNeighbours(nNodes, nodes, simplexType);
        nn.calculateNodeNeigbours();
        int[] directNeighbours = nn.getDirectNeighbours();
        int[] directNeighboursInfo = nn.getDirectNeighboursInfo();

        int[] expectedDirectNeighbours = {5, 7, 10, 2, 3, 1, 3, 4, 5, 7, 1, 2, 4, 6, 2, 3, 5, 6, 8, 9, 0, 2, 4, 7, 8, 10, 3, 4, 9, 0, 2, 5, 4, 5, 9, 10, 4, 6, 8, 10, 0, 5, 8, 9};
        int[] expectedDirectNeighboursInfo = {0, 3, 5, 10, 14, 20, 26, 29, 32, 36, 40, 44};

        assertArrayEquals(expectedDirectNeighbours, directNeighbours);
        assertArrayEquals(expectedDirectNeighboursInfo, directNeighboursInfo);
    }

    @Test
    public void testDirectNeighboursTriangles1() {
        int[] nodes = {0, 1, 6, 1, 2, 7, 1, 6, 7, 2, 3, 8, 2, 7, 8, 3, 4, 9, 3, 8, 9, 4, 5, 10, 4, 9, 10, 6, 7, 12, 6, 11, 12, 7, 8, 13, 7, 12, 13, 8, 9, 14, 8, 13, 14, 9, 10, 15, 9, 14, 15, 10, 15, 16, 11, 12, 17, 12, 13, 18, 12, 17, 18, 13, 14, 19, 13, 18, 19, 14, 15, 20, 14, 19, 20, 15, 16, 21, 15, 20, 21, 17, 18, 23, 17, 22, 23, 18, 19, 24, 18, 23, 24, 19, 20, 25, 19, 24, 25, 20, 21, 26, 20, 25, 26, 21, 26, 27, 22, 23, 28, 23, 24, 29, 23, 28, 29, 24, 25, 30, 24, 29, 30, 25, 26, 31, 25, 30, 31, 26, 27, 32, 26, 31, 32, 28, 29, 34, 28, 33, 34, 29, 30, 35, 29, 34, 35, 30, 31, 36, 30, 35, 36, 31, 32, 37, 31, 36, 37, 32, 37, 38, 33, 34, 39, 34, 35, 40, 34, 39, 40, 35, 36, 41, 35, 40, 41, 36, 37, 42, 36, 41, 42, 37, 38, 43, 37, 42, 43};
        int nNodes = 44;
        CellType simplexType = CellType.TRIANGLE;

        NodeNeighbours nn = new NodeNeighbours(nNodes, nodes, simplexType);
        nn.calculateNodeNeigbours();

        int[] directNeighbours = nn.getDirectNeighbours();
        int[] directNeighboursInfo = nn.getDirectNeighboursInfo();

        int[] expectedDirectNeighboursInfo = {0, 2, 6, 10, 14, 18, 20, 25, 31, 37, 43, 48, 51, 57, 63, 69, 75, 78, 83, 89, 95, 101, 106, 109, 115, 121, 127, 133, 136, 141, 147, 153, 159, 164, 167, 173, 179, 185, 191, 194, 197, 201, 205, 209, 212};
        int[] expectedDirectNeighbours = {1, 6, 0, 2, 6, 7, 1, 3, 7, 8, 2, 4, 8, 9, 3, 5, 9, 10, 4, 10, 0, 1, 7, 11, 12, 1, 2, 6, 8, 12, 13, 2, 3, 7, 9, 13, 14, 3, 4, 8, 10, 14, 15, 4, 5, 9, 15, 16, 6, 12, 17, 6, 7, 11, 13, 17, 18, 7, 8, 12, 14, 18, 19, 8, 9, 13, 15, 19, 20, 9, 10, 14, 16, 20, 21, 10, 15, 21, 11, 12, 18, 22, 23, 12, 13, 17, 19, 23, 24, 13, 14, 18, 20, 24, 25, 14, 15, 19, 21, 25, 26, 15, 16, 20, 26, 27, 17, 23, 28, 17, 18, 22, 24, 28, 29, 18, 19, 23, 25, 29, 30, 19, 20, 24, 26, 30, 31, 20, 21, 25, 27, 31, 32, 21, 26, 32, 22, 23, 29, 33, 34, 23, 24, 28, 30, 34, 35, 24, 25, 29, 31, 35, 36, 25, 26, 30, 32, 36, 37, 26, 27, 31, 37, 38, 28, 34, 39, 28, 29, 33, 35, 39, 40, 29, 30, 34, 36, 40, 41, 30, 31, 35, 37, 41, 42, 31, 32, 36, 38, 42, 43, 32, 37, 43, 33, 34, 40, 34, 35, 39, 41, 35, 36, 40, 42, 36, 37, 41, 43, 37, 38, 42};
        int[][] computedDirectNeighbours = new int[nNodes][];
        assertArrayEquals(expectedDirectNeighbours, directNeighbours);
        assertArrayEquals(expectedDirectNeighboursInfo, directNeighboursInfo);
    }

    @Test
    public void testSubsequentNeighboursForNode() {
        int[] nodes = {1, 2, 3, 2, 3, 4, 2, 4, 5, 3, 4, 6, 4, 6, 9, 4, 8, 9, 4, 5, 8, 5, 8, 10, 8, 9, 10, 5, 10, 0, 5, 7, 0, 2, 5, 7};
        int nNodes = 11;
        CellType simplexType = CellType.TRIANGLE;

        NodeNeighbours nn = new NodeNeighbours(nNodes, nodes, simplexType);
        nn.calculateNodeNeigbours();

        int[][] expectedSubsequentNeighbours = {
            {2, 4, 5, 7, 8, 9, 10},
            {2, 3, 4, 5, 6, 7},
            {0, 1, 3, 4, 5, 6, 7, 8, 9, 10},
            {1, 2, 4, 5, 6, 7, 8, 9},
            {0, 1, 2, 3, 5, 6, 7, 8, 9, 10},
            {0, 1, 2, 3, 4, 6, 7, 8, 9, 10},
            {1, 2, 3, 4, 5, 8, 9, 10},
            {0, 1, 2, 3, 4, 5, 8, 10},
            {0, 2, 3, 4, 5, 6, 7, 9, 10},
            {0, 2, 3, 4, 5, 6, 8, 10},
            {0, 2, 4, 5, 6, 7, 8, 9}
        };
        int[][] computedSubsequentNeighbours = new int[nNodes][];
        for (int i = 0; i < nNodes; i++) {
            int[] currentNeighbours = nn.getDirectNeighboursForNode(i);
            computedSubsequentNeighbours[i] = nn.subsequentNeighboursForNode(i, currentNeighbours);
        }
        assertArrayEquals(expectedSubsequentNeighbours, computedSubsequentNeighbours);
    }

    @Test
    public void testSubsequentNeighboursForNode1() {
        int[] nodes = {0, 1, 6, 1, 2, 7, 1, 6, 7, 2, 3, 8, 2, 7, 8, 3, 4, 9, 3, 8, 9, 4, 5, 10, 4, 9, 10, 6, 7, 12, 6, 11, 12, 7, 8, 13, 7, 12, 13, 8, 9, 14, 8, 13, 14, 9, 10, 15, 9, 14, 15, 10, 15, 16, 11, 12, 17, 12, 13, 18, 12, 17, 18, 13, 14, 19, 13, 18, 19, 14, 15, 20, 14, 19, 20, 15, 16, 21, 15, 20, 21, 17, 18, 23, 17, 22, 23, 18, 19, 24, 18, 23, 24, 19, 20, 25, 19, 24, 25, 20, 21, 26, 20, 25, 26, 21, 26, 27, 22, 23, 28, 23, 24, 29, 23, 28, 29, 24, 25, 30, 24, 29, 30, 25, 26, 31, 25, 30, 31, 26, 27, 32, 26, 31, 32, 28, 29, 34, 28, 33, 34, 29, 30, 35, 29, 34, 35, 30, 31, 36, 30, 35, 36, 31, 32, 37, 31, 36, 37, 32, 37, 38, 33, 34, 39, 34, 35, 40, 34, 39, 40, 35, 36, 41, 35, 40, 41, 36, 37, 42, 36, 41, 42, 37, 38, 43, 37, 42, 43};
        int nNodes = 44;
        CellType simplexType = CellType.TRIANGLE;

        NodeNeighbours nn = new NodeNeighbours(nNodes, nodes, simplexType);
        nn.calculateNodeNeigbours();

        int[][] expectedSubsequentNeighbours = {
            {1, 2, 6, 7, 11, 12},
            {0, 2, 3, 6, 7, 8, 11, 12, 13},
            {0, 1, 3, 4, 6, 7, 8, 9, 12, 13, 14},
            {1, 2, 4, 5, 7, 8, 9, 10, 13, 14, 15},
            {2, 3, 5, 8, 9, 10, 14, 15, 16},
            {3, 4, 9, 10, 15, 16},
            {0, 1, 2, 7, 8, 11, 12, 13, 17, 18},
            {0, 1, 2, 3, 6, 8, 9, 11, 12, 13, 14, 17, 18, 19},
            {1, 2, 3, 4, 6, 7, 9, 10, 12, 13, 14, 15, 18, 19, 20},
            {2, 3, 4, 5, 7, 8, 10, 13, 14, 15, 16, 19, 20, 21},
            {3, 4, 5, 8, 9, 14, 15, 16, 20, 21},
            {0, 1, 6, 7, 12, 13, 17, 18, 22, 23},
            {0, 1, 2, 6, 7, 8, 11, 13, 14, 17, 18, 19, 22, 23, 24},
            {1, 2, 3, 6, 7, 8, 9, 11, 12, 14, 15, 17, 18, 19, 20, 23, 24, 25},
            {2, 3, 4, 7, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20, 21, 24, 25, 26},
            {3, 4, 5, 8, 9, 10, 13, 14, 16, 19, 20, 21, 25, 26, 27},
            {4, 5, 9, 10, 14, 15, 20, 21, 26, 27},
            {6, 7, 11, 12, 13, 18, 19, 22, 23, 24, 28, 29},
            {6, 7, 8, 11, 12, 13, 14, 17, 19, 20, 22, 23, 24, 25, 28, 29, 30},
            {7, 8, 9, 12, 13, 14, 15, 17, 18, 20, 21, 23, 24, 25, 26, 29, 30, 31},
            {8, 9, 10, 13, 14, 15, 16, 18, 19, 21, 24, 25, 26, 27, 30, 31, 32},
            {9, 10, 14, 15, 16, 19, 20, 25, 26, 27, 31, 32},
            {11, 12, 17, 18, 23, 24, 28, 29, 33, 34},
            {11, 12, 13, 17, 18, 19, 22, 24, 25, 28, 29, 30, 33, 34, 35},
            {12, 13, 14, 17, 18, 19, 20, 22, 23, 25, 26, 28, 29, 30, 31, 34, 35, 36},
            {13, 14, 15, 18, 19, 20, 21, 23, 24, 26, 27, 29, 30, 31, 32, 35, 36, 37},
            {14, 15, 16, 19, 20, 21, 24, 25, 27, 30, 31, 32, 36, 37, 38},
            {15, 16, 20, 21, 25, 26, 31, 32, 37, 38},
            {17, 18, 22, 23, 24, 29, 30, 33, 34, 35, 39, 40},
            {17, 18, 19, 22, 23, 24, 25, 28, 30, 31, 33, 34, 35, 36, 39, 40, 41},
            {18, 19, 20, 23, 24, 25, 26, 28, 29, 31, 32, 34, 35, 36, 37, 40, 41, 42},
            {19, 20, 21, 24, 25, 26, 27, 29, 30, 32, 35, 36, 37, 38, 41, 42, 43},
            {20, 21, 25, 26, 27, 30, 31, 36, 37, 38, 42, 43},
            {22, 23, 28, 29, 34, 35, 39, 40},
            {22, 23, 24, 28, 29, 30, 33, 35, 36, 39, 40, 41},
            {23, 24, 25, 28, 29, 30, 31, 33, 34, 36, 37, 39, 40, 41, 42},
            {24, 25, 26, 29, 30, 31, 32, 34, 35, 37, 38, 40, 41, 42, 43},
            {25, 26, 27, 30, 31, 32, 35, 36, 38, 41, 42, 43},
            {26, 27, 31, 32, 36, 37, 42, 43},
            {28, 29, 33, 34, 35, 40, 41},
            {28, 29, 30, 33, 34, 35, 36, 39, 41, 42},
            {29, 30, 31, 34, 35, 36, 37, 39, 40, 42, 43},
            {30, 31, 32, 35, 36, 37, 38, 40, 41, 43},
            {31, 32, 36, 37, 38, 41, 42}
        };
        int[][] computedSubsequentNeighbours = new int[nNodes][];
        for (int i = 0; i < nNodes; i++) {
            int[] currentNeighbours = nn.getDirectNeighboursForNode(i);
            computedSubsequentNeighbours[i] = nn.subsequentNeighboursForNode(i, currentNeighbours);
        }
        assertArrayEquals(expectedSubsequentNeighbours, computedSubsequentNeighbours);
    }

    //TODO
    @Test
    public void testPreprocessNeighbours() throws ComputationError {
        int[] nodes = {1, 2, 3, 2, 3, 4, 2, 4, 5, 3, 4, 6, 4, 6, 9, 4, 8, 9, 4, 5, 8, 5, 8, 10, 8, 9, 10, 5, 10, 0, 5, 7, 0, 2, 5, 7};
        int nNodes = 11;
        CellType simplexType = CellType.TRIANGLE;

        NodeNeighbours nn = new NodeNeighbours(nNodes, nodes, simplexType);
        nn.calculateNodeNeigbours();
        nn.preprocessNeighbours(simplexType.getDim());
        int[][] preprocessedNeighbours = nn.getRequiredNeighbours();

        int[][] expectedPreprocessedNeighbours = {
            {2, 4, 5, 7, 8, 9, 10},
            {2, 3, 4, 5, 6, 7},
            {0, 1, 3, 4, 5, 6, 7, 8, 9, 10},
            {1, 2, 4, 5, 6, 7, 8, 9},
            {2, 3, 5, 6, 8, 9},
            {0, 2, 4, 7, 8, 10},
            {1, 2, 3, 4, 5, 8, 9, 10},
            {0, 1, 2, 3, 4, 5, 8, 10},
            {0, 2, 3, 4, 5, 6, 7, 9, 10},
            {0, 2, 3, 4, 5, 6, 8, 10},
            {0, 2, 4, 5, 6, 7, 8, 9}
        };

        assertArrayEquals(expectedPreprocessedNeighbours, preprocessedNeighbours);
    }

}
