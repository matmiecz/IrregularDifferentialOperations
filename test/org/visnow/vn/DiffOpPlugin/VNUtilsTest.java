/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.DiffOpPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author mateuszmieczkowski
 */
public class VNUtilsTest {

    public VNUtilsTest() {
    }

    @Test
    public void testInvokeScript() throws IOException, InterruptedException {
        String path = "/Users/mateuszmieczkowski/tmp/qhull_test/script.sh";
        List<String> out = VNUtils.invokeShellScript(path);
        assert out.size() == 1;
        assert out.get(0).equals("ala ma kota");
    }

    @Test
    public void testInvokeTriangulationScript() throws IOException, InterruptedException {
        String path = "/Users/mateuszmieczkowski/tmp/qhull_test/triangulation_script.sh";
        List<String> out = VNUtils.invokeShellScript(path);
        assert out.size() > 0;
    }

    @Test
    public void testInvokeCommand() throws IOException, InterruptedException {
        String delaunayExecPath = "/Users/mateuszmieczkowski/Documents/ICM_studia/PRACA_MAGISTERSKA_ICM/external_libraries/qhull/qhull/bin/qdelaunay";
        String coordsFilePath = "/Users/mateuszmieczkowski/Documents/ICM_studia/PRACA_MAGISTERSKA_ICM/tmp/coords_dumped.txt";
        // by default delaunay output to file (buffering errors while invoking shell and trying to fetch stdout)
        String triangulationTargetFilePath = "/Users/mateuszmieczkowski/Documents/ICM_studia/PRACA_MAGISTERSKA_ICM/tmp/triangulation_results.txt";

        // invoke qhull program (external dependency) 
        String[] commands = new String[]{
            delaunayExecPath,
            "i",
            "Qt",
            "TI",
            coordsFilePath,
            "TO",
            triangulationTargetFilePath
        };

        String command = "";
        for (String s : commands) {
            command = command.concat(s);
            command = command.concat(" ");
        }

        List<String> script = new ArrayList<>();
        script.add("#!/bin/bash");
        script.add(command);

        String tmpShellScript = "/Users/mateuszmieczkowski/tmp/qhull_test/tmp_script.sh";
        VNUtils.linesToFile(script, tmpShellScript);

        Process pr = VNUtils.callShell(tmpShellScript);
        if (pr.exitValue() == 0) {
            InputStream stdout = pr.getInputStream();
            boolean is3D = true;
            int[] triangulation = ArrayUtils.readTriangulationResults(triangulationTargetFilePath, is3D);
            int length = triangulation.length;
            assert length > 0;
            
            File tmpShellScriptFile = new File(tmpShellScript);
            File tmpTriangulationResultsFile = new File(triangulationTargetFilePath);
            tmpShellScriptFile.delete();
            tmpTriangulationResultsFile.delete();
        } else {
            throw new AssertionError("exit while executing script");
        }
    }

    @Test
    public void testWriteToFile() throws IOException {
        String filePath = "/Users/mateuszmieczkowski/tmp/qhull_test/dump_test_script.sh";
        List<String> script = new ArrayList<>();

        script.add("#!/bin/bash");
        script.add("delaunay i Qt TI 'path'");
        VNUtils.linesToFile(script, filePath);

    }
}
