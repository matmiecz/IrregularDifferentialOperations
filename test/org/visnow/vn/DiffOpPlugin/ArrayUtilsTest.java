package org.visnow.vn.DiffOpPlugin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import org.visnow.vn.ModuleCreator.templates.ModuleTemplateStrings;

/**
 *
 * @author mateuszmieczkowski
 */
public class ArrayUtilsTest {

    public ArrayUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testArray2DTo1D() {
        int[][] a = {{3, 11, 4, 5, 10, 11, 8}, {5, 4, 5, 1, -1}};
        int[] expected_a1D = {3, 11, 4, 5, 10, 11, 8, 5, 4, 5, 1, -1};
        int[] a1D = ArrayUtils.array2DTo1D(a);
        assertArrayEquals(expected_a1D, a1D);
    }

    @Test
    public void testArray1DTo2D() {
        //TODO
    }

    @Test
    public void testExtractRow() {
        //TODO
    }

    @Test
    public void testSortArray2D() {
        int[][] arrayToSort = {
            {-5, -100, 12, 2, 0},
            {-1, -1, -1, -100},
            {},
            {10, 11, 12},
            {-5, -5, -5}
        };
        int[][] expectedArraySorted = {
            {-100, -5, 0, 2, 12},
            {-100, -1, -1, -1},
            {},
            {10, 11, 12},
            {-5, -5, -5}
        };

        int[][] computedArraySorted = ArrayUtils.sortArray2D(arrayToSort);
        assertArrayEquals(expectedArraySorted, computedArraySorted);
    }

    @Test
    public void testRemoveNegatives1D() {
        int[] array = {-5, 1, 1, 3, 6, 2, -5, 13, 14, -1};
        int[] expected = {1, 1, 2, 3, 6, 13, 14};

        int[] removedNegatives = ArrayUtils.removeNegatives1D(array);
        assertArrayEquals(expected, removedNegatives);
    }

    @Test
    public void testRemoveNegatives2D() {
        int[][] array2D = {{5, -1, -4, 2, -6}, {-1, 3, 2, 3, 3, -1, 3}, {-6, -8, -5}};
        int[][] expectedArray2D = {{2, 5}, {2, 3, 3, 3, 3}, {}};
        assertArrayEquals(ArrayUtils.removeNegatives2D(array2D), expectedArray2D);
    }

    @Test
    public void testRemoveDuplicates1D() {
        int[] a = {3, 11, 3, 4, 5, 6, 11, 8};
        int[] b = {-30, 11, 1, 1, -50, -3, -10, 9, 1, 11};

        int[] removed_duplicates_a = ArrayUtils.removeDuplicates1D(a);
        int[] removed_duplicates_b = ArrayUtils.removeDuplicates1D(b);

        int[] expected_removed_duplicates_a = {3, 4, 5, 6, 8, 11};
        int[] expected_removed_duplicates_b = {-50, -30, -10, -3, 1, 9, 11};
        assertArrayEquals(expected_removed_duplicates_a, removed_duplicates_a);
        assertArrayEquals(expected_removed_duplicates_b, removed_duplicates_b);
    }

    @Test
    public void testRemoveDuplicates2D() {
        int[][] a = {{3, 11, 3, 4, 5, 6, 11, 8}, {5, 4, 5, 1, -1}};

        int[][] removed_duplicates_a = ArrayUtils.removeDuplicates2D(a);
        int[][] expected_removed_duplicates_a = {{3, 4, 5, 6, 8, 11}, {-1, 1, 4, 5}};
        assertArrayEquals(expected_removed_duplicates_a, removed_duplicates_a);
    }

    @Test
    public void testLSolveLUD() {
        double[][] a1 = {{3, 2, -1}, {2, -2, 4}, {-1, 0.5, -1}};
        double[] b1 = {1, -2, 0};
        double[] expectedResult1 = {1, -2, -2};

        double[] result1 = ArrayUtils.lsolveLUD(a1, b1);
        System.out.println(Arrays.toString(result1));
        double delta = 0.0000001;
        assertArrayEquals(expectedResult1, result1, delta);
        //test multiplied equation
//        double[][] a2 = {{3, 2, -1}, {6, 4, -2}, {-1, 0.5, -1}};
//        double[] b2 = {1, 2, 0};
//        double[] expectedResult2 = {};
//
//        double[] result2 = Utils.lsolveLUD(a2, b2);
//        System.out.println(Arrays.toString(result2));
    }

    //@Test
    public void testLSolve() {
        double delta = 0.0000001;

//        double[][] a1 = {{3, 2, -1}, {2, -2, 4}, {-1, 0.5, -1}};
//        double[] b1 = {1, -2, 0};
//        double[] expectedResult1 = {1, -2, -2};
//
//        double[] result1 = Utils.lsolve(a1, b1);
//        assertArrayEquals(expectedResult1, result1, delta);
//        System.out.println(Arrays.toString(result1));
        double[][] a2 = {
            {14.0, 0.0, 1.1920928955078125E-7, 0.10204076766967773, 0.0033319418453174876, 0.003331946711004008, 0.0020824636533234298, 0.0, 0.0, 6.082103709559306E-10},
            {0.0, 0.0033319418453174876, 0.0, 0.0, 0.0, 0.0, 0.0, 2.4824901262801048E-11, 1.6999695186345953E-5, 0.0},
            {0.0, 0.0, 0.0033319467110029927, -2.606615875525416E-10, -3.5464144661144345E-12, 1.2057826792979565E-10, -5.319621699171652E-12, 0.0, 0.0, 1.6999720011260162E-5},
            {0.0, 0.0, 0.0, 0.0013387266342793273, -7.2855836512914025E-6, -7.285594290532857E-6, 2.7320938692341294E-5, 0.0, 0.0, 9.309337973550393E-12},
            {0.0, 0.0, 0.0, 0.0, 5.550918230288781E-7, -1.387731584093502E-7, -2.6469779601696886E-23, 0.0, 0.0, -7.599455946540244E-14},
            {0.0, 0.0, 0.0, 0.0, 0.0, 5.204001039817752E-7, 4.632210463942018E-23, 0.0, 0.0, 3.166444600343982E-14},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -8.733845244637029E-24, 0.0, 0.0, 2.601996720177877E-7},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 6.938657920474543E-7, -1.2665759910900856E-13, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.6019929204476684E-7, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.792058005076893E-22}};
        double[] b2 = {-2.7361685931682587, 0.0019258233297563265, 0.0023005148072045172, 2.0072599295495506E-5, -5.310144499294625E-7, -7.487495205379264E-7, -5.052691215991873E-19, 4.5258029162981335E-18, -3.103112657849305E-12, -5.677902626099035E-20};
        double[] result2 = ArrayUtils.lsolve(a2, b2);
        double[] result3 = ArrayUtils.lsolveLUD(a2, b2);

        System.out.println(Arrays.toString(result2));
        System.out.println(Arrays.toString(result3));
    }

    @Test
    public void testDivision() {
        int[] x = {3, 1, 0, 15, 57, 54};
        int[] y = new int[x.length];
        for (int i = 0; i < x.length; i++) {
            y[i] = x[i] / 2;
            System.out.println(y[i]);
        }
    }

    public void getTemplate() {
        String header = ModuleTemplateStrings.getHeader("aaa_package", "bbb_module");
        //System.out.println(header);

        String variables = ModuleTemplateStrings.getVariables();
        String constructor = ModuleTemplateStrings.getConstructor("bbb_module");
        String onActive = ModuleTemplateStrings.getOnActive();
        String updateUI = ModuleTemplateStrings.getUpdateUI();

        System.out.println(variables);
        System.out.println(constructor);
        System.out.println(onActive);
        System.out.println(updateUI);
    }

    @Test
    public void minmaxTest() {
        double[][] tab = {{12.4, 1.1, 21.9, 8.4}, {1.5, 1.9}};
        double min = ArrayUtils.findMin(tab);
        double max = ArrayUtils.findMax(tab);

        double expectedMin = 1.1;
        double expectedMax = 21.9;
        assertEquals(expectedMax, max, 0.000001);
        assertEquals(expectedMin, min, 0.000001);
        System.out.println("--------------");
        System.out.println(min);
        System.out.println(max);
        System.out.println("--------------");
    }

    @Test
    public void normalizeTest() {
        double[] a = {0.1, 0.9, 200, 11.3, 5.2, 111};
        double[] b = ArrayUtils.normalizeArray(a, 0);
        System.out.println("--------------");
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
        System.out.println("--------------");
    }

    @Test
    public void testCountOccurences() {
        byte[] array = {0, 1, 1, 1, 2, 2, 2, 2, 0, 1, 0};
        int targetCount0 = 3;
        int count0 = ArrayUtils.countByteOccurences(array, (byte) 0);
        assertEquals(count0, targetCount0);
    }

    @Test
    public void testIndirectSort() {
        double[] array = {5.5, 6.5, 3.5};
        int[] targetIndicesArray = {1, 0, 2};

        int[] resultIndicesArray = ArrayUtils.sortIndices(array);
        assertArrayEquals(resultIndicesArray, targetIndicesArray);
    }

    @Test
    public void testExtrapolate() {
        double[] x = {1.0, 2.0};
        double[] y = {10.0, 20.0};

        double[][] params = {{1.5, 15.0}, {3.0, 30.0}};
        for (double[] param : params) {
            double value = param[0];
            double targetResult = param[1];
            double result = ArrayUtils.linearExtrapolation(x, y, value);
            assertEquals(targetResult, result, 0.000000001);
        }
    }

    @Test
    public void testFindSmallestIndices() {
        double[] arr = {100.0, 100.0};
        int[] target = {0};

        int[] result = ArrayUtils.findNSmallestIndices(arr, 1);
        assertArrayEquals(target, result);

    }

    @Test
    public void testReadTriangulationResults() throws IOException {
        String fileName = "/Users/mateuszmieczkowski/Documents/ICM_studia/PRACA_MAGISTERSKA_ICM/tmp/coords_dumped.txt";
        fileName = "/Users/mateuszmieczkowski/tmp/qhull_test/triangulation3D.txt";

        int[] result = ArrayUtils.readTriangulationResults(fileName, true);
        assertArrayEquals(result, new int[]{1, 2, 3});
    }

    @Test
    public void testPermutations() {
        int[] input = new int[]{1, 2, 3, 4};
        int n = 3;
        int[] flattenedTarget = new int[]{1, 2, 3, 1, 2, 4, 1, 3, 4, 2, 3, 4};

        List<int[]> result = ArrayUtils.permutationsDecorator(input, n);
        int[] flattenedResult = new int[n * result.size()];
        for (int i = 0; i < result.size(); i++) {
            int[] singleResult = result.get(i);
            System.arraycopy(singleResult, 0, flattenedResult, n * i, singleResult.length);

        }
        assertArrayEquals(flattenedTarget, flattenedResult);

    }

    @Test
    public void testNPermutations() {
        int supersetLen = 4;
        int subsetLen = 2;

        int target = 6;

        int result = ArrayUtils.getNPermutations(supersetLen, subsetLen);

        assert result == target;
    }

}
